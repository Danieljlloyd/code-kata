#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/socket.h>
#include <sys/un.h>

#define SV_SOCK_FN "/tmp/test_server"
#define COMMAND_SIZE 4
#define QUERY_SIZE 50
#define STR_SIZE 50
#define LOG_INTERVAL 10

struct Metrics {
	float vol;
	float cur;
};

int process_query(struct Metrics *metrics, char *query)
{
	/* Get the command name. */
	char command[QUERY_SIZE];
	strcpy(command, query);
	command[COMMAND_SIZE] = '\0';

	if (!strcmp(command, "VSET"))
		sscanf(query, "%4s %f %f", command,
		       &metrics->vol, &metrics->cur);
}

int main(void)
{
	int rc;
	struct Metrics metrics = {.vol=0.0, .cur=0.0};
	int count = 0;

	/* Create a listener socket. */
	int sfd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sfd == -1) {
		perror("socket");
		return -1;
	}

	/* Bind to an address. */
	rc = remove(SV_SOCK_FN);
	if (rc == -1 && errno != ENOENT) {
		perror("remove");
		return -1;
	}

	struct sockaddr_un svaddr;
	memset(&svaddr, 0, sizeof(struct sockaddr_un));
	svaddr.sun_family = AF_UNIX;
	rc = strcpy(svaddr.sun_path, SV_SOCK_FN);

	rc = bind(sfd, (struct sockaddr *) &svaddr, sizeof(struct sockaddr_un));
	if (rc == -1) {
		perror("bind");
		return -1;
	}

	/* Create a polling interface. */
	struct pollfd pollsfd;
	pollsfd.fd = sfd;
	pollsfd.events = POLLIN;

	/* Forever. */
	for (;;) {
		/* Poll the input interface. */
		poll(&pollsfd, 1, 0);

		/* Gather and process messages. */
		struct sockaddr_un claddr;
		char query[QUERY_SIZE];
		socklen_t len;
		len = sizeof(struct sockaddr_un);
		int bytes = 0;

		if (pollsfd.revents & POLLIN) {
			recvfrom(sfd, query, QUERY_SIZE, 0, &claddr, &len);
			if (bytes == -1) {
				perror("recvfrom");
				return -1;
			}

			process_query(&metrics, query);
		}

		/* Do some stuff. */
		time_t now;
		struct tm *tmp;

		now = time(NULL);
		tmp = localtime(&now);
		if (tmp == NULL) {
			perror("localtime");
			return -1;
		}

		char nowstr[STR_SIZE];
		strftime(nowstr, STR_SIZE, "%F_%T", tmp);

		usleep(1000000);

		/* Log periodically. */
		if (!(count % LOG_INTERVAL))
			printf("%s %06.2f %06.2f\n", nowstr,
                               metrics.vol, metrics.cur);

		count++;
	}
}
