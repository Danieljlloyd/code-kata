#include <stdio.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/un.h>

#define QUERY_SIZE 50
#define CLIENT_FN_SIZE 50
#define SV_SOCK_FN "/tmp/test_server"

int main(void)
{
	int rc;
	float v,i = 0;

	/* Create the client socket. */
	int sfd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sfd == -1) {
		perror("socket");
		return -1;
	}

	/* Create a unique client address. */
	long pid = getpid();
	struct sockaddr_un claddr;
	memset(&claddr, 0, sizeof(struct sockaddr_un));
	claddr.sun_family = AF_UNIX;
	sprintf(claddr.sun_path, "/tmp/test_client.%ld", pid);

	/* Bind the socket to the client address. */
	rc = bind(sfd, (struct sockaddr *) &claddr, sizeof(struct sockaddr_un));
	if (rc == -1) {
		perror("bind");
		return -1;
	}

	/* Define the server address. */
	struct sockaddr_un svaddr;
	memset(&svaddr, 0, sizeof(struct sockaddr_un));
	svaddr.sun_family = AF_UNIX;
	strcpy(svaddr.sun_path, SV_SOCK_FN);

	while (1) {
		/* Change the values. */
		v += 1.1;
		i += 2.3;

		/* Send a datagram to the server. */
		char query[QUERY_SIZE];	
		sprintf(query, "VSET %06.2f %06.2f", v, i);
		int msg_len = strlen(query);
		socklen_t len = sizeof(struct sockaddr_un);
		sendto(sfd, query, msg_len, 0, &svaddr, len);

		printf("%s\n", query);

		/* Wait 3 seconds. */
		usleep(3e6);
	}
}
