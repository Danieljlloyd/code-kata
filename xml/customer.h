#define MAX_CUSTOMERS 100

#include <libxml/parser.h>
#include <libxml/tree.h>

typedef struct customer_s {
	char name[50];
	char email[50];
	char age;
	char sex;
} customer_t;

typedef struct customers_s {
	customer_t data[MAX_CUSTOMERS];
	int n;
} customers_t;

customers_t customers_create(void);
customer_t customer_create(char *name, char *email, char age, char sex);
int customers_update(customers_t *self, customer_t customer);
customer_t customers_get(customers_t *self, char *name);
int customer_dump(customer_t *self, char *out);
int customers_dump(customers_t *self, char *out);
customer_t customer_load(xmlDocPtr doc, xmlNodePtr cur);
customers_t customers_load(xmlDocPtr doc, xmlNodePtr cur);
