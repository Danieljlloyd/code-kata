/* Example of a customer class that can serialize and deserialize itself as
 * XML using the libxml library. */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <customer.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlwriter.h>

customers_t customers_create(void)
{
	customers_t customers;

	customers.n = 0;
	memset(&customers.data, 0, sizeof(customer_t) * MAX_CUSTOMERS);

	return customers;
}

customer_t customer_create(char *name, char *email, char age, char sex)
{
	customer_t customer;

	strcpy(customer.name, name);
	strcpy(customer.email, email);
	customer.age = age;
	customer.sex = sex;

	return customer;
}

customer_t customers_get(customers_t *self, char *name)
{
	customer_t customer = customer_create("", "", 0, 0);

	/* Try and find the customer and return it. */
	for (int i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].name, name)) {
			customer = self->data[i];
		}
	}

	return customer;
}

int customers_update(customers_t *self, customer_t customer)
{
	/* Try and find the customer in the collection already and update it. */
	for (int i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].name, customer.name)) {
			self->data[i] = customer;
			return 0;
		}
	}

	/* If no customer found, create a new one. */
	self->data[self->n] = customer;
	self->n++;
	return 0;
}

int customer_dump(customer_t *self, char *out)
{
	int len;

	if (strcmp(out, "")) {
		fprintf(stderr, "customer serialization: out string must "
				"be empty.\n");
		return -1;
	}


	xmlBufferPtr buf = xmlBufferCreate();

	xmlTextWriterPtr writer = xmlNewTextWriterMemory(buf, 0);
	xmlTextWriterSetIndent(writer, 1);
	xmlTextWriterStartElement(writer, BAD_CAST "customer");
	xmlTextWriterWriteFormatElement(writer, BAD_CAST "name", "%s",
			self->name);
	xmlTextWriterWriteFormatElement(writer, BAD_CAST "email", "%s",
			self->email);
	xmlTextWriterWriteFormatElement(writer, BAD_CAST "age", "%d",
			self->age);
	xmlTextWriterWriteFormatElement(writer, BAD_CAST "sex", "%c",
			self->sex);
	xmlTextWriterEndElement(writer);
	xmlFreeTextWriter(writer);

	strcpy(out, buf->content);
	xmlBufferFree(buf);

	return strlen(out);
}

int customers_dump(customers_t *self, char *out)
{
	int len;
	char data_str[4096];
	char tmp[1024];

	if (strcmp(out, "")) {
		fprintf(stderr, "customer set serialization: out string must "
				"be empty.\n");
		return -1;
	}


	for (int i=0; i<self->n; i++) {
		memset(tmp, 0, 1024);
		customer_dump(&self->data[i], tmp);
		strcat(data_str, tmp);
	}

	xmlBufferPtr buf = xmlBufferCreate();

	xmlTextWriterPtr writer = xmlNewTextWriterMemory(buf, 0);
	xmlTextWriterSetIndent(writer, 1);
	xmlTextWriterStartElement(writer, BAD_CAST "customers");

	xmlTextWriterWriteFormatElement(writer, BAD_CAST "number", "%d",
			self->n);

	xmlTextWriterStartElement(writer, BAD_CAST "data");
	xmlTextWriterWriteRaw(writer, data_str);
	xmlTextWriterEndElement(writer);

	xmlTextWriterEndElement(writer);
	xmlFreeTextWriter(writer);

	strcpy(out, buf->content);
	xmlBufferFree(buf);

	return strlen(out);
}

customer_t customer_load(xmlDocPtr doc, xmlNodePtr cur)
{
	customer_t customer;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		xmlChar *content;

		if (!strcmp(cur->name, "name")) {
			content = xmlNodeListGetString(doc,
					cur->xmlChildrenNode, 1);
			sscanf(content, "%100[A-Za-z ]", customer.name);
		} else if (!strcmp(cur->name, "email")) {
			content = xmlNodeListGetString(doc,
					cur->xmlChildrenNode, 1);
			sscanf(content, "%100[A-Za-z @.0-9]", customer.email);
		} else if (!strcmp(cur->name, "age")) {
			content = xmlNodeListGetString(doc,
					cur->xmlChildrenNode, 1);
			sscanf(content, "%d", &customer.age);
		} else if (!strcmp(cur->name, "sex")) {
			content = xmlNodeListGetString(doc,
					cur->xmlChildrenNode, 1);
			sscanf(content, "%c", &customer.sex);
		}

		cur = cur->next;
	}

	return customer;
}

customers_t customers_load(xmlDocPtr doc, xmlNodePtr cur)
{
	customers_t customers;
	int i = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		xmlChar *content;
		if (!strcmp(cur->name, "number")) {
			content = xmlNodeListGetString(doc,
					cur->xmlChildrenNode, 1);
			sscanf(content, "%d", &customers.n);
		}
		if (!strcmp(cur->name, "data")) {
			xmlNodePtr cur2 = cur->xmlChildrenNode;
			while (cur2 != NULL) {
				if (!strcmp(cur2->name, "customer")) {
					customer_t new_customer = customer_load(
							doc, cur2);
					customers.data[i] = new_customer;
					i++;
				}
				cur2 = cur2->next;
			}
		}
		cur = cur->next;
	}

	return customers;
}
