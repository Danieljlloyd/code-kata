#include <customer.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <string.h>

#define SERIALIZE_FILE "customers.xml"

customers_t create_customers(void)
{
	customers_t customers;

	/* Create three customers. */
	customer_t daniel = customer_create("Daniel Lloyd",
			"DanielLloyd7@gmail.com", 24, 'm');

	customer_t shekar = customer_create("Shekar Seldon",
			"ss.dorji@gmail.com", 24, 'f');

	customer_t dad = customer_create("Steven Lloyd",
			"steveljohnlloyd@gmail.com", 57, 'm');

	/* Insert them into the customers array. */
	customers_update(&customers, daniel);
	customers_update(&customers, shekar);
	customers_update(&customers, dad);

	return customers;
}

int dump_customers(customers_t customers, char *filename)
{
	/* Serialise the customers array into an XML tree. */
	char customers_str[4096];
	int len = customers_dump(&customers, customers_str);

	/* Dump the XML tree to file. */
	unlink(filename);
	int fd = open(filename, O_CREAT | O_WRONLY, S_IWUSR | S_IRUSR);
	write(fd, customers_str, len);

	return 0;
}

customers_t read_customers(char *filename)
{
	/* Parse the XML file. */
	xmlDoc *doc = xmlReadFile(filename, NULL, 0);

	/* Go to the root element of the document. */
	xmlNode *root_element = xmlDocGetRootElement(doc);

	/* Create an array of customers. */
	customers_t customers = customers_load(doc, root_element);

	/* Do libxml cleanup. */
	xmlFreeDoc(doc);
	xmlCleanupParser();

	return customers;
}

int print_customers(customers_t customers)
{
	/* Print the customers, one by one. */
	customer_t daniel = customers_get(&customers, "Daniel Lloyd");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", daniel.name,
			daniel.email, daniel.age, daniel.sex);

	customer_t steven = customers_get(&customers, "Steven Lloyd");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", steven.name,
			steven.email, steven.age, steven.sex);

	customer_t shekar = customers_get(&customers, "Shekar Seldon");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", shekar.name,
			shekar.email, shekar.age, shekar.sex);

	return 0;
}

int main(int argc, char *argv[])
{
	/* Create an array of customers. */
	customers_t customers = create_customers();

	/* Dump the customers into an XML file. */
	dump_customers(customers, SERIALIZE_FILE);

	/* Read the customers back from the XML file. */
	customers = read_customers(SERIALIZE_FILE);

	/* Print the customers from memory. */
	print_customers(customers);
}
