#!/bin/bash

SERVER=13.55.89.103

# Source the variables specific to this server.
source vars.sh

# Write a templated configuration file to a server.
echo "Writing some templated configuration file to the server..."
envsubst < example.template | ssh ubuntu@$SERVER "cat > ~/tmpfile"
ssh -t ubuntu@$SERVER "sudo mv tmpfile /tmp/tmpfile"

# Print out a custom hello world html doc.
envsubst < hello.html.template
