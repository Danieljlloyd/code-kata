import sheets_utils as su
import json

spreadsheet_id = '1hGG_9vIysApFKdMhKIHJjBkf-ZCjLYijE5CxB7Y14yY'
range_name = 'Expenses!A1:G1000'

service = su.create_service()
spreadsheet = su.get_spreadsheet(service, spreadsheet_id, range_name)

print(json.dumps(spreadsheet, indent=2))
