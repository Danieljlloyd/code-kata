#include <string>
#include <iostream>
#include <vector>

class IBillingStrategy
{
	public:
	virtual double getActualPrice(double rawPrice) = 0;
};

////////////////////////////////////////////////////////////////////////////////

class NormalStrategy : public IBillingStrategy
{
	public:
	double getActualPrice(double rawPrice);
};

double NormalStrategy::getActualPrice(double rawPrice)
{
	return rawPrice;
}

////////////////////////////////////////////////////////////////////////////////

class HappyHourStrategy : public IBillingStrategy
{
	public:
	double getActualPrice(double rawPrice);
};

double HappyHourStrategy::getActualPrice(double rawPrice)
{
	return rawPrice * 0.5;
}

////////////////////////////////////////////////////////////////////////////////
class Customer
{
	private:
	std::string m_name;
	std::vector<double> m_drinks;
	IBillingStrategy *m_strategy;

	public:
	Customer(std::string);
	void setStrategy(IBillingStrategy *);
	void addDrinks(double price, int quantity);
	void printBill();
};

Customer::Customer(std::string name)
{
	m_strategy = NULL;
	m_name = name;
}

void Customer::setStrategy(IBillingStrategy *newStrategy)
{
	m_strategy = newStrategy;
}

void Customer::addDrinks(double price, int quantity)
{
	m_drinks.push_back(m_strategy->getActualPrice(price * quantity));
}

void Customer::printBill()
{
	double sum = 0;

	for (auto &it : m_drinks) {
		sum += it;
	}

	std::cout << "Total due for " << m_name << ": " << sum << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	Customer firstCustomer = Customer("Harry");
	Customer secondCustomer = Customer("Todd");

	NormalStrategy normalStrategy = NormalStrategy();
	HappyHourStrategy happyHourStrategy = HappyHourStrategy();

	firstCustomer.setStrategy(&normalStrategy);
	secondCustomer.setStrategy(&normalStrategy);

	firstCustomer.addDrinks(1.0, 1);

	firstCustomer.setStrategy(&happyHourStrategy);
	secondCustomer.setStrategy(&happyHourStrategy);

	firstCustomer.addDrinks(1.0, 2);
	secondCustomer.addDrinks(0.8, 1);

	firstCustomer.setStrategy(&normalStrategy);
	secondCustomer.setStrategy(&normalStrategy);

	secondCustomer.addDrinks(1.3, 2);
	secondCustomer.addDrinks(2.5, 1);

	firstCustomer.printBill();
	secondCustomer.printBill();

	return 0;
}
