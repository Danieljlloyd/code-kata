#include <iostream>
#include <string>
#include "Employee.h"

struct EmployeeImpl
{
	std::string name;
	std::string id;
};

void EmployeeHiddenMethod(struct EmployeeImpl *p_impl)
{
	std::cout << "My new name is " << p_impl->name << std::endl;
}

Employee::Employee(std::string name, std::string id)
{
	p_impl = (EmployeeImpl *) malloc(sizeof(struct EmployeeImpl));
	p_impl->name = name;
	p_impl->id = id;
}

Employee::~Employee()
{
	free(p_impl);
};

std::string Employee::getName() const
{
	return p_impl->name;
}

std::string Employee::getId() const
{
	return p_impl->id;
}

void Employee::setName(std::string name)
{
	p_impl->name = name;
	EmployeeHiddenMethod(p_impl);
}
