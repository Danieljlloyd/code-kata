#ifndef __EMPLOYEE_H__
#define __EMPLOYEE_H__

#include <string>
#include <memory>

struct EmployeeImpl;

class Employee
{
public:
	Employee(std::string name, std::string id);
	~Employee();
	std::string getName() const;
	std::string getId() const;
	void setName(std::string name);

private:
	EmployeeImpl *p_impl;
};

#endif
