#include <iostream>
#include "Employee.h"

int main(void)
{
	Employee e1("Daniel", "E007");
	Employee e2("John", "E425");

	std::cout << e1.getName() << ':' << e1.getId() << '\n';
	std::cout << e2.getName() << ':' << e2.getId() << '\n';

	e1.setName("Lloyd");
	std::cout << e1.getName() << ':' << e1.getId() << '\n';
	std::cout << e2.getName() << ':' << e2.getId() << '\n';
}
