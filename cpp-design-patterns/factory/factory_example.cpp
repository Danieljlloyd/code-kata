#include <iostream>
#include <string>
#include <memory>

class Animal
{
public:
	virtual void say() = 0;
	virtual ~Animal() {};
};

class Cat : public Animal
{
public:
	void say();
	~Cat();
};

void Cat::say()
{
	std::cout << "Meow!!" << std::endl;
}

Cat::~Cat()
{
	std::cout << "Deleting cat." << std::endl;
}

class Dog : public Animal
{
public:
	void say();
	~Dog();
};

void Dog::say()
{
	std::cout << "Woof!" << std::endl;
}

Dog::~Dog()
{
	std::cout << "Deleting dog." << std::endl;
}


/*
Say something along the lines of "It is expected that the caller of this factory
takes responsibility for managing the memory of the objects that it receives."
*/
class AnimalFactory
{
public:
	Animal *createAnimal(std::string type);
};

Animal *AnimalFactory::createAnimal(std::string type)
{
	Animal *p_animal = NULL;

	if (type == "cat")
		p_animal = new Cat();
	else if (type == "dog")
		p_animal = new Dog();

	return p_animal;
}

int main(int argc, char *argv[])
{
	AnimalFactory factory = AnimalFactory();

	std::unique_ptr<Animal> dog(factory.createAnimal("dog"));

	dog->say();

	std::unique_ptr<Animal> cat(factory.createAnimal("cat"));

	cat->say();
}
