#include <iostream>
#include <string>

class SerialPort
{
public:
	virtual std::string read(void) = 0;
};

class SerialPortA : public SerialPort
{
public:
	std::string read(void);
};

std::string SerialPortA::read()
{
	return "Reading from the production serial port.";
}

class SerialPortB : public SerialPort
{
public:
	std::string read(void);
};

std::string SerialPortB::read()
{
	return "Reading from a mock serial port for testing.";
}

class Device
{
public:
	void setSerialPort(SerialPort *);
	std::string read(void);
private:
	SerialPort *m_serialPort;
};

void Device::setSerialPort(SerialPort *serialPort)
{
	m_serialPort = serialPort;
}

std::string Device::read()
{
	return m_serialPort->read();
}

int main(void)
{
	Device device = Device();

	SerialPortA serialPortA = SerialPortA();
	SerialPortB serialPortB = SerialPortB();

	device.setSerialPort(&serialPortA);
	std::cout << device.read() << std::endl;

	device.setSerialPort(&serialPortB);
	std::cout << device.read() << std::endl;
}
