"""
Prototype script demonstrating the use of an MQTT broker.
"""

from Queue import Queue
import threading
import time
import random

import paho.mqtt.client as mqtt

TOPIC = 'my-topic'
g_msgq = Queue()

class Watcher(object):
    """
    Class responsible for watching the broker and publishing messages to stdout.
    """

    def __init__(self):
        """
        Create a connection to the broker and start processing messages
        asynchronously.
        """

        self.client = mqtt.Client(
            clean_session=True,
            userdata=self,
            protocol=mqtt.MQTTv31
        )

        self.client.on_connect = Watcher.on_connect
        self.client.on_message = Watcher.on_message

        self.client.connect_async(
            host='localhost',
            port=1883
        )
        self.client.loop_start()

    @staticmethod
    def on_connect(client, userdata, flags, rc):
        userdata.client.subscribe(TOPIC)

    @staticmethod
    def on_message(client, userdata, msg):
        g_msgq.put('{0}: {1}'.format(msg.topic, msg.payload))

if __name__ == '__main__':
    watcher = Watcher()

    client = mqtt.Client(
        clean_session=True,
        userdata={},
        protocol=mqtt.MQTTv31
    )

    client.connect_async(
        host='localhost',
        port=1883
    )

    client.loop_start()
    time.sleep(1)

    while True:
        # Publish a message
        num = random.randint(0, 100)
        client.publish(TOPIC, num)

        # Read the messages back
        time.sleep(1)

        if g_msgq.empty():
            print('No message')

        while not g_msgq.empty():
            msg = g_msgq.get()
            print(msg)
