/*
The proxy pattern wraps an already existing class in order to control access to
it or extend the functionality. In this example we have a device which is able
to just set registers and we are adding extra functionality that allows it
to set a sequence of registers.
*/

#include <stdio.h>
#include <string.h>

struct old_device {
	int (*command) (struct old_device *, char *command_key,
		double command_value);
};

struct new_device {
	struct old_device old_dev;
	int (*limit_output) (struct new_device *dev, double value);
	int (*send_setpoint) (struct new_device *dev, double value);
};

int old_abb_inverter_command(struct old_device *dev, char *command_key,
	double command_value)
{
	if (!strcmp(command_key, "unlock")) {
		puts("Sending unlock command.");
	} else if (!strcmp(command_key, "limit")) {
		printf("Sending limit command with value %lf.\n",
			command_value);
	} else if (!strcmp(command_key, "setpoint")) {
		printf("Sending setpoint command with value %lf.\n",
			command_value);
	}
}

int old_abb_inverter_init(struct old_device *dev)
{
	dev->command = old_abb_inverter_command;
}

int new_abb_limit_output(struct new_device *dev, double value)
{
	dev->old_dev.command(&dev->old_dev, "unlock", 0.0);
	dev->old_dev.command(&dev->old_dev, "limit", value);
}

int new_abb_send_setpoint(struct new_device *dev, double value)
{
	dev->old_dev.command(&dev->old_dev, "unlock", 0.0);
	dev->old_dev.command(&dev->old_dev, "setpoint", value);
}

int new_abb_inverter_init(struct new_device *dev)
{
	old_abb_inverter_init(&dev->old_dev);
	dev->limit_output = new_abb_limit_output;
	dev->send_setpoint = new_abb_send_setpoint;
}

int main(void)
{
	struct new_device abb;
	new_abb_inverter_init(&abb);

	abb.limit_output(&abb, 20);
	abb.send_setpoint(&abb, 12);
}
