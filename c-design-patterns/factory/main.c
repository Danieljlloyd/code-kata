#include "drivers.h"

int main(void)
{
	struct device devices[2] = {
		create_device("canbus"),
		create_device("modbus")
	};

	for (int i=0; i<2; i++) {
		struct device dev = devices[i];

		dev.read(&dev);
		dev.write(&dev);
	}
}
