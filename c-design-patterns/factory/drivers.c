#include <stdio.h>
#include <string.h>
#include "drivers.h"

int modbus_device_read(struct device *dev)
{
	printf("Reading from modbus device with slave id %d.\n",
		dev->u.modbus.slave_id);
}

int modbus_device_write(struct device *dev)
{
	printf("Writing to modbus device with slave id %d.\n",
		dev->u.modbus.slave_id);
}

struct device create_modbus_device(void)
{
	struct device dev;

	dev.read = modbus_device_read;
	dev.write = modbus_device_write;
	dev.u.modbus.slave_id = 24;

	return dev;
}

int canbus_device_read(struct device *dev)
{
	printf("Reading packets canbus device with message ids %d and %d.\n",
		dev->u.canbus.msg_ids[0], dev->u.canbus.msg_ids[1]);
}

int canbus_device_write(struct device *dev)
{
	printf("Writing packet to canbus device with message id %d.\n",
		dev->u.canbus.write_msg_id);
}

struct device create_canbus_device(void)
{
	struct device dev;

	dev.read = canbus_device_read;
	dev.write = canbus_device_write;
	dev.u.canbus.msg_ids[0] = 24;
	dev.u.canbus.msg_ids[1] = 86;
	dev.u.canbus.write_msg_id = 104;

	return dev;
}

struct device create_device(char *identifier)
{
	struct device new_device = {0};

	if (!strcmp(identifier, "canbus")) {
		new_device = create_canbus_device();
	} if (!strcmp(identifier, "modbus")) {
		new_device = create_modbus_device();
	}

	return new_device;
}
