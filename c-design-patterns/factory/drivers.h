#ifndef _DRIVERS_H_
#define _DRIVERS_H_

struct modbus_device {
	int slave_id;
};

struct canbus_device {
	int msg_ids[100];
	int write_msg_id;
};

struct device {
	int (*read) (struct device *);
	int (*write) (struct device *);

	union {
		struct modbus_device modbus;
		struct canbus_device canbus;
	} u;
};

struct device create_device(char *identifier);

#endif
