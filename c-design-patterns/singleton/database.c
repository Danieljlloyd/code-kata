#include <stdio.h>
#include <stdlib.h>

struct database {
	int calls;
};

int database_get_all_customers(struct database *db)
{
	db->calls++;
}

struct database *get_database_instance(void)
{
	static struct database *db = NULL;

	if (db == NULL) {
		db = malloc(sizeof(struct database));
		db->calls = 0;
	}

	return db;
}

int main(void)
{
	struct database *db1 = get_database_instance();
	struct database *db2 = get_database_instance();

	database_get_all_customers(db1);
	database_get_all_customers(db2);

	printf("Database 1 has been called %d times.\n", db1->calls);
	printf("Database 2 has been called %d times.\n", db2->calls);

	return 0;
}
