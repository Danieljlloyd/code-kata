#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

struct controller;
struct controller *controller_create(int, int);
int controller_destroy(struct controller *);
int controller_transition(struct controller *ctrl, int soc);
int controller_get_relay_state(struct controller *ctrl);
int controller_get_current_measurement(struct controller *ctrl);

#endif
