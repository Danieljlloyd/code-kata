#include <stdio.h>
#include <stdlib.h>
#include "controller.h"

struct controller_on_state {
        int current_measurement;
};

struct controller_state {
        int (*get_relay_state) (struct controller_state *);
	int (*measure_current) (struct controller_state *);

	union {
		struct controller_on_state on;
	} u;
};

struct controller {
        struct controller_state *current_state;
	struct controller_state on_state;
	struct controller_state off_state;
	int higher_bound;
	int lower_bound;
};

int on_state_get_relay(struct controller_state *on)
{
	return 1;
}

int on_state_measure_current(struct controller_state *on)
{
	return on->u.on.current_measurement;
}

struct controller_state create_on_state()
{
	struct controller_state on_state;
	on_state.get_relay_state = on_state_get_relay;
	on_state.measure_current = on_state_measure_current;
	on_state.u.on.current_measurement = 100;
	return on_state;
}

int off_state_get_relay(struct controller_state *off)
{
	return 0;
}

int off_state_measure_current(struct controller_state *off)
{
	return 0;
}

struct controller_state create_off_state()
{
	struct controller_state off_state;
	off_state.get_relay_state = off_state_get_relay;
	off_state.measure_current = off_state_measure_current;
	return off_state;
}

int controller_transition(struct controller *ctrl, int soc)
{
	if (!ctrl->current_state)
		ctrl->current_state = &ctrl->off_state;

	if (ctrl->current_state == &ctrl->off_state) {
		if (soc > ctrl->higher_bound)
			ctrl->current_state = &ctrl->on_state;
	} else if (ctrl->current_state == &ctrl->on_state) {
		if (soc < ctrl->lower_bound)
			ctrl->current_state = &ctrl->off_state;
	}

	return 0;
}

int controller_get_relay_state(struct controller *ctrl)
{
	return ctrl->current_state->get_relay_state(ctrl->current_state);
}

int controller_get_current_measurement(struct controller *ctrl)
{
	return ctrl->current_state->measure_current(ctrl->current_state);
}

int controller_init(struct controller *ctrl, int higher_bound,
	int lower_bound)
{
	ctrl->on_state = create_on_state();
	ctrl->off_state = create_off_state();
	ctrl->current_state = &ctrl->off_state;
	ctrl->higher_bound = higher_bound;
	ctrl->lower_bound = lower_bound;

	return 0;
}

struct controller *controller_create(int higher_bound, int lower_bound)
{
	struct controller *ctrl = malloc(sizeof(struct controller));
	controller_init(ctrl, higher_bound, lower_bound);
	return ctrl;
}

int controller_destroy(struct controller *ctrl)
{
	free(ctrl);
}
