#include <stdio.h>
#include "controller.h"

int main(void)
{
	struct controller *ctrl = controller_create(80, 90);

	printf("Relay state: %d\n", controller_get_relay_state(ctrl));
	printf("Current: %d\n", controller_get_current_measurement(ctrl));

	controller_transition(ctrl, 99);

	printf("Relay state: %d\n", controller_get_relay_state(ctrl));
	printf("Current: %d\n", controller_get_current_measurement(ctrl));

	controller_transition(ctrl, 70);
	printf("Relay state: %d\n", controller_get_relay_state(ctrl));
	printf("Current: %d\n", controller_get_current_measurement(ctrl));

	controller_destroy(ctrl);

	return 0;
}
