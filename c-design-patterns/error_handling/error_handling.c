#include <stdio.h>
#include <string.h>
#include <setjmp.h>

typedef enum my_error_t {
	DIVIDE_BY_ZERO,
	INVALID_POINTER
} my_error_t;

static jmp_buf my_exception_env;
static my_error_t my_errno;
int my_perror(char *);

int divide_numbers(double a, double b, double *ans);

int main(int argc, char *argv[])
{
	int result = 0;
	double ans = 0;

	if (divide_numbers(10.0, 2.0, &ans)) {
		my_perror("Failed to divide 10 by 2");
		goto fail_main;
	}

	printf("10 / 2 is %lf\n", ans);

	if (divide_numbers(10.0, 0.0, &ans)) {
		my_perror("Failed to divide 10 by 0");
		goto fail_main;
	}

	printf("10 / 0 is %lf\n", ans);

fail_main:
	return result;
}

int divide_numbers(double a, double b, double *ans)
{
	int result = 0;

	if (ans == NULL) {
		my_errno = INVALID_POINTER;
		result = -1;
		goto fail_divide_numbers;
	}

	if (b == 0) {
		my_errno = DIVIDE_BY_ZERO;
		result = -1;
		goto fail_divide_numbers;
	}

	*ans = a / b;

fail_divide_numbers:
	return result;
}

int my_perror(char *msg)
{
	int result = 0;

	if (msg == NULL) {
		my_errno = INVALID_POINTER;
		result = -1;
		goto fail_my_perror;
	}

	char error_string[100];

	switch(my_errno) {
		case DIVIDE_BY_ZERO:
			strcpy(error_string, "Tried to divide by zero");
			break;
		default:
			strcpy(error_string, "");
	}

	printf("%s: %s\n", msg, error_string);

fail_my_perror:
	return result;
}
