#include <iostream>
#include <string>
#include <memory>

/*
Base device class.
*/
class Device
{
	public:
		Device(){};
		virtual ~Device(){};
		virtual void write(std::string)=0;
		virtual std::string read(int)=0;
};

/*
Specific device class.
*/
class MyDevice : public Device
{
	public:
		MyDevice();
		~MyDevice();
		void write(std::string);
		std::string read(int);
};

MyDevice::MyDevice()
{
	std::cout << "Creating a device" << std::endl;
}

MyDevice::~MyDevice()
{
	std::cout << "Destroying a device" << std::endl;
}

void MyDevice::write(std::string command)
{
	std::cout << "Writing: " << command << std::endl;
}

std::string MyDevice::read(int character_count)
{
	std::cout << "Reading " << character_count << " characters"
		<< std::endl;

	return std::string("");
}

/*
Device factory class.
*/
class DeviceFactory
{
	public:
		DeviceFactory();
		~DeviceFactory();
		std::unique_ptr<Device> create(std::string);
};

DeviceFactory::DeviceFactory() {}
DeviceFactory::~DeviceFactory() {}

std::unique_ptr<Device> DeviceFactory::create(std::string identifier)
{
	if (identifier == "MyDevice")
		return std::make_unique<MyDevice>();

	return NULL;
}


int main(int argc, char *argv[])
{
	DeviceFactory device_factory = DeviceFactory();

	std::string identifier("MyDevice");
	std::unique_ptr<Device> my_device = device_factory.create(identifier);

	my_device->write("Command string");
	my_device->read(10);

	return 0;
}
