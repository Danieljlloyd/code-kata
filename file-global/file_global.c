static int my_var;

int get_my_var()
{
	return my_var;
}

int set_my_var(int val)
{
	my_var = val;

	return 0;
}
