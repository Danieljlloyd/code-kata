#include <stdio.h>
#include "file_global.h"

extern int my_var;

int main(void)
{
	set_my_var(5);
	my_var = 10;

	printf("%d\n", get_my_var());
}
