#include <iostream>
#include <string>
#include <Eigen/Dense>

using Eigen::MatrixXd;

int main()
{
	MatrixXd m(2, 2);

	for (int i=0; i<2; i++) {
		std::string numString;

		getline(std::cin, numString, ',');
		m(i, 0) = std::stod(numString);

		getline(std::cin, numString);
		m(i, 1) = std::stod(numString);
	}

	std::cout << m << std::endl;
}
