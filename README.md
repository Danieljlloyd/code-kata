# Code Kata

This repository is used for documenting prototypes and explorations as I
learn how to use new technologies. Eventually this will be a point of reference
whenever new work is to be done.