import curses
import time

def init():
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(1)
    return stdscr

def exit(stdscr):
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()

def main():
    stdscr = init()
    stdscr.addstr(0, 0, 'Hello world!')
    stdscr.refresh()
    time.sleep(5)
    exit(stdscr)

if __name__ == '__main__':
    main()
