#include <stdio.h>
#include "logging.h"

void enable_verbose_logging()
{
	__VERBOSE_ENABLED = 1;
}

void disable_verbose_logging()
{
	__VERBOSE_ENABLED = 0;
}

void __logger_update_time_string()
{
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	snprintf(__LOGGER_TIMESTRING_BUFFER, 50, "%d-%d-%d %d:%d:%d",
		tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
		tm.tm_min, tm.tm_sec);
}
