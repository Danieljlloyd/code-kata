#ifndef __LOGGING_H__
#define __LOGGING_H__

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

char __LOGGER_TIMESTRING_BUFFER[50] = {0};
char __VERBOSE_ENABLED = 0;

void __logger_update_time_string();
void enable_verbose_logging();
void disable_verbose_logging();

#define print_log_exec_info(stream, ...) 						\
	__logger_update_time_string();							\
	fprintf(stream, "%s: %s: %s: %d: ", __LOGGER_TIMESTRING_BUFFER, \
		__FILE__, __func__, __LINE__);

#define log_message(stream, ...)	\
	print_log_exec_info(stream);	\
	fprintf(stream,  __VA_ARGS__);	\
	fprintf(stream, "\n");

#define log_print(...)				\
	log_message(stdout, __VA_ARGS__)

#define log_vprint(...)					\
	if (__VERBOSE_ENABLED) {			\
		log_message(stdout, __VA_ARGS__)	\
	}

#define log_error(...)				\
	log_message(stderr, __VA_ARGS__)

#define log_errexit(...)		\
	log_error(__VA_ARGS__)		\
	exit(-1)

#endif
