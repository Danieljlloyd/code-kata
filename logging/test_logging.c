#include "logging.h"

void other_function(void)
{
	log_print("I'm in another function");
}

int main(void)
{
	log_error("Failed calling something(%d, \"%s\")", 4, "Some string");
	log_print("Some interesting thing happened");
	other_function();
	log_vprint("Something happened but it's not important");
	enable_verbose_logging();
	log_vprint("Verbose prints are now enabled");
	log_errexit("Some critical failure");
}
