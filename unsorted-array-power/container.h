#ifndef _CONTAINER_EXAMPLE
#define _CONTAINER_EXAMPLE

/*
 * Demonstration of how no container is any more powerful (expressive in it's
 * interface) than an unsorted array container. In many cases, we are dealing
 * with data structures of small size and using them to iterate over. Using
 * an unsorted array in this case is the most memory efficient and performant
 * choice. Complex data structures should only be implemented when there is
 * evidence of poor performance using a profiling utility such as gprof.
 */

/* Simple element that has a key and piece of data (integer number) */
struct Element {
	char key[50];
	int value;
};

/* Simple unsorted array container type. */
struct Container {
	struct Element elements[100];
	int n;
};

/* Simple iterator type for the container. */
struct ContainerIterator {
	struct Element element;
	int i;
};

/* Initialisation methods. */
int container_init(struct Container *self);
int container_iterator_init(struct ContainerIterator *self);

/* List interface. */
int container_push(struct Container *self, struct Element element);
struct Element container_pop(struct Container *self);
int container_foreach(struct Container *self, struct ContainerIterator *it);

/* Queue interface. */
int container_pushl(struct Container *self, struct Element element);
struct Element container_popl(struct Container *self);

/* Associative array interface. */
struct Element container_find(struct Container *self, char *key);
int container_update(struct Container *self, struct Element element);

#endif
