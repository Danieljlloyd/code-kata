#include <string.h>
#include "container.h"

/* Find an element in an container by key, simulating a dictionary get. */
struct Element container_find(struct Container *self, char *key)
{
	struct Element element;
	memset(&element, 0, sizeof(struct Element));

	/* Try to find the element in the container. */
	for (int i=0; i<self->n; i++) {
		if (!strcmp(key, self->elements[i].key)) {
			element = self->elements[i];
		}
	}

	return element;
}

/* Append to an container. */
int container_push(struct Container *self, struct Element element)
{
	self->elements[self->n] = element;
	self->n++;
}

/* Pop an element off an container. */
struct Element container_pop(struct Container *self)
{
	struct Element element = self->elements[self->n-1];
	self->n--;

	return element;
}

/* Push an element to the beginning of an container. */
int container_pushl(struct Container *self, struct Element element)
{
	/* Move every element up one. */
	for (int i=self->n; i>0; i--) {
		self->elements[i] = self->elements[i-1];
	}

	/* Insert at the beginning. */
	self->elements[0] = element;

	/* Update the number of elements. */
	self->n++;

	return 0;
}

/* Pop an element from the end of an container. */
struct Element container_popl(struct Container *self)
{
	/* Save the element at the beginning. */
	struct Element element = self->elements[0];

	/* Move every element down. */
	for (int i=0; i<self->n-1; i++) {
		self->elements[i] = self->elements[i+1];
	}

	/* Update the number of elements. */
	self->n--;

	/* Return the element. */
	return element;
}

/* Update a key in the container. */
int container_update(struct Container *self, struct Element element)
{
	for (int i=0; i<self->n; i++) {
		if (self->elements[i].key == element.key) {
			self->elements[i] = element;
			return 0;
		}
	}

	self->elements[self->n] = element;
	self->n++;

	return 1;
}

/* Iterate through the container. Return -1 if we have reached the end. */
int container_foreach(struct Container *self, struct ContainerIterator *it)
{
	if (it->i >= self->n) {
		return -1;
	} else {
		it->element = self->elements[it->i];
		it->i++;
	}

	return 0;
}

/* Initialise the container. */
int container_init(struct Container *self)
{
	self->n = 0;
	memset(self->elements, 0, sizeof(struct Element) * 100);
}

/* Initialise the iterator for the container. */
int container_iterator_init(struct ContainerIterator *self)
{
	memset(&self->element, 0, sizeof(struct Element));
	self->i=0;
}
