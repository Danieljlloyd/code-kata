/* Program demonstrating how every complex container can be simulated as
 * an unsorted array. Unsorted arrays can support push, pop, pushl, popl,
 * find, update. For small N, these are actually the smallest and most
 * performant data structures. You should use these as a first measure and
 * profile the result. Only use more complicated data structures if they are
 * actually of value. */

#include <stdio.h>
#include <string.h>
#include "container.h"

int main(void) {
	struct Container my_container;
	struct ContainerIterator it;
	struct Element my_element;

	/* Act like a dictionary. */
	container_init(&my_container);
	strcpy(my_element.key, "some key");
	my_element.value = 1;
	container_update(&my_container, my_element);

	my_element = container_find(&my_container, "some key");

	printf("\n");
	printf("---------------\n");
	printf("Dictionary test\n");
	printf("---------------\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);

	/* Act like a list. */
	container_init(&my_container);

	strcpy(my_element.key, "key1");
	my_element.value = 1;
	container_push(&my_container, my_element);

	strcpy(my_element.key, "key2");
	my_element.value = 2;
	container_push(&my_container, my_element);

	strcpy(my_element.key, "key3");
	my_element.value = 3;
	container_push(&my_container, my_element);

	puts("");
	puts("---------");
	puts("List test");
	puts("---------");
	puts("");

	container_iterator_init(&it);
	while (!container_foreach(&my_container, &it)) {
		printf("Element %d\n", it.i);
		printf("----------\n");
		printf("key: %s\n", it.element.key);
		printf("value: %d\n", it.element.value);
		printf("\n");
	}

	my_element = container_pop(&my_container);
	printf("Pop 1\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);
	printf("\n");

	my_element = container_pop(&my_container);
	printf("Pop 2\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);
	printf("\n");

	my_element = container_pop(&my_container);
	printf("Pop 3\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);

	/* Act like a queue. */
	container_init(&my_container);

	strcpy(my_element.key, "key1");
	my_element.value = 1;
	container_pushl(&my_container, my_element);

	strcpy(my_element.key, "key2");
	my_element.value = 2;
	container_pushl(&my_container, my_element);

	strcpy(my_element.key, "key3");
	my_element.value = 3;
	container_pushl(&my_container, my_element);

	puts("");
	puts("----------");
	puts("Queue test");
	puts("----------");
	puts("");

	container_iterator_init(&it);
	while (!container_foreach(&my_container, &it)) {
		printf("Element %d\n", it.i);
		printf("----------\n");
		printf("key: %s\n", it.element.key);
		printf("value: %d\n", it.element.value);
		printf("\n");
	}

	my_element = container_popl(&my_container);
	printf("Pop 1\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);
	printf("\n");

	my_element = container_popl(&my_container);
	printf("Pop 2\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);
	printf("\n");

	my_element = container_popl(&my_container);
	printf("Pop 3\n");
	printf("-----\n");
	printf("key: %s\n", my_element.key);
	printf("value: %d\n", my_element.value);

	return 0;
}
