alter table users add column session_data hstore not null default ''::hstore;

update users
	set session_data = session_data || hstore('4567', '64.107.86.0')
	where id = 1;
