#include <stdio.h>
#include <unistd.h>
#include <state_store.h>

int main(void)
{
	char outstr[100];
	char value[_STATE_STORE_VAL_SIZE];
	char *p;
	int rc;
	state_store_t state_store;

	/* Create the state store. */
	state_store_init(&state_store);

	/* Update the state store. */
	state_store_update(&state_store, "my_key", "my_value");
	state_store_update(&state_store, "my_second_key", "another value");
	state_store_update(&state_store, "another_key", "yet another value");

	state_store_find(&state_store, "my_key", value);
	sprintf(outstr, "my_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	state_store_find(&state_store, "my_second_key", value);
	sprintf(outstr, "my_second_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	state_store_find(&state_store, "another_key", value);
	sprintf(outstr, "another_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	/* Persist the state store. */
	state_store_dump(&state_store);

	return 0;
}
