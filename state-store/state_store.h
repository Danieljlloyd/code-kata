/* A high performance associative shared state class that is threadsafe. It is
 * able to be shared between processes with mmap and correctly synchronised
 * using a readers-writers lock. This is safe because the underlying
 * implementation of pthread rwlock uses futex. */

#ifndef _STATE_STORE_H
#define _STATE_STORE_H

#include "pthread.h"

#define _STATE_STORE_KEY_SIZE 50
#define _STATE_STORE_VAL_SIZE 20
#define _STATE_STORE_MAX_PARAMS 1000
#define _STATE_STORE_PERSIST_FILE "/opt/data/state_store.bin"

/* Definition of a key/value pair for the state store. */
typedef struct state_parameter_t {
	char key[_STATE_STORE_KEY_SIZE];
	char value[_STATE_STORE_VAL_SIZE];
} state_parameter_t;

/* State store data structure. */
typedef struct state_store_t {
	struct state_parameter_t data[_STATE_STORE_MAX_PARAMS];
	int n;
	pthread_rwlock_t rwlock;
} state_store_t;


/* Initialise the state store. Clears all memory, set the size to 0 and
 * initializes the rwlock. */
int state_store_init(state_store_t *);

/* Updates a key in the state store with the new value. */
int state_store_update(state_store_t *self, char *key, char *value);

/* Finds a key in the state store if possible and loads it into value. Returns
 * -1 if no key is found. */
int state_store_find(state_store_t *self, char *key, char *value);

/* Dumps the entire state store data structure into a file in binary format. */
int state_store_dump(state_store_t *self);

/* Loads the state store from the persistance file. */
int state_store_load(state_store_t *self);

/* Increment the state store. Mainly used for thread safety tests. */
int state_store_inc(state_store_t *self, char *key);

#endif
