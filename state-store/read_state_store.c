#include <stdio.h>
#include <unistd.h>
#include <state_store.h>

int main(void)
{
	char outstr[100];
	char value[_STATE_STORE_VAL_SIZE];
	char *p;
	int rc;
	state_store_t state_store;

	/* Read the state store. */
	state_store_init(&state_store);

	state_store_load(&state_store);

	state_store_find(&state_store, "my_key", value);

	sprintf(outstr, "my_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	state_store_find(&state_store, "my_second_key", value);

	sprintf(outstr, "my_second_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	state_store_find(&state_store, "another_key", value);

	sprintf(outstr, "another_key: %s\n", value);
	for (p = outstr; *p != '\0'; p++) write(1, p, 1);

	return 0;
}
