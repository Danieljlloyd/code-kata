#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <state_store.h>
#include <pthread.h>

/* Initialise the state store with a completely empty memory space. Take
 * information from the config file. */
int state_store_init(state_store_t *self)
{
	/* Wipe all the data initially. */
	int memsize = sizeof(state_parameter_t) * _STATE_STORE_MAX_PARAMS;
	memset(&self->data, 0, memsize);

	/* Set the parameter count to 0. */
	self->n = 0;

	/* Initialise the rwlock. */
	pthread_rwlock_init(&self->rwlock, NULL);

	return 0;
}

/* Destroy the state store and release all resources. */
int state_store_destroy(state_store_t *self)
{
	/* Wipe all the data. */
	int memsize = sizeof(state_parameter_t) * _STATE_STORE_MAX_PARAMS;
	memset(&self->data, 0, memsize);

	/* Set the parameter count to 0. */
	self->n = 0;

	/* Destroy the rwlock. */
	pthread_rwlock_destroy(&self->rwlock);

	return 0;
}

/* Update the state store value for a particular key. */
int state_store_update(state_store_t *self, char *key, char *value)
{
	int i;

	pthread_rwlock_wrlock(&self->rwlock);

	/* Try to find a parameter with the same key. */
	for (i=0; i<self->n; i++) {
		/* If found, update with the new value and return. */
		if (!strcmp(key, self->data[i].key)) {
			strcpy(self->data[i].value, value);
			return 0;
		}
	}

	/* If the key does not exist in the state store currently, create a
	 * new parameter. */
	strcpy(self->data[self->n].key, key);
	strcpy(self->data[self->n].value, value);

	/* Update the parameter count. */
	self->n++;

	pthread_rwlock_unlock(&self->rwlock);

	return 0;
}

/* Find a particular key in the state store and copy the value into the value
 * parameter given. */
int state_store_find(state_store_t *self, char *key, char *value)
{
	int i;

	pthread_rwlock_rdlock(&self->rwlock);

	for (i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].key, key)) {
			strcpy(value, self->data[i].value);
			return 0;
		}
	}

	pthread_rwlock_unlock(&self->rwlock);

	return -1;
}

/* Dump the state store into the state store persistance file. */
int state_store_dump(state_store_t *self)
{
	int fd;

	fd = open(_STATE_STORE_PERSIST_FILE, O_WRONLY | O_CREAT);

	flock(fd, LOCK_EX);
	pthread_rwlock_rdlock(&self->rwlock);

	write(fd, self, sizeof(state_store_t));

	pthread_rwlock_unlock(&self->rwlock);
	flock(fd, LOCK_UN);

	close(fd);

	return 0;
}

/* Load the state store from the state store persistance file. */
int state_store_load(state_store_t *self)
{
	int fd;

	fd = open(_STATE_STORE_PERSIST_FILE, O_RDONLY | O_CREAT);

	flock(fd, LOCK_EX);
	pthread_rwlock_wrlock(&self->rwlock);

	read(fd, self, sizeof(state_store_t));

	pthread_rwlock_unlock(&self->rwlock);
	flock(fd, LOCK_UN);

	close(fd);

	return 0;
}

/* Increment a variable. Mostly used to demonstrate threadsafe behaviour. */
int state_store_inc(state_store_t *self, char *key)
{
	char tmpstr[100];
	int tmp;

	pthread_rwlock_wrlock(&self->rwlock);
	for (int i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].key, key)) {
			strcpy(tmpstr, self->data[i].value);
			sscanf(tmpstr, "%d", &tmp);

			tmp++;

			sprintf(tmpstr, "%d", tmp);
			strcpy(self->data[i].value, tmpstr);
		}
	}
	pthread_rwlock_unlock(&self->rwlock);
}
