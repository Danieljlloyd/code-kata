#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <state_store.h>

typedef struct {
	state_store_t *state_store;
	int loops;
} shared_state_t;

static void *my_thread(void *arg)
{
	shared_state_t *my_shared_state = arg;

	int i;
	for (i=0; i<my_shared_state->loops; i++)
		state_store_inc(my_shared_state->state_store, "mykey");
}

int main(void)
{
	pthread_t t1, t2;
	int loops = 10000000;
	char result[50];

	memset(result, 0, 50);

	state_store_t state_store;
	state_store_init(&state_store);

	state_store_update(&state_store, "mykey", "0");

	shared_state_t shared_state = {
		.state_store = &state_store,
		.loops = loops
	};

	pthread_create(&t1, NULL, my_thread, &shared_state);
	pthread_create(&t2, NULL, my_thread, &shared_state);

	pthread_join(t1, NULL);
	pthread_join(t2, NULL);

	state_store_find(&state_store, "mykey", result);

	puts(result);
}
