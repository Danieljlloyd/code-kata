#include <stdio.h>

struct Animal;

struct Dog {
	int numberOfDogFoodTinsEaten;
	void (*eatDogFood) (struct Animal *self);
};

struct Cat {
	int numberOfFishEaten;
	void (*eatFish) (struct Animal *self);
};

/* Note how we are able to use unions for inheritance. */
struct Animal {
	void (*talk) (struct Animal *self);
	union {
		struct Dog dog;
		struct Cat cat;
	} u;
};

void Dog_eatDogFood(struct Animal *self)
{
	self->u.dog.numberOfDogFoodTinsEaten++;
}

void Dog_talk(struct Animal *self)
{
	printf("Woof! I'm a dog and I have eaten %d cans of dog food.",
			self->u.dog.numberOfDogFoodTinsEaten);
}

void Dog_New(struct Animal *self)
{
	self->talk = Dog_talk;

	self->u.dog.eatDogFood = Dog_eatDogFood;
	self->u.dog.numberOfDogFoodTinsEaten = 0;
}

void Cat_eatFish(struct Animal *self)
{
	self->u.cat.numberOfFishEaten++;
}

void Cat_talk(struct Animal *self)
{
	printf("Meowwww! I'm a cat and I have eaten %d fish.\n",
			self->u.cat.numberOfFishEaten);
}

void Cat_New(struct Animal *self)
{
	self->talk = Cat_talk;

	self->u.cat.eatFish = Cat_eatFish;
	self->u.cat.numberOfFishEaten = 0;
}

int main(void)
{
	struct Animal myCat;
	Cat_New(&myCat);
	myCat.u.cat.eatFish(&myCat);
	myCat.u.cat.eatFish(&myCat);

	struct Animal myDog;
	Dog_New(&myDog);
	myDog.u.dog.eatDogFood(&myDog);
	myDog.u.dog.eatDogFood(&myDog);
	myDog.u.dog.eatDogFood(&myDog);

	/* Deal with a generic "Animal" class polymorphically */
	struct Animal myAnimals[2];
	myAnimals[0] = myCat;
	myAnimals[1] = myDog;
	for (int i=0; i<2; i++) {
		myAnimals[i].talk(&myAnimals[i]);
	}
}
