/*
The point of polymorphism is to determine the method that you intend to use
at run-time. This can be achieved effectively by putting an enum in the base
class. Switch statement must be maintained, but this is relatively trivial
and obvious. Could also use a registration type of approach, but I doubt it
would be much cleaner.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_MAX 50

enum AnimalKind {ANIMAL_KIND_DOG, ANIMAL_KIND_CAT};

struct AnimalInterface
{
	char species[STR_MAX];
	enum AnimalKind kind;
};

struct Animal
{
	struct AnimalInterface base;
};

struct Dog
{
	struct AnimalInterface base;
};

struct Cat
{
	struct AnimalInterface base;
};

int cat_init(struct Cat *);
int dog_init(struct Dog *);
int cat_say(struct Cat *);
int dog_say(struct Dog *);
int animal_say(struct Animal *);

int main(void)
{
	struct Dog my_dog;
	struct Cat my_cat;

	dog_init(&my_dog);
	cat_init(&my_cat);

	animal_say((struct Animal *) &my_dog);
	animal_say((struct Animal *) &my_cat);
}

int cat_init(struct Cat *self)
{
	strcpy(self->base.species, "cat");
	self->base.kind = ANIMAL_KIND_CAT;
}

int dog_init(struct Dog *self)
{
	strcpy(self->base.species, "dog");
	self->base.kind = ANIMAL_KIND_DOG;
}

int cat_say(struct Cat *self)
{
	puts("Meeeooooowwww!");
}

int dog_say(struct Dog *self)
{
	puts("Woof... Wooof!!!");
}

int animal_say(struct Animal *self)
{
	switch (self->base.kind) {
		case ANIMAL_KIND_CAT:
			cat_say((struct Cat *) self);
			break;
		case ANIMAL_KIND_DOG:
			dog_say((struct Dog *) self);
			break;
		default:
			fprintf(stderr, "animal_say: Could not identify "
				"animal kind.\n");
			exit(-1);
	}
}
