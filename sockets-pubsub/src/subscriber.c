#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

char *socket_path = "/tmp/mysocket";

int main(int argc, char *argv[]) {
	struct sockaddr_un addr;
	char buf[100];
	int fd,rc,ret;

	if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);


	ret = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	if (ret == -1) {
		perror("connect error");
		exit(-1);
	}

	while (1) {
		sleep(1);

		char *ptr = buf;
		while (rc > 0) {
			rc=read(fd,ptr,1);
			ptr++;
			
			printf("read %u bytes: %.*s\n", rc, rc, buf);
		}
		if (rc == -1) {
			perror("read error");
			exit(-1);
		}
	}

	return 0;
}
