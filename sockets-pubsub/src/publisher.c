#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <string.h>

struct subscriber_node {
	int id;
	int sock;
	struct subscriber_node *next;
};

int subscriber_add(struct subscriber_node *, const char *);
struct subscriber_node *subscriber_new(const char *);
int subscriber_list_destroy(struct subscriber_node *);
struct subscriber_node *subscriber_list_create();

int main(int argc, char *argv[]) {
	struct subscriber_node *sub_list = subscriber_list_create();
	struct subscriber_node *sub;
	int rc;
	char buf[1000];
	int count = 0;
	int cl;

	sub = sub_list;

	while (1) {
		if ((cl = accept(sub->sock, NULL, NULL)) == -1) {
			perror("accept error");
		}

		rc = sprintf(buf, "%d", count);

		if (write(cl, buf, rc) != rc) {
			if (rc > 0)
				fprintf(stderr, "partial write");
			else {
				perror("write error");
				exit(-1);
			}
		}

		//int eof = -1;
		//write(cl, (void *) &eof, 1);

		count++;
		printf("%d\n", count);

		sleep(1);
	}

	subscriber_list_destroy(sub_list);

	return 0;
}

int subscriber_add(struct subscriber_node *sub, const char *key) 
{
	while(sub->next != 0)
		sub = sub->next;

	sub->next = subscriber_new(key);

	return 0;
}

struct subscriber_node *subscriber_new(const char *socket_path) 
{
	static int id_count = 0;

	struct subscriber_node *sub = (struct subscriber_node *) malloc(sizeof(struct subscriber_node));
	struct sockaddr_un addr;
	int fd;

	if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
	unlink(socket_path);

	if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
		perror("bind error");
		exit(-1);
	}

	if (listen(fd, 5) == -1) {
		perror("listen error");
		exit(-1);
	}

	sub->next = NULL;
	sub->sock = fd;
	sub->id = id_count++;

	return sub;
}

int subscriber_list_destroy(struct subscriber_node *sub)
{
	struct subscriber_node *prev;

	while(sub->next != 0) {
		prev = sub;
		sub = sub->next;
		free(prev);
	}

	free(sub);

	return 0;
}

struct subscriber_node *subscriber_list_create()
{
	struct subscriber_node *list;

	list = subscriber_new("/tmp/mysocket");

	return list;
}
