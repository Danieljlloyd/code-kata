#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>

void err_exit(char *context) {
	perror(context);
	exit(-1);
}

typedef struct logger_t {
	int fifo_log_fd;
	int level;
} logger_t;

int init_logger(struct logger_t *self, const char *fifoname, int level)
{
	int readfd = -1;
	self->fifo_log_fd = -1;
	self->level = level;

	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		err_exit("signal");
	}

	if (access(fifoname, F_OK) == -1) {
		if (mkfifo(fifoname, 0666) < 0) {
			err_exit("mkfifo");
		}
	}

	if ((readfd = open(fifoname, O_RDONLY | O_NONBLOCK)) < 0) {
		err_exit("open readfd");
	}

	if ((self->fifo_log_fd = open(fifoname, O_WRONLY | O_NONBLOCK)) < 0) {
		err_exit("open writefd");
	}

	close(readfd);

	return 0;
}

int delete_logger(struct logger_t *self)
{
	close(self->fifo_log_fd);
}

void logg(struct logger_t *self, const char *msg, int level) {
	if (level >= self->level) {
		write(1, msg, strlen(msg));
	}

	write(self->fifo_log_fd, msg, strlen(msg));
}

int main(void)
{
	const char *progress_msg = "Writing to log...\n";
	const char *log_msg = "Hello world\n";
	const char *fifoname = "/var/log/mytestlog";

	logger_t log;
	init_logger(&log, fifoname, 20);

	while (1) {
		logg(&log, progress_msg, 30);
		logg(&log, log_msg, 10);
		sleep(1);
	}

fail_main:
	delete_logger(&log);
	return 0;
}
