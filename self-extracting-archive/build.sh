#!/bin/bash

function build_archive {
	dist_dir="$1"
	source_archive="$2"

	pushd $dist_dir
	tar zcvf "../$source_archive" *
	popd
}

function make_self_extracting {
	source_archive="$1"
	dest_file="$2"

	cat <<END > "$dest_file"
#!/bin/bash
DESTDIR=\${DESTDIR:-/}
archive_start=\$(awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' \$0)
tail -n+\$archive_start \$0 | tar zxv -C \$DESTDIR
exit 0
__ARCHIVE_BELOW__
END

	cat $source_archive >> $dest_file

	chmod 755 $dest_file
}

build_archive dist/ hello_application.tar.gz
make_self_extracting hello_application.tar.gz hello_application_installer.sh
rm hello_application.tar.gz
