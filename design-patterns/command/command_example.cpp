#include <iostream>
#include <list>
#include <unistd.h>

class PvInverter
{
	public:
		PvInverter();
		~PvInverter();
		static void curtail_output(PvInverter *, double);
};
PvInverter::PvInverter() {}
PvInverter::~PvInverter(){}

void PvInverter::curtail_output(PvInverter *self, double percentage)
{
	std::cout << "Curtailing PV output power to " << percentage
		<< " percent." << std::endl;
}

class PvCurtailmentCommand
{
	public:
		PvCurtailmentCommand(void(*cb)(PvInverter *, double),
			PvInverter *);
		~PvCurtailmentCommand();
		void execute(double percentage);

	private:
		PvInverter *inverter_p;
		void (*cb) (PvInverter *, double);
};

PvCurtailmentCommand::~PvCurtailmentCommand(){}

PvCurtailmentCommand::PvCurtailmentCommand(void (*cb)(PvInverter *, double),
	PvInverter *inverter_p)
{
	this->inverter_p = inverter_p;
	this->cb = cb;
}

void PvCurtailmentCommand::execute(double percentage)
{
	this->cb(this->inverter_p, percentage);
}

class Controller
{
	public:
		Controller(std::list<PvCurtailmentCommand>);
		~Controller();
		void periodic_processing();

	private:
		std::list<PvCurtailmentCommand> pv_curtailment_commands;
};

Controller::Controller(
	std::list<PvCurtailmentCommand> pv_curtailment_commands)
{
	this->pv_curtailment_commands = pv_curtailment_commands;
}

Controller::~Controller() {}

void Controller::periodic_processing()
{
	double pv_curtailment_percentage = 75.0;

	for (auto it = this->pv_curtailment_commands.begin();
		it != this->pv_curtailment_commands.end(); ++it) {

		it->execute(pv_curtailment_percentage);
	}
}


int main(void)
{
	/* Create the inverter objects. */
	PvInverter sma_inverter;

	/* Create the commands to act on the inverter objects. */
	std::list<PvCurtailmentCommand> pv_curtailment_commands;
	pv_curtailment_commands.push_back(
		PvCurtailmentCommand(PvInverter::curtail_output, &sma_inverter)
	);

	/* Create a controller that has access to the commands. */
	Controller controller(pv_curtailment_commands);

	/* Control the inverters from the controller. The controller has no
		knowledge of the individual inverters. */
	while (1) {
		controller.periodic_processing();
		sleep(1);
	}
}
