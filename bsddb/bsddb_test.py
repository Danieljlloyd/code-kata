import bsddb
import random
import time
from datetime import datetime

MIN_VALUE = 0
MAX_VALUE = 100

def measure(keys):
    measurements = []

    for key in keys:
        measurements.append({
            'measure': key,
            'value': random.randint(0, 100)
            })

    return measurements

def print_db(db):
    for key in db.keys():
        value = db[key]
        print str(datetime.now())+' -- '+key+': '+value

def main():

    keys = [
        'voltage',
        'current',
        'power'
        ]

    db = bsddb.btopen('spam.db')

    while True:
        measurements = measure(keys)

        for measurement in measurements:
            key = measurement['measure']
            value = measurement['value']

            db[key] = str(value)

        print_db(db)

        time.sleep(1)

if __name__ == '__main__':
    main()
