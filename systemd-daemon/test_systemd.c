/* This example is a simple systemd daemon process that exhibits systemd
 * functionality. Processes are able to be written as normal user-space
 * programs and systemd is able to daemonise them, supervise them and do
 * sensible things with their standard file descriptors. */

#include <stdio.h>
#include <unistd.h>

#define PRINT_SIZE 1024

char g_verbose;

/* This level is for things that should always be known. Faults that are not
 * expected to occur and important events fall into this category. */
int print(char *msg)
{
	int i;

	/* Write the message to stdout. Max size given by PRINT_SIZE */
	for (i=0; i<PRINT_SIZE && msg[i] != 0; i++) {
		write(1, &msg[i], 1);
	}
	write(1, "\n", 1);

	if (msg[i] != 0)
		return -1;

	return 0;
}

/* This level is for things that are useful for debugging purposes. Expected
 * faults fall into this area. */
int vprint(char *msg)
{
	int i;

	/* Ignore if not in verbose mode. */
	if (!g_verbose)
		return -1;

	print(msg);

	return 0;
}

int main(int argc, char *argv[])
{
	int count, len;
	char str[1024], c;

	count = 0;

	/* Defaults. */
	g_verbose = 0;

	while ((c = getopt(argc, argv, "v")) != -1) {
		switch (c) {
			case 'v':
				g_verbose = 1;
				break;
			case '?':
				sprintf(str, "Unrecognised option -%c", c);
				print(str);
		}
	}

	while (1) {
		len = sprintf(str, "The test systemd process has been alive"
				" for %d seconds.", count);
		vprint(str);

		if (!(count % 5))
			print("This is where errors go.");

		sleep(1);

		count++;
	}
}
