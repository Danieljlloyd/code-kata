#ifndef MY_ANIMAL_H
#define MY_ANIMAL_H
class Animal
{
	public:
		virtual ~Animal(void);
		virtual void say(void)=0;
};

class Dog : Animal
{
	public:
		Dog(void);
		~Dog(void);
		virtual void say(void);
};

class Cat : Animal
{
	public:
		Cat(void);
		~Cat(void);
		virtual void say(void);
};
#endif
