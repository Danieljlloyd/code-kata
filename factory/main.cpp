#include <iostream>
#include <memory>
#include <list>
#include <animal_factory.hpp>
#include <animal.hpp>

class AnimalController
{
	public:
		AnimalController();
		~AnimalController();
		void listen_to_animals();
	private:
		std::list<std::unique_ptr<Animal>> animals;
};

AnimalController::AnimalController()
{
	AnimalFactory my_factory = AnimalFactory();

	this->animals.push_back(my_factory.create("cat"));
	this->animals.push_back(my_factory.create("dog"));
}

AnimalController::~AnimalController(){}

void AnimalController::listen_to_animals()
{
	for (auto &animal : this->animals)
		animal->say();
}

int main(int argc, char *argv[])
{
	AnimalController animal_controller;
	animal_controller.listen_to_animals();

	return 0;
}
