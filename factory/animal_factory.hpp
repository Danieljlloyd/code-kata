#ifndef MY_ANIMAL_FACTORY_HPP
#define MY_ANIMAL_FACTORY_HPP
#include <memory>
#include <animal.hpp>

class AnimalFactory
{
	public:
		AnimalFactory();
		~AnimalFactory();
		std::unique_ptr<Animal> create(const char *);
};
#endif
