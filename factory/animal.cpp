#include <animal.hpp>
#include <iostream>

Animal::~Animal(){}

Cat::Cat(void){}
Cat::~Cat(void){
	std::cout << "Killing cat." << std::endl;
}
Dog::Dog(void){}
Dog::~Dog(void){
	std::cout << "Killing dog." << std::endl;
}

void Cat::say(void)
{
	std::cout << "Meeeeoowwwwww!" << std::endl;
}

void Dog::say(void)
{
	std::cout << "Arrrfffff!" << std::endl;
}
