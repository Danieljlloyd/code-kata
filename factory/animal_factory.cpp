#include <memory>
#include <animal_factory.hpp>
#include <animal.hpp>
#include <string.h>

AnimalFactory::AnimalFactory() {}
AnimalFactory::~AnimalFactory() {}

std::unique_ptr<Animal> AnimalFactory::create(const char *animal_type)
{
	Animal *new_animal;

	if (!strcmp(animal_type, "dog")) {
		new_animal = (Animal *) new Dog();
	} else if (!strcmp(animal_type, "cat")) {
		new_animal = (Animal *) new Cat();
	}

	return std::unique_ptr<Animal>(new_animal);
}
