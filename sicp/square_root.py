import sys

def average(x, y):
    return (x + y) / 2.0

def improve(guess, x):
    return average(guess, (x / guess))

def good_enough(guess, x):
    return abs(guess**2 - x) < 0.001

def sqrt_iter(guess, x):
    if good_enough(guess, x):
        return guess
    else:
        return sqrt_iter(improve(guess, x), x)

def sqrt(x):
    try:
        return sqrt_iter(1.0, x)
    except RuntimeError as ex:
        sys.stderr.write('Failed to compute sqrt: {0}\n'.format(ex))
        return None

def main():
    print(sqrt(9))
    print(sqrt(100))
    print(sqrt(85))
    print(sqrt(773829918))
    print(sqrt(1022999988727122))
    print(sqrt(82891299283827717271))

if __name__ == '__main__':
    main()
