def square_two_largest(x, y, z):
    if x < y and x < z:
        return y**2 + z**2
    elif y < x and y < z:
        return x**2 + z**2
    elif z < x and z < y:
        return x**2 + y**2

print(square_two_largest(1, 3, 5))
print(square_two_largest(-5, 3, 8))
