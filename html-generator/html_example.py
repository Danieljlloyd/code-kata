#!/usr/local/lib/venv/html-example/bin/python

from html import HTML

class CustomerListViewModel(object):
    def __init__(self, customers):
        self.customers = customers

class CustomerListView(object):
    def generate_html(self, customer_list_view_model):
        doc = HTML()

        head = doc.head()
        head.title('Customers List')

        body = doc.body()
        body.h1('Customers List', klass='customer-list-title')
        customer_list = body.ul(klass='customer-list')

        for customer in customer_list_view_model.customers:
            customer_list.li(customer, klass='customer-list-element')

        return doc

def main():
    # Create a customer view - Normally this is the job of a Presenter class.
    customers = [
        'Daniel',
        'Shekar',
        'Steven',
        'Mathew',
        'Lorraine',
    ]

    view_model = CustomerListViewModel(
        customers=customers
    )

    view = CustomerListView()

    html = view.generate_html(view_model)

    print 'Content-type: text/html\n'
    print html

if __name__ == '__main__':
    main()
