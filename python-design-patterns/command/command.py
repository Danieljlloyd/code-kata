import random
import time

class PvInverter(object):
    def __init__(self, identifier):
        self._id = identifier

    def curtail(self, percentage):
        print('Curtailing output of inverter {0} to {1}%'.format(
            self._id, percentage))

class Controller(object):
    def __init__(self, curtailment_commands):
        self._curtailment_commands = curtailment_commands

    def iterate(self):
        percentage = random.randint(1, 100)

        for command in self._curtailment_commands:
            command(percentage)

def main():
    inverter_one = PvInverter(1)
    inverter_two = PvInverter(2)

    commands = []
    commands.append(lambda pct: inverter_one.curtail(pct))
    commands.append(lambda pct: inverter_two.curtail(pct))

    controller = Controller(commands)

    while True:
        controller.iterate()
        time.sleep(1)

if __name__ == '__main__':
    main()
