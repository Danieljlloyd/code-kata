class Borg(object):
    __shared_state = {}

    def __init__(self):
        self.__dict__ = self.__shared_state
        self._state = None

    def put_state(self, value):
        self._state = value

    def get_state(self):
        return self._state

def main():
    borg1 = Borg()
    borg2 = Borg()

    borg1.put_state(100)
    print borg2.get_state()

if __name__ == '__main__':
    main()
