class Device(object):
    def write(self):
        raise NotImplementedError()

    @staticmethod
    def implements(identifier):
        raise NotImplementedError()

class DeviceOne(Device):
    def write(self):
        print('Writing using device one!')

    @staticmethod
    def implements(identifier):
        return identifier == 'DeviceOne'

class DeviceTwo(Device):
    def write(self):
        print('Writing using device two!')

    @staticmethod
    def implements(identifier):
        return identifier == 'DeviceTwo'

class DeviceFactory(object):
    @staticmethod
    def create(identifier):
        for device in Device.__subclasses__():
            if device.implements(identifier):
                return device()

        return None

def main():
    device_factory = DeviceFactory()

    devices = []
    devices.append(device_factory.create('DeviceOne'))
    devices.append(device_factory.create('DeviceTwo'))

    for device in devices:
        device.write()

if __name__ == '__main__':
    main()
