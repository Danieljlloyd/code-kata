#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	char *request_method = getenv("REQUEST_METHOD");

	if (!strcmp(request_method, "GET")) {
		puts(
"Content-Type: text/html\n\n"
"<html>\n"
"  <head><title>Set Wifi Connection</title></head>\n"
"  <body>\n"
"    <form action=\"http://localhost/cgi-bin/configure_wifi\" "
		"method=\"post\">\n"
"      SSID <input type=\"text\" name=\"ssid\"><br>\n"
"      Passphrase <input type=\"password\" name=\"passphrase\"><br>\n"
"      <input type=\"submit\" name=\"submit\">\n"
"    </form>\n"
"  </body>\n"
"</html>"
		);
	} else if (!strcmp(request_method, "POST")) {
		char *lenstr = getenv("CONTENT_LENGTH");
		int len = strtol(lenstr, NULL, 10);

		char *post_data = malloc(len+1);
		fgets(post_data, len+1, stdin);

		char *ssid;
		char *passphrase;

		strtok(post_data, "=");
		ssid = strtok(NULL, "&");

		strtok(NULL, "=");
		passphrase = strtok(NULL, "&");

		printf("Content-Type: text/plain\n\n");

		puts(
			"Would write the following configuration to "
			"the /etc/wpa_supplicant/wpa_supplicant.conf file.\n\n"
		);

		char wpa_supplicant_config[] =
			"network={\n"
			"	ssid=\"%s\"\n"
			"	scan_ssid=1\n"
			"	key_mgmt=WPA-PSK\n"
			"	psk=\"%s\"\n"
			"}";

		printf(wpa_supplicant_config, ssid, passphrase);
	} else {
		return -1;
	}

	return 0;
}
