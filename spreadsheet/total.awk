BEGIN {
	row1 = 0
	row2 = 0
	row3 = 0
}

{
	row1 += $1
	row2 += $2
	row3 += $3
}

END {
	print row1, row2, row3
}
