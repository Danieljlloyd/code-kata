"""
Prototype script demonstrating the ability of Postgres to do a location radius
search.
"""

import sqlite3
import random
import time

# Connect to the database.
conn = sqlite3.connect('location_example.db')
c = conn.cursor()

# Define an origin point.
origin_x = (-80.7749, -80.7747)
origin_y = (35.3776, 35.3778)

# Create a Locations table.
c.execute('DROP TABLE IF EXISTS Locations;') 

c.execute('''
CREATE VIRTUAL TABLE Locations USING rtree(
    id,
    min_x, max_x,
    min_y, max_y
);
''')

# Fill the database with some locations.
print 'Filling database...'
start = time.time()
size = 10000
for i in range(1, size+1):
    distance = random.randint(0, 100) / 100.0 * 3.0

    new_x = (origin_x[0] + distance, origin_x[1] + distance)
    new_y = (origin_y[0] + distance, origin_y[1] + distance)

    c.execute('''
    INSERT INTO Locations VALUES (
        ?,
        ?, ?,
        ?, ? 
    );
    ''', (i, new_x[0], new_x[1], new_y[0], new_y[1]))

    conn.commit()

# Print all the locations in the database.
c.execute('SELECT * FROM Locations')
locations = c.fetchall()
end = time.time()

print 'Fill took %ss' % (end - start)

start = time.time()

i = 3

radius = 3.0 / 10.0 * i 

radius_x = (origin_x[0] - radius, origin_x[1] + radius)
radius_y = (origin_y[0] - radius, origin_y[1] + radius)

print 'Searching database...'
c.execute('''
    SELECT * FROM Locations
    WHERE min_x >= ? and max_x <= ?
    AND min_y >= ? and max_y <= ?
    LIMIT 100
''', (radius_x[0], radius_x[1], radius_y[0], radius_y[1])) 

locations = c.fetchall()

end = time.time()

print 'Locations within %s: %d' % (radius, len(locations))
print 'Query took %ss' % (end - start)

start = time.time()
for location in locations:
    print location
end = time.time()

print 'Write took %ss' % (end - start)

conn.close()
