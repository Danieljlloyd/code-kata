def average(a, b):
    return (a + b) / 2

def sqrt(x, delta=0.001):
    """
    Square root of a number by Newtonian approximation.
    """

    def improve(guess):
        return average(guess, x / guess)

    def is_good_enough(guess):
        return abs(guess**2 - x) < delta

    def sqrt_iter(guess):
        return guess if is_good_enough(guess) else sqrt_iter(improve(guess))

    return sqrt_iter(1.0)

print(sqrt(100, delta=0.1))
print(sqrt(100, delta=0.0000000001))
print(sqrt(18392919))
print(sqrt(18392919, delta=0.000001))
