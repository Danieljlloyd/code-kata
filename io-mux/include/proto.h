/*
Author: Daniel Lloyd <DanielLloyd7@gmail.com>
Date: 2017-04-17

Holds functions relating to the protocol to be communicated over the socket
interface.
*/

#ifndef _PROTO_H
#define _PROTO_H

/*
Structure that represents a measurement from a device.

label: String representing the particular measurement.
magnitude: Float representing the magnitude of the measurement.
*/
struct Measurement {
	char label[4];
	float magnitude;
};

/*
Function that caclulates a checksum for the packet by adding the bytes and
returns a 16-bit integer.

Input
-----
buf: Pointer to the start of the packet.
len: Size of the packet.

Return
------
On success returns the result of the checksum as a 16-bit integer. On failure
returns -1.
*/
uint16_t calc_checksum(void *buf, int len);

/*
Function to construct a packet to be communicated over the socket interface.

Input
-----
packet: A pointer to an allocated buffer that is used to store the resultant
	packet.
measurement: A measurement that is to be represented in the packet.

Return
------
On success returns 0. On failure returns -1.
*/
int make_packet(char *packet, struct Measurement measurement);

#endif
