/*
Author: Daniel Lloyd <DanielLloyd7@gmail.com>
Date: 2017-04-18

File to describe a process for a server that accepts packets from the client
and prints a human readable description of the packet.
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>

#include <proto.h> 

/* 
Routine to constantly read packets sent by the client and print the packets in
human readable format.
*/
int main(void)
{
	int rc;

	/* Create a socket. */
	int listener = socket(AF_INET, SOCK_STREAM, 0);

	if (listener < 0) {
		perror("Failed to open socket");
		return -1;
	}
	
	/* Initialize the socket structure. */ 
	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof(struct sockaddr_in));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(5000);

	/* Bind to the host address. */
	rc = bind(listener, &serv_addr, sizeof(struct sockaddr_in));
	if (rc < 0) {
		perror("Failed to bind");
		return -1;
	}

	/* Listen on the socket. */
	listen(listener, 5);

	/* Add the listener to the fdset. */
	int ndfs = 0;
	fd_set masterfds;
	FD_ZERO(&masterfds);
	FD_SET(listener, &masterfds);
	ndfs = listener + 1;

	while (1) {

		/* NOTE: File descriptor set and timeout are modified in
		select and must be created every time. */

		/* Create the file descriptor set. */ 
		fd_set readfds = masterfds;

		/* Specify the timeout to be 10 seconds. */
		struct timeval timeout;
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		/* Wait for data from client. */
		int ready = select(ndfs, &readfds, NULL, NULL, &timeout);
		if (ready < 0) {
			perror("Failed during select");
			exit(-1);
		}
		if (!ready)
			continue;

		/* Check all open connections. */
		for (int fd=0; fd<ndfs; fd++) {	

			/* Read from the clients. */
			if (FD_ISSET(fd, &readfds)) {

				/* Listen for new connections. */
				if (fd == listener) {
					socklen_t addr_size = sizeof(struct sockaddr_in);
					int new_fd = accept(listener, &serv_addr, &addr_size);
					if (new_fd < 0) {
						perror("Failed to accept connection");
						exit(-1);
					}

					/* Add to the master list. */
					FD_SET(new_fd, &masterfds);
					if (new_fd >= ndfs)
						ndfs = new_fd + 1;
				} else {
					/* Read from this client. */
					char packet[1000];
					ssize_t nbytes;
					char *p = packet;
					while((nbytes = read(fd, p, 1)) > 0) {
						if (nbytes < 0) {
							perror("Failed to read from client");
							continue;
						}

						if (*p == '$') {
							break;
						}

						p++;
					}
					*++p = '\0';

					/* If the length of the packet is 0, then the client hung up. */
					if (packet == p) {
						close(fd);
						FD_CLR(fd, &masterfds);
					}

					/* Make sure that this is one contiguous packet. */
					if (*packet != '^') {
						fprintf(stderr, "Throwing out packet %s\n", packet);
						continue;
					}

					/* Unpack into a measurement. */
					struct Measurement measurement;
					memset(&measurement, 0, sizeof(struct Measurement));
					int rc = sscanf(packet, "^%[^:]:%f$",
						&measurement.label, &measurement.magnitude); 
					if (rc == EOF)
						continue;

					/* Print to the user. */
					printf("Label: %s\nMagnitude: %.2f\n",
						measurement.label, measurement.magnitude);
				}
			}
		}
	}
}
