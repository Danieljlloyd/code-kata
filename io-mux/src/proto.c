/*
Author: Daniel Lloyd <DanielLloyd7@gmail.com>
Date: 2017-04-18

Holds the logic for the protocol to be communicated over the socket interface.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <proto.h>

/*
Function to construct a packet to be communicated over the socket interface.
*/
int make_packet(char *packet, struct Measurement measurement)
{
	/* Check that the pointers given are not NULL. */
	if (packet == NULL) {
		fprintf(stderr, "Constructing packet. Packet pointer cannot "
			"be the NULL pointer.\n");
		return -1;
	}

	/* Make the packet. */
	sprintf(packet, "^%s:%.2f$", measurement.label, measurement.magnitude);

	/* Return 0 on success. */
	return 0;
}
