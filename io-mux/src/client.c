/*
Author: Daniel Lloyd <DanielLloyd7@gmail.com>
Date: 2017-04-17

Client process that constantly send a measurement packet to the server process.
*/

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include <proto.h> 

/*
Process that constantly sends a random packet of data to the server.
*/
int main(void)
{
	int rc;
	int base = 1000;

	/* Create a socket to the server. */
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		fprintf(stderr, "Failed to create socket: %d\n", sock);
		return -1;
	}

	/* Define the server address. */
	struct sockaddr_in server_addr;
	memset(&server_addr, '\0', sizeof(struct sockaddr_in));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(5000);

	/* Seed the rand function. */
	srand(time(NULL));

	/* Connect to the server. */
	socklen_t addr_size = sizeof(server_addr);
	rc = connect(sock, &server_addr, addr_size);
	if (rc < 0) {
		fprintf(stderr, "Failed to connect to server.\n");
		return -1;
	}

	/* Write random packets to the server every second, forever. */
	while (1) {
		/* Generate a random integer between 0 and 1000 times
		the base*/
		int random_int = rand();				
		random_int %= 1000 * base;

		/* Divide the randomly generated integer by the base. */
		float random_float = random_int;
		random_float /= base;

		/* Construct a measurement. */
		struct Measurement measure;
		memset(&measure, 0, sizeof(struct Measurement));	
		memcpy(measure.label, "vol", 3);
		measure.magnitude = random_float;

		/* Make a packet for the measurement to be sent to
		the server. */
		char packet[1000];
		memset(packet, 0, 10);
		rc = make_packet(packet, measure);
		if (rc < 0) {
			fprintf(stderr, "Failed to construct packet.\n");
			return -1;
		}

		/* Write the packet to the server. */
		write(sock, packet, strlen(packet));
		puts(packet);

		/* Sleep for a second. */
		sleep(1);
	}

	return 0;
}
