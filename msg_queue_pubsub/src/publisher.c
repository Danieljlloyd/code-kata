#include <mqueue.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define MSG_SIZE 128
#define MAX_MSGS 10

struct subscriber_node {
	mqd_t queue;	
	struct subscriber_node *next;
};

int subscriber_add(struct subscriber_node *, const char *);
struct subscriber_node *subscriber_new(const char *);
int subscriber_list_destroy(struct subscriber_node *);
struct subscriber_node *subscriber_list_create();

int main(void)
{
	int ret;
	int count = 0;
	struct subscriber_node *sub;
	struct subscriber_node *sub_list;
	char msg[MSG_SIZE];

	sub_list = subscriber_list_create();

	while(1) {
		count += 1;		

		for(sub = sub_list; sub != NULL; sub = sub->next) {
			sprintf(msg, "%d", count);
			ret = mq_send(sub->queue, msg, strlen(msg), 1);
			if(ret == -1)
				perror("Failed sending message");
		}

		sleep(1);
	}

	subscriber_list_destroy(sub_list);

	return 0;
}

int subscriber_add(struct subscriber_node *sub, const char *key) 
{
	while(sub->next != 0)
		sub = sub->next;

	sub->next = subscriber_new(key);

	return 0;
}

struct subscriber_node *subscriber_new(const char *key) 
{
	struct subscriber_node *sub = (struct subscriber_node *) malloc(sizeof(struct subscriber_node));

	struct mq_attr attr;
	attr.mq_maxmsg = MAX_MSGS;
	attr.mq_msgsize = MSG_SIZE;
	attr.mq_flags = 0;
	attr.mq_curmsgs = 0;

	int ret = sub->queue = mq_open(key, O_RDWR | O_CREAT, 0666, &attr);
	if(ret == -1)
		perror("Failed opening message queue");

	sub->next = NULL;

	return sub;
}

int subscriber_list_destroy(struct subscriber_node *sub)
{
	struct subscriber_node *prev;

	while(sub->next != 0) {
		prev = sub;
		sub = sub->next;
		free(prev);
	}

	free(sub);

	return 0;
}

struct subscriber_node *subscriber_list_create()
{
	struct subscriber_node *list;

	list = subscriber_new("/queuetest-slave1");
	subscriber_add(list, "/queuetest-slave2");
	//subscriber_add(list, "/queuetest-slave3");

	return list;
}
