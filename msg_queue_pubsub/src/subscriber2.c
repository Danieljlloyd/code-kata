#include <stdio.h>
#include <unistd.h>
#include <mqueue.h>
#include <string.h>

#define MSG_SIZE 128
#define MAX_MSGS 10

int main(void) 
{
	int ret;
	char value[MSG_SIZE];

	struct mq_attr attr;
	attr.mq_maxmsg = MAX_MSGS;
	attr.mq_msgsize = MSG_SIZE;
	attr.mq_flags = 0;
	attr.mq_curmsgs = 0;

	mqd_t queue2 = mq_open("/queuetest-slave2", O_RDWR | O_CREAT, 0666, &attr);
	if(queue2 == -1)
		perror("Failed opening message queue");

	strcpy(value, "");

	while(1) {
		ret = 1;
		while(ret > 0) {
			ret = mq_receive(queue2, value, MSG_SIZE, 0); 
			if(ret == -1)
				perror("Failed receiving message");
			else
				value[ret] = '\0';

			if(strcmp(value, ""))
				printf("Queue 2: %s\n", value);

		}

		sleep(1);
	}
}
