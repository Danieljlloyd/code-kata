function Controller(model, view) {
  this.model = model;
  this.view = view;
  this.view.render();

  this.add_todo = function () {
    new_text = this.view.get_text();
    this.view.clear_textbox();

    this.model.add_todo(new_text);
    this.view.render();
  };

  this.remove_todo = function (id) {
    this.model.remove_todo(id);
    this.view.render();
  };
}

function Model() {
  this.todos = [];
  this.count = 0;

  this.add_todo = function (new_text) {
    this.todos.push({text: new_text, count: this.count});
    this.count++;
  };

  this.remove_todo = function (id) {
    for (var i=0; i<this.todos.length; i++) {
      if (this.todos[i].count == id) {
        this.todos.splice(i, 1);
      }
    }
  };

  this.get_all_todos = function () {
    return this.todos;
  }
}

function View(model) {
  this.model = model;
  this.etextbox = document.getElementById('my-textbox');
  this.etodo = document.getElementById('todo-list');

  this.todo_template = document.getElementById('todo-template');
  this.callback_str = 'app.controller.remove_todo($id)';

  this.render = function () {
    var view_html = '';

    todos = this.model.get_all_todos();

    for (var i=0; i < todos.length; i++) {
      var my_todo = todos[i];

      var temp = this.todo_template.innerHTML;
      temp = temp.replace('$callback', this.callback_str);
      temp = temp.replace('$text', my_todo.text);
      temp = temp.replace(/\$id/g, my_todo.count);

      console.log(temp);
      view_html = view_html + temp;
    }

    this.etodo.innerHTML = view_html;
  };

  this.get_text = function () {
    return this.etextbox.value;
  };

  this.clear_textbox = function () {
    this.etextbox.value = '';
  };

  this.etextbox.onkeypress = function (e) {
    console.log(this.id);

    if (e.keyCode === 13) {
      app.controller.add_todo();
    }
  };
}

window.onload = function () {
  model = new Model();
  view = new View(model);
  controller = new Controller(model, view);

  app = {};
  app.controller = controller;
}
