#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define AWK_PROGRAM "				\
{ 						\
	print $2; lines += 1			\
}						\
						\
END {						\
	print \"Number of lines: \", lines;	\
}						\
"

int main(void)
{
	char *argv1[] = {"ls", "-l", "-h", NULL};
	char *argv2[] = {"awk", AWK_PROGRAM, NULL};

	int pipefd[2];
	pipe(pipefd);

	pid_t pid1 = fork();
	if (pid1 == 0) {
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[0]);
		execvp(argv1[0], argv1);
		perror("exec");
		return 1;
	}

	pid_t pid2 = fork();
	if (pid2 == 0) {
		dup2(pipefd[0], STDIN_FILENO);
		close(pipefd[1]);
		execvp(argv2[0], argv2);
		perror("exec");
		return 1;
	}

	close(pipefd[0]);
	close(pipefd[1]);

	int wstatus1, wstatus2;
	waitpid(pid1, &wstatus1, 0);
	waitpid(pid2, &wstatus2, 0);
	return 0;
}
