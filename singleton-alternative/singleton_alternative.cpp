#include <map>
#include <string>
#include <vector>
#include <iostream>

class IDatabaseGateway
{
	public:
	virtual double getMetric(std::string key)=0;
	virtual int setMetric(std::string key, double value)=0;
};

class MemoryDatabaseGateway : public IDatabaseGateway
{
	public:
	double getMetric(std::string key);
	int setMetric(std::string key, double value);

	private:
	std::map<std::string, double> metrics;
};

double MemoryDatabaseGateway::getMetric(std::string key)
{
	return this->metrics[key];
}

int MemoryDatabaseGateway::setMetric(std::string key, double value)
{
	this->metrics[key] = value;
}

////////////////////////////////////////////////////////////////////////////////

class Measurement
{
	public:
	Measurement(std::string key, double value);
	std::string key;
	double value;
};

Measurement::Measurement(std::string key, double value)
{
	this->key = key;
	this->value = value;
}

class IDevice
{
	public:
	virtual std::vector<Measurement> getMeasurements(void)=0;
};

class PvInverter : IDevice
{
	public:
	std::vector<Measurement> getMeasurements(void);
};

std::vector<Measurement> PvInverter::getMeasurements()
{
	std::vector<Measurement> measurements;
	measurements.push_back(Measurement("Solar Power", 10.3));
	return measurements;
}

class PowerMeter : IDevice
{
	public:
	std::vector<Measurement> getMeasurements(void);
};

std::vector<Measurement> PowerMeter::getMeasurements()
{
	std::vector<Measurement> measurements;
	measurements.push_back(Measurement("Grid Power", 7.6));
	return measurements;
}

////////////////////////////////////////////////////////////////////////////////

class Aggregator
{
	public:
	Aggregator(IDatabaseGateway *db);
	int aggregate(void);

	private:
	IDatabaseGateway *db;
};

Aggregator::Aggregator(IDatabaseGateway *db)
{
	this->db = db;
}

int Aggregator::aggregate(void)
{
	double gridPower = this->db->getMetric("Grid Power");
	double pvPower = this->db->getMetric("Solar Power");

	double loadPower = gridPower + pvPower;
	this->db->setMetric("Load Power", loadPower);
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	IDatabaseGateway *db = new MemoryDatabaseGateway();

	std::vector<Measurement> measurements;

	PvInverter pvInverter;
	PowerMeter powerMeter;

	Aggregator aggregator = Aggregator(db);

	/* Get the measurements from the PV Inverter. */
	std::vector<Measurement> pvInverterMeasurements;
	pvInverterMeasurements = pvInverter.getMeasurements();
	measurements.insert(measurements.end(), pvInverterMeasurements.begin(),
		pvInverterMeasurements.end());

	/* Get the measurements from the power meter. */
	std::vector<Measurement> powerMeterMeasurements;
	powerMeterMeasurements = powerMeter.getMeasurements();
	measurements.insert(measurements.end(), powerMeterMeasurements.begin(),
		powerMeterMeasurements.end());

	for (auto &m : measurements) {
		db->setMetric(m.key, m.value);
	}

	aggregator.aggregate();

	std::cout << "Solar Power: " << db->getMetric("Solar Power")
		<< std::endl;
	std::cout << "Grid Power: " << db->getMetric("Grid Power")
		<< std::endl;
	std::cout << "Load Power: " << db->getMetric("Load Power")
		<< std::endl;
}
