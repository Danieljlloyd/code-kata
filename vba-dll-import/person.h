#ifndef _PERSON_H
#define _PERSON_H

#include <stdio.h>

#define STR_FIELD_SIZE 50

int __stdcall create_person(char *name, char *email, char *phone_number,
		int age);
int __stdcall person_get_name(int handle, char *name);
int __stdcall person_get_email(int handle, char *email);
int __stdcall person_get_phone_number(int handle, char *phone_number);
int __stdcall person_get_age(int handle);

#endif
