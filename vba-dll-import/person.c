#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "person.h"

#define MAX_PEOPLE 100

#define LOG_FILE "C:\\Users\\Danie\\Documents\\Src\\code-kata\\vba-dll-import\\test.log"

void log_error(char *msg)
{
	FILE *fp = fopen(LOG_FILE, "a+");
	fprintf(fp, msg);
	fclose(fp);
}

typedef struct person_c {
	char name[STR_FIELD_SIZE];
	char email[STR_FIELD_SIZE];
	char phone_number[STR_FIELD_SIZE];
	int age;
} person_c;

typedef struct person_factory_c {
	person_c people[MAX_PEOPLE];
	int num_people;
} person_factory_c;

person_factory_c *get_person_factory_instance(void) {
	static person_factory_c person_factory;
	return &person_factory;
}

int person_factory_create_person(person_factory_c *factory,
		char *name, char *email, char *phone_number, int age)
{
	int handle = -1;
	person_c new_person;

	if (factory->num_people >= MAX_PEOPLE) {
		log_error("Maximum number of people exceeded.\n");
	} else {

		handle = factory->num_people;

		strncpy(new_person.name, name, STR_FIELD_SIZE);
		strncpy(new_person.email, email, STR_FIELD_SIZE);
		strncpy(new_person.phone_number, phone_number, STR_FIELD_SIZE);
		new_person.age = age;

		factory->people[handle] = new_person;

		factory->num_people++;
	}

	return handle;
}

int person_factory_get_person(person_factory_c *factory, int handle,
		person_c *person)
{
	int result = -1;
	char test_msg[1024];
	memset(person, 0, sizeof(person_c));

       	sprintf(test_msg, "handle is %d.\n", handle);
	log_error(test_msg);

	sprintf(test_msg, "num people is %d.\n", factory->num_people);
	log_error(test_msg);

	if (handle >= factory->num_people) {
		log_error("This person has not yet been created.\n");
	} else {
		*person = factory->people[handle];
		result = 0;
	}

	return result;
}

__declspec(dllexport) int __stdcall create_person(char *name, char *email, char *phone_number,
		int age)
{
	int handle = -1;

	person_factory_c *factory = get_person_factory_instance();

	if((handle = person_factory_create_person(factory, name, email,
					phone_number, age)) < 0) {
		log_error("Failed to create person.\n");
	}

	return handle;
}

int get_person(int handle, person_c *person)
{
	int result = -1;

	person_factory_c *factory = get_person_factory_instance();

	if (person_factory_get_person(factory, handle, person) < 0) {
		log_error("Failed to retrieve person.\n");
	} else {
		result = 0;
	}

	return result;
}

__declspec(dllexport) int __stdcall person_get_name(int handle, char *name)
{
	int result = -1;
	person_c person = {0};

	char test_msg[1024];

	if (get_person(handle, &person)) {
		log_error("Failed to retrieve person.\n");
	} else {
		log_error("Copying name into given string.\n");
		strncpy(name, person.name, STR_FIELD_SIZE);

		sprintf(test_msg, "Name string: %s\n", person.name);
		log_error(test_msg);

		sprintf(test_msg, "Returned string: %s\n", name);
		log_error(test_msg);

		result = 0;
	}

	return result;
}

__declspec(dllexport) int __stdcall person_get_email(int handle, char *email)
{
	int result = -1;
	person_c person = {0};

	if (get_person(handle, &person)) {
		log_error("Failed to retrieve person.\n");
	} else {
		strncpy(email, person.email, STR_FIELD_SIZE);
		result = 0;
	}

	return result;
}

__declspec(dllexport) int __stdcall person_get_phone_number(int handle, char *phone_number)
{
	int result = -1;
	person_c person = {0};

	if (get_person(handle, &person)) {
		log_error("Failed to retrieve person.\n");
	} else {
		strncpy(phone_number, person.phone_number, STR_FIELD_SIZE);
		result = 0;
	}

	return result;
}

__declspec(dllexport) int __stdcall person_get_age(int handle)
{
	person_c person = {0};
	int age = -1;

	if (get_person(handle, &person)) {
		log_error("Failed to retrieve person.\n");
	} else {
		age = person.age;
	}

	return age;
}
