#include <stdio.h>
#include "person.h"

int main(void)
{
	int handle;
	int age = -1;
	int result = -1;
	char name[STR_FIELD_SIZE] = {0};
	char email[STR_FIELD_SIZE] = {0};
	char phone_number[STR_FIELD_SIZE] = {0};

	handle = create_person("Daniel Lloyd", "DanielLloyd7@gmail.com",
			"0466 536 465", 25);

	if (person_get_name(handle, name)) {
		fprintf(stderr, "Failed to get my name.\n");
	}
	else if (person_get_email(handle, email)) {
		fprintf(stderr, "Failed to get email.\n");
	}
	else if (person_get_phone_number(handle, phone_number)) {
		fprintf(stderr, "Failed to get phone number.\n");
	}
	else if ((age = person_get_age(handle)) < 0) {
		fprintf(stderr, "Failed to get age.\n");
	} else {
		printf("My handle is %d.\nMy name is %s.\nMy email is %s.\n"
				"My phone number is %s.\nMy age is %d.", handle,
				name, email, phone_number, age);
		result = 0;
	}

	return result;
}
