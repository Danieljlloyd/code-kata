Attribute VB_Name = "Macros"
Private Declare Function dllCreatePerson Lib "person.dll" Alias "create_person@16" (ByVal name As String, ByVal email As String, ByVal phone_number As String, ByVal age As Long) As Long
Private Declare Function dllPersonGetName Lib "person.dll" Alias "person_get_name@8" (ByVal handle As Long, ByVal name As String) As Long
Private Declare Function dllPersonGetPhoneNumber Lib "person.dll" Alias "person_get_phone_number@8" (ByVal handle As Long, ByVal phone_number As String) As Long
Private Declare Function dllPersonGetAge Lib "person.dll" Alias "person_get_age@4" (ByVal handle As Long) As Long
Private Declare Function dllPersonGetEmail Lib "person.dll" Alias "person_get_email@8" (ByVal handle As Long, ByVal email As String) As Long

Sub TestPerson()
    Dim personHandle As Long
    Dim rc As Long
    Dim name As String * 50
    Dim email As String * 50
    Dim phoneNumber As String * 50
    Dim age As Long
    
    personHandle = dllCreatePerson("Daniel Lloyd", "DanielLloyd7@gmail.com", "0466 536 465", 25)
    rc = dllPersonGetName(personHandle, name)
    rc = dllPersonGetEmail(personHandle, email)
    rc = dllPersonGetPhoneNumber(personHandle, phoneNumber)
    age = dllPersonGetAge(personHandle)
    
    MsgBox personHandle
    MsgBox name
    MsgBox email
    MsgBox phoneNumber
    MsgBox age
End Sub
