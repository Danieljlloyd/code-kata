#!/bin/bash

# ARGUMENTS
# 1: IP of remote server
# 2: User of remote server

echo 'INFO: Install SSHFS'
sudo apt-get install sshfs

if [[ ! -d /shared_data ]]; then
	echo 'INFO: Create directory for shared files'
	sudo mkdir /shared_data
	sudo chown $USER:$USER /shared_data
fi

if [[ ! -d /shared_data/$1 ]]; then
	echo "INFO: Create directory for shared files from $1"
	mkdir /shared_data/$1
fi

nohup sshfs $2@$1:shared_data /shared_data/$1
