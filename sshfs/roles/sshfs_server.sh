#!/bin/bash

# Make a shared directory
if [[ ! -d /home/vagrant/shared_data ]]; then
	echo 'INFO: Making shared file system directory'
	mkdir $HOME/shared_data
fi

# Write a test file to the directory
echo "To infinity and beyond" > $HOME/shared_data/test_file
