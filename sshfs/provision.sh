#!/bin/bash

source inventory.sh
source var.sh

echo "Installing file servers"
for s in $file_servers; do
	scp files/authorized_keys $s:.ssh/authorized_keys
	ssh -t $s < roles/base.sh
	ssh -t $s < roles/sshfs_server.sh
done

echo "Installing PCs"
for s in $pcs; do
	scp files/authorized_keys $s:.ssh/authorized_keys
	ssh -t $s < roles/base.sh
	ssh -t $s "bash -s $SSHFS_SERVER_IP $SSHFS_SERVER_USER" < roles/sshfs_client.sh
done
