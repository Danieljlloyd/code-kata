#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#define BUF_SIZE 200

int main(int argc, char *argv[])
{
	int pfd[2];
	char buf[BUF_SIZE];
	ssize_t num_read;

	if (argc != 2 || strcmp(argv[1], "--help") == 0)
		exit(-1);

	if (pipe(pfd) == -1)
		exit(-2);

	switch(fork()) {
		/* Fork error. */
		case -1:
			fputs("Error forking.\n", stderr);

		/* Child process. */
		case 0:
			close(pfd[1]);

			num_read = read(pfd[0], buf, BUF_SIZE);

			write(STDOUT_FILENO, buf, num_read);
			write(STDOUT_FILENO, "\n", 1);

			close(pfd[0]);

			exit(0);

		/* Parent writing process. */
		default:
			close(pfd[0]);

			write(pfd[1], argv[1], strlen(argv[1]));

			close(pfd[1]);

			wait(NULL);

			exit(0);
	}
}
