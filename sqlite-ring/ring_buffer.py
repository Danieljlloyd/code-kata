import sqlite3

class RingBuffer(object):
    """
    A ring buffer implementation using SQLite.
    """

    def __init__(self, db_name, ring_name, ring_size):
        self.conn = sqlite3.connect(db_name)
        self.c = self.conn.cursor()
        self.ring_name = ring_name
        self.ring_size = ring_size

        self._table_setup()

    def __del__(self):
        self.conn.close()

    def _table_setup(self):
        """
        Create the ring buffer table if it does not exist.
        """

        self.c.execute("""
            CREATE TABLE IF NOT EXISTS {0} (
                id INTEGER PRIMARY KEY,
                content TEXT,
                processed INTEGER
            );
        """.format(self.ring_name, self.ring_size))

        query = """
            CREATE TRIGGER {0}_delete_tail AFTER INSERT ON {0}
            BEGIN
                DELETE FROM {0} WHERE id%{1}=NEW.id%{1}
                    AND id != NEW.id;
            END;
        """.format(self.ring_name, self.ring_size)

        self.c.execute(query)

    def pop(self, limit=100):
        """
        Pops a number of items off the ring buffer.
        """

        params = {
            'limit': limit
        }

        self.c.execute("""
            SELECT * FROM {0} WHERE processed = 0 ORDER BY id
                LIMIT :limit;
        """.format(self.ring_name), params)

        items = self.c.fetchall()

        for item in items:
            params['id'] = item[0]
            self.c.execute("""
                UPDATE {0} SET processed = 1 WHERE id = :id;
            """.format(self.ring_name), params)

        return [ x[1] for x in items ]

    def push(self, items):
        """
        Pushes a list of items onto the ring buffer.
        """

        for item in items:
            query = ("INSERT INTO {0} (content, processed) " + \
                    "VALUES('{1}', 0);").format(self.ring_name, item);

            self.c.execute(query)

        self.conn.commit()

        return True

def main():
    import time
    import os

    try:
        os.unlink('test.sqlite')
    except:
        pass

    ring_buffer = RingBuffer('test.sqlite', 'ring_buffer', 1000)

    start = time.clock()
    ring_buffer.push([ str(i) for i in range(2000) ])
    items = ring_buffer.pop(limit=1000)
    end = time.clock()

    print '2000 push / 1000 pop took {0}s'.format(end - start)

    print 'Select 1:'
    for item in items:
        print item


    print 'Select 2:'.format(i)
    items = ring_buffer.pop(limit=1000)
    print 'Length of items: {0}'.format(len(items))
    for item in items:
        print item

if __name__ == '__main__':
    main()
