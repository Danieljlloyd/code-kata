#include <stdio.h>

extern char **environ;

int print_env()
{
	puts("Set-Cookie: session_id=25");

	puts("Content-type: text/html");
	puts("");
	puts("");
	puts("<html>");

	puts("<head>");
	puts("<title>CGI Example</title>");
	puts("</head>");

	puts("<body>");

	puts("Here are all my environment parameters:");
	int i = 0;
	while (environ[i]) {
		printf("<li>%s</li>\n", environ[i]);
		i++;
	}

	puts("</body>");
	puts("</html>");
}


int main(void)
{
	print_env();
}
