import socket
import select
import json 
import os

shared_state = {}

def set_metric(query_data):
    shared_state[query_data['key']] = query_data['data']

def get_metric(query_data):
    return shared_state[query_data['key']]

if __name__ == '__main__':
    # Define a server address.
    server_addr = '/tmp/shared_state'

    # Create a socket.
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    # Bind to the server address.
    if os.path.exists(server_addr):
        os.unlink(server_addr)

    sock.bind(server_addr)

    while True:
        ready = select.select([sock], [], [], 1.0)

        if ready[0] == []:
            continue

        addr = None
        try:
            query, addr = sock.recvfrom(4096)
        except sock.error as msg:
            print >>sys.stderr, 'Reading query: %s' % str(msg)
            sock.close()

        query_data = json.loads(query)

        if query_data['command'] == 'set':
            set_metric(query_data)
        elif query_data['command'] == 'get':
            data = get_metric(query_data)

            response = json.dumps({
                'key': query_data['key'],
                'data': data
            })

            sock.sendto(response, addr)
