import socket
import select
import sys
import time
import json
import os
from datetime import datetime, timedelta

server_addr = '/tmp/shared_state'
client_addr = '/tmp/shared_state.client.%d' % os.getpid()

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
rlist = [sock]

sock.bind(client_addr)

start = time.time()
i = 0
while i < 1000:
    command = json.dumps({
        'command': 'set',
        'key': 'voltage', 
        'data': i
    })

    query = json.dumps({
        'command': 'get',
        'key': 'voltage'
    })

    sock.sendto(command, server_addr)

    sock.sendto(query, server_addr)

    ready,_,_ = select.select(rlist, [], [], 1.0)

    if ready[0] == sock:
        response = sock.recv(100)
    else:
        print sys.stderr, 'No response.'

    i += 1

end = time.time()

time_taken = end - start
request_time = time_taken / 1000.0
print 'Took %f seconds for 1000 sets/gets' % time_taken
print '%f seconds per request' % request_time

i = 0
tnow = int(time.time())
tstart = tnow + 1
t1sec = tnow + 2
while(tnow < tstart):
    tnow = int(time.time())
while tnow < t1sec:
    tnow = int(time.time())

    command = json.dumps({
        'command': 'set',
        'key': 'voltage', 
        'data': i
    })

    query = json.dumps({
        'command': 'get',
        'key': 'voltage'
    })

    sock.sendto(command, server_addr)

    sock.sendto(query, server_addr)

    ready,_,_ = select.select(rlist, [], [], 1.0)

    if ready[0] == sock:
        response = sock.recv(100)
    else:
        print sys.stderr, 'No response.'

    i += 1

print 'Approx. %d sets/gets per second.' % i
