import socket
import select
import os
import sys

# Define a server address.
server_addr = '/tmp/server_sock'

# Create a listener socket.
listener = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Create an asynchronous interface.
rlist = [listener]

# Bind to the server address.
if os.path.exists(server_addr):
    os.unlink(server_addr)

listener.bind(server_addr)

# Start listening.
listener.listen(1)

# Constantly echo.
while True:
    ready = select.select(rlist, [], [], 1.0)

    for sock in ready[0]:
        # Add a new connection if one is found.
        if sock is listener:
            (conn, address) = listener.accept()
            rlist.append(conn)
            print 'Accepting new connection: %s' % address

        # If there is a message to be read from the client, read and echo back.
        else:
            try:
                query = sock.recv(100)
                print 'Received query: %s' % query
            except sock.error as msg:
                print >>sys.stderr, 'Reading query: %s' % str(msg)
                sock.close()
                rlist.remove(sock)

            try:
                sock.send(query)
            except socket.error as msg:
                print >>sys.stderr, 'Reply: %s' % repr(msg)
                sock.close()
                rlist.remove(sock)
