import socket
import sys
import time

server_addr = '/tmp/server_sock'

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

try:
    sock.connect(server_addr)
except socket.error as msg:
    sys.stderr.write('Failed to connect to socket %s' % str(msg))

while True:
    sock.send('Hello world!')

    response = sock.recv(100)

    print 'Got response: %s' % response 

    time.sleep(0.3) 
