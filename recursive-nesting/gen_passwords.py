"""
Python script to generate all possible passwords of a specific length.
"""

import argparse

valid_characters = map(lambda x: chr(x), range(ord('1'), ord('z')+1))

def gen_characters(n, prefix):
    if n == 0:
        print prefix
    else:
        for character in valid_characters:
            gen_characters(n-1, prefix + character)

parser = argparse.ArgumentParser()
parser.add_argument('maxlength', help='Maximum password length', type=int)

args = parser.parse_args()

for i in range(1, args.maxlength + 1):
    gen_characters(i, '')
