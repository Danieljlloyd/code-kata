#include <stdio.h>
#include <fir_filter.h>

uint8_t fir_filter_update(struct FIRFilter *self, int32_t signal);

uint8_t fir_filter_init(struct FIRFilter *self, int32_t *coeffs,
	int32_t *history, uint16_t n, uint8_t shift, uint32_t sampling_period)
{
	int i;

	self->coeffs = coeffs;
	self->history = history;
	self->n = n;
	self->shift = shift;
	self->sampling_period = sampling_period;

	self->last_updated = 0;
	self->position = 0;

	/* Erase the history buffer. */
	for (i=0; i<self->n; i++)
		self->history[i] = 0;
}

uint8_t fir_filter_update(struct FIRFilter *self, int32_t signal)
{
	uint16_t i;
	uint32_t value;
	int history_index;

	/* Accumulate historical data. */
	value = 0;
	for (i=0; i<self->n; i++) {
		history_index = (i + self->position) % self->n;

		value += self->history[history_index] * self->coeffs[i] >> \
			self->shift;
	}

	/* Add the new sensor measurement. */
	value += (signal * self->coeffs[self->position]) >> self->shift;
//	printf("value: %d\ncoeff: %d\nsignal: %d\n\n", value,
//		self->coeffs[self->position], signal);
	self->history[self->position] = value;

	self->position++;
	if (self->position = self->n-1)
		self->position = 0;

	return 0;
}

uint32_t fir_filter_calculate(struct FIRFilter *self, int32_t signal,
	uint32_t now)
{
	if (now > self->last_updated + self->sampling_period) {
		fir_filter_update(self, signal);
		self->last_updated = now;
	}

	return self->history[self->position];
}
