#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fir_filter.h>

/* Filter parameters. */
#define N_COEFFS 10 
#define FIR_SHIFT 10 

/* Testing parameters. */
#define N_SAMPLES 10000

int main(void)
{
	int seed = 8372;

	int i;
	int32_t fir_filter_coeffs[N_COEFFS] = { 102, 102, 102, 102, 102, 102,
		102, 102, 102, 102};
	int32_t fir_filter_history[N_COEFFS];
	struct FIRFilter sma_filter;
	int time[N_SAMPLES];
	double signal[N_SAMPLES];
	int rounded_signal[N_SAMPLES];
	int filtered_signal[N_SAMPLES];

	srand(seed);

	/* Create a simple moving average filter using the FIR filter ADT. */
	fir_filter_init(&sma_filter, fir_filter_coeffs, fir_filter_history,
		N_COEFFS, FIR_SHIFT, 100);

	/* Create a test dataset of noise to filter. */
	for (i=0; i<N_SAMPLES; i++) {
		time[i] = i*5;
		signal[i] = sin(i / 1000.0) + rand() / 10000000000.0;
	}

	/* Scale the signal. This is what would actually come through on
		the ADC.*/
	for (i=0; i<N_SAMPLES; i++) {
		rounded_signal[i] = signal[i] * 1024 + 2000;
	}

	/* Filter the signal. */
	for (i=0; i<N_SAMPLES; i++) {
		filtered_signal[i] = fir_filter_calculate(&sma_filter,
			rounded_signal[i], time[i]);
	}

	/* View the output. */
	for (i=0; i<N_SAMPLES; i++) {
		printf("%d, %d, %d\n", time[i], rounded_signal[i],
			filtered_signal[i]);
	}
}
