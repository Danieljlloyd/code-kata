set terminal png
set datafile separator ","
set output "sma_test_filtered.png"

set title "Simple Moving Average Filter Test"
set xlabel "time (us)"
set ylabel "Signal"

plot 'sma_test.csv' using 1:3 with lines
