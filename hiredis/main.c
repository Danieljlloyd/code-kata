#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <hiredis.h>

int main(void) {
	redisContext *c;
	redisReply *reply;

	/* Connect to redis. */
	c = redisConnect("127.0.0.1", 6379);
	if (c == NULL) {
		fprintf(stderr, "Can't allocate redis context.\n");
		exit(-1);
	}
	else if (c->err) {
		fprintf(stderr, "Redis error: %s\n", c->errstr);
		exit(-1);
	}

	char my_str[50] = "Hello world!";

	/* Write to a key. */
	reply = redisCommand(c, "SET foo %s", my_str);
	printf("SET: %s\n", reply->str);
	freeReplyObject(reply);

	/* Read back the key. */
	reply = redisCommand(c, "GET foo");
	printf("GET: %s\n", reply->str);
	freeReplyObject(reply);
}
