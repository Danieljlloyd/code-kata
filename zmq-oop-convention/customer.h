#ifndef _CUSTOMER_H
#define _CUSTOMER_H

typedef struct customer_t {
	int age;
	char name[50];
} customer_t;

/* Constructor */
int customer_init(customer_t *self, int age, char *name);

customer_t *customer_new(int age, char *name);

/* Destructor */
int customer_destroy(customer_t *);

/* Setter */
int customer_set_age(customer_t *, int age);

/* Getter */
int customer_age(customer_t *);

#endif
