#include <stdio.h>
#include <customer.h>

int main(void)
{
	/* Manual memory creation. */
	customer_t *my_customer = customer_new(24, "Daniel Lloyd");

	customer_set_age(my_customer, 25);

	printf("%d\n", customer_age(my_customer));

	customer_destroy(my_customer);

	/* Automatic memory creation. */
	customer_t new_customer;
	customer_init(&new_customer, 25, "Shekar Seldon");

	customer_set_age(&new_customer, 26);

	printf("%d\n", customer_age(&new_customer));
}
