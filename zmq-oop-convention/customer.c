#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "customer.h"

static const int MAX_AGE = 150;

int _customer_set_name(customer_t *self, char *name)
{
	assert(self);
	assert(name);
	assert(strlen(name) < 50);

	strncpy(self->name, name, 50);
}

int customer_set_age(customer_t *self, int age)
{
	assert(self);
	assert(age > 0 && age < MAX_AGE);

	self->age = age;
}

int customer_age(customer_t *self)
{
	assert(self);

	return self->age;
}

int customer_init(customer_t *self, int age, char *name)
{
	assert(self);
	assert(age > 0 && age < MAX_AGE);
	assert(strlen(name) < 50);

	customer_set_age(self, age);
	_customer_set_name(self, name);
}

customer_t *customer_new(int age, char *name)
{
	assert(age > 0 && age < MAX_AGE);
	assert(strlen(name) < 50);

	customer_t *self = (customer_t *) malloc (sizeof(customer_t));
	assert(self);

	customer_init(self, age, name);

	return self;
}

int customer_destroy(customer_t *self)
{
	assert(self);
	free(self);
}
