import threading
from Queue import Queue
import time
import pickle

class Command(object):
    def __init__(self, label, value):
        self.label = label
        self.value = value

    def __str__(self):
        return 'label={0}, value={1}'.format(self.label, self.value)

class WorkerThread(threading.Thread):
    def __init__(self, worker, start_event, finished_event, measurement_queue,
            commands_queue, stdout_lock):
        super(WorkerThread, self).__init__()
        self.worker = worker
        self.start_event = start_event
        self.finished_event = finished_event
        self.measurement_queue = measurement_queue
        self.commands_queue = commands_queue
        self.stdout_lock = stdout_lock

    def run(self):
        while True:
            self.start_event.wait(timeout=10)
            self.start_event.clear()

            while not self.commands_queue.empty():
                command = self.commands_queue.get()
                with self.stdout_lock:
                    print('Handling command: {0}'.format(command))

            time.sleep(0.8)
            self.measurement_queue.put(self.worker.get_msg())

            self.finished_event.set()

class WorkerMsg(object):
    def __init__(self, topic, worker_id):
        self.topic = topic
        self.worker_id = worker_id

    def __str__(self):
        return 'measurement={0}, value={1}'.format(self.topic,
            self.worker_id)

class Worker(object):
    def __init__(self, worker_id):
        self.worker_id = worker_id

    def get_msg(self):
        return WorkerMsg(
            topic='serial',
            worker_id=self.worker_id,
        )

class WorkerEntry(object):
    def __init__(self, thread, start_event, finished_event, command_queue):
        self.thread = thread
        self.start_event = start_event
        self.finished_event = finished_event
        self.command_queue = command_queue

class Scheduler(threading.Thread):
    def __init__(self, iteration_start_event):
        super(Scheduler, self).__init__()
        self.iteration_start_event = iteration_start_event

    def run(self):
        while True:
            self.iteration_start_event.set()
            time.sleep(1)

def main():
    stdout_lock = threading.Lock()
    measurement_queue = Queue()

    iteration_start_event = threading.Event()
    scheduler = Scheduler(iteration_start_event)
    scheduler.setDaemon(True)

    threads = []
    for i in range(5):
        new_worker = Worker(i)

        # Inject polymorphic classes into the thread.
        command_queue = Queue()
        start_event = threading.Event()
        finished_event = threading.Event()
        new_worker_thread = WorkerThread(new_worker, start_event,
            finished_event, measurement_queue, command_queue, stdout_lock)
        new_worker_thread.setDaemon(True)
        new_worker_thread.start()

        threads.append(WorkerEntry(new_worker_thread, start_event,
            finished_event, command_queue))

    scheduler.start()
    while True:
        iteration_start_event.wait()
        iteration_start_event.clear()

        for entry in threads:
            entry.start_event.set()

        for entry in threads:
            entry.finished_event.wait(timeout=10)
            entry.finished_event.clear()

        while not measurement_queue.empty():
            msg = measurement_queue.get()
            with stdout_lock:
                print('Received measurement: {0}'.format(msg))

        with stdout_lock:
            print('Do control processing.')

        for entry in threads:
            entry.command_queue.put(Command(
                label='someDevice.currentSetpoint',
                value=102.4,
            ))

if __name__ == '__main__':
    main()
