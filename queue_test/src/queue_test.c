#include <sys/queue.h>	
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

SLIST_HEAD(customer_slist_head, customer_entry) head = SLIST_HEAD_INITIALIZER(head);

struct customer_entry {
	SLIST_ENTRY(customer_entry) entries;
	char name[50];
	char phone[50];
	char email[50];
};

int main(void)
{
	struct customer_entry *n1, *n2, *np;

	n1 = malloc(sizeof(struct customer_entry));
	strcpy(n1->name, "Daniel Lloyd");
	strcpy(n1->phone, "0466 536 465");
	strcpy(n1->email, "Daniellloyd7@gmail.com");

	n2 = malloc(sizeof(struct customer_entry));
	strcpy(n2->name, "Steven Lloyd");
	strcpy(n2->phone, "0466 536 465");
	strcpy(n2->email, "Stevenlloyd@showpage.com.au");

	SLIST_INSERT_HEAD(&head, n1, entries);
	SLIST_INSERT_HEAD(&head, n2, entries);

	SLIST_FOREACH(np, &head, entries)
		printf("Name is %s, email is %s, phone is %s\n", np->name, np->email, np->phone);

	return 0;
}
