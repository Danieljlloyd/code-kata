#include <iostream>
#include <string>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlwriter.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

class HelloView
{
	public:
	HelloView();
	std::string createView();
	std::string getStylesheet();

	private:
	std::string title;
	std::string text;
	std::string stylesheet;
};

HelloView::HelloView()
{
	/* You would normally get this kind of information from a presenter
	class that retrieves relevant information from the database. */
	this->title = "Hello World Example";
	this->text = "This is my cool CGI website example in C++.";
	this->stylesheet = "/var/www/html/hello_cgi.xsl";
}

std::string HelloView::createView()
{
	xmlDocPtr doc, res = NULL;
	xsltStylesheetPtr cur = NULL;
	xmlNodePtr root_node = NULL;
	xmlChar *xmlbuff;
	int buffersize;
	int rc;

	/* Create the xml document. */
	doc = xmlNewDoc(BAD_CAST "1.0");
	root_node = xmlNewNode(NULL, BAD_CAST "hello_view");
	xmlDocSetRootElement(doc, root_node);

	/* Would normally call a presenter class to get some information
	from the database. */

	/* Add attributes to the XML document. */
	xmlNewChild(root_node, NULL, BAD_CAST "title",
		(const xmlChar *) this->title.c_str());
	xmlNewChild(root_node, NULL, BAD_CAST "text",
		(const xmlChar *) this->text.c_str());

	/* Style the document with XSL. */
	cur = xsltParseStylesheetFile((const xmlChar *) this->stylesheet.c_str());
	res = xsltApplyStylesheet(cur, doc, NULL);

	/* Convert the XML tree to a string. */
	xmlDocDumpFormatMemory(res, &xmlbuff, &buffersize, 1);
	std::string out((char *) xmlbuff);

	xsltFreeStylesheet(cur);
	xsltCleanupGlobals();

	xmlFreeDoc(doc);
	xmlCleanupParser();
	xmlMemoryDump();

	return out;
}

int main(int argc, char *argv[])
{
	HelloView view;

	/* Print a HTTP response with the html rendered by the view. */
	std::cout << "Content-Type: text/html" << std::endl << std::endl;
	std::cout << view.createView() << std::endl;

	return 0;
}
