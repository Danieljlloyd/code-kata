BEGIN {
	OFS=","

	for (i=0; i<1000; i++) {
		x = i*0.01
		y = sin(x) + exp(x/10)

		print x, y
	}	
}
