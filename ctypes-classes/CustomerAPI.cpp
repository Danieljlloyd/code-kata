#include "CustomerAPI.h"
#include "Customer.h"

#define MAKE_CUSTOMER(ptr) static_cast<Customer *>(ptr)

void *customer_new(char *name, char *email)
{
	return (void *) new Customer(name, email);
}

void customer_delete(void *self)
{
	delete MAKE_CUSTOMER(self);
}

char *customer_get_name(void *self)
{
	return MAKE_CUSTOMER(self)->getName();
}

char *customer_get_email(void *self)
{
	return MAKE_CUSTOMER(self)->getEmail();
}
