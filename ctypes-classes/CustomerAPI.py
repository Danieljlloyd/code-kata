from ctypes import *
import os

def wrap_function(lib, funcname, restype, argtypes):
    func = lib.__getattr__(funcname)
    func.restype = restype
    func.argtypes = argtypes
    return func

class Customer(object):
    API = cdll.LoadLibrary(os.path.join(os.getcwd(), 'libctypes_example.so'))
    API_CUSTOMER_NEW = wrap_function(API, 'customer_new', c_void_p,
            [c_char_p, c_char_p])
    API_CUSTOMER_GET_NAME = wrap_function(API, 'customer_get_name',
            c_char_p, [c_void_p])
    API_CUSTOMER_GET_EMAIL = wrap_function(API, 'customer_get_email',
            c_char_p, [c_void_p])
    API_CUSTOMER_DELETE = wrap_function(API, 'customer_delete',
            None, [c_void_p])

    def __init__(self, name, email):
        self.ptr = self.API_CUSTOMER_NEW(name, email)

    def getName(self):
        return self.API_CUSTOMER_GET_NAME(self.ptr)

    def getEmail(self):
        return self.API_CUSTOMER_GET_EMAIL(self.ptr)

    def __del__(self):
        self.API_CUSTOMER_DELETE(self.ptr)

def main():
    daniel = Customer('Daniel Lloyd', 'DanielLloyd7@gmail.com')
    print(daniel.getName())
    print(daniel.getEmail())

if __name__ == '__main__':
    main()
