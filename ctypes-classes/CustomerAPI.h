extern "C" {
	void * customer_new(char *name, char *email);
	void customer_delete(void *);
	char *customer_get_name(void *);
	char *customer_get_email(void *);
}
