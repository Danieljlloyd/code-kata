#include <stdio.h>

float add_(float *, float *);

void matmul_(float *, float *);

int main(void) 
{
	float a = 4.9;
	float b = 2.4;
	int i;

	float a_matrix[4] = { 
		2.4, 1.4, 
		5.2, 5.1 
		};

	float b_matrix[4] = { 
		6.2, 4.6, 
		2.3, 6.1 
		};

	float c = add_(&a, &b);

	printf("Answer is %f\n", c);

	matmul_(a_matrix, b_matrix);

	puts("");
	puts("Matrix multiplication");
	puts("---------------------");
	for (i=0; i<2; i++) {
		printf("%f, %f\n", a_matrix[i*2 + 1], a_matrix[i*2 + 2]);
	}
}
