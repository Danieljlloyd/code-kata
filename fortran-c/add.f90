real function add(x, y) result(res)
        implicit none
        real, intent(in) :: x, y

        res = x + y

        return
end function add

subroutine matmul(a, b)
        complex, dimension(2,2), intent(inout) :: a
        complex, dimension(2,2), intent(in) :: b

        a = a * b
end subroutine matmul
