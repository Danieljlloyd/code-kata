/*
 * An example of fixed point arithmetic in C. The formula used is Newton's
 * first law.
 */

#include <stdio.h>
#include <unistd.h>

#define MASS 50
#define ACCELERATION_ADC_RANGE 10

struct Inputs {
	int acceleration;
};

struct Outputs {
	int force;
};

int read_input(struct Inputs *);
int write_output(struct Outputs *);
int compute_force(int acceleration);

int main(void)
{
	int rc;

	struct Inputs inputs;
	struct Outputs outputs;

	rc = read_input(&inputs);
	if (rc) {
		fprintf(stderr, "Failed to read: %d", rc);
		_exit(-1);
	}

	outputs.force = compute_force(inputs.acceleration);

	rc = write_output(&outputs);
	if (rc) {
		fprintf(stderr, "Failed to write: %d", rc);
		_exit(-1);
	}
}

int read_input(struct Inputs *inputs)
{
	float f_acceleration;
	float scaled_acceleration;

	printf("What is the current acceleration? > ");
	scanf("%f", &f_acceleration);

	scaled_acceleration = f_acceleration * 16.0;

	inputs->acceleration = scaled_acceleration;

	return 0;
}

int write_output(struct Outputs *outputs)
{
	float scaled_force;
	float unscaled_force;

	scaled_force = outputs->force;

	unscaled_force = scaled_force / 16.0;

	printf("Force: %f\n", unscaled_force);

	return 0;
}

int compute_force(int acceleration)
{
	int force;

	force = MASS * acceleration;

	return force;
}
