#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include "threadsafe_state.h"

int threadsafe_state_new(struct ThreadsafeState *self, char *name)
{
	int rc;

	/* Give this object a new id. */
	static int id_count = 0;
	self->id = id_count;
	id_count++;

	/* Use the given name. */
	strcpy(self->name, name);

	/* Initialise attribute to 1. */
	self->attr = 0;

	/* Initialise the mutex. */
	rc = pthread_mutex_init(&self->mtx, NULL);
	if (rc != 0) {
		fprintf(stderr, "Failed to initialize mutex: %d\n", rc);
		exit(-1);
	}
}

int threadsafe_state_destroy(struct ThreadsafeState *self)
{
	int rc;

	/* Destroy the mutex. */
	rc = pthread_mutex_destroy(&self->mtx);
	if (rc != 0) {
		fprintf(stderr, "Failed to destroy mutex: %d\n", rc);
		exit(-1);
	}
}

int threadsafe_state_increment_attr(struct ThreadsafeState *self)
{
	int rc;

	/* Lock the mutex. */
	rc = pthread_mutex_lock(&self->mtx);
	if (rc != 0) {
		fprintf(stderr, "Failed to get mutex lock: %d\n", rc);
		exit(-1);
	}

	/* Update the attr variable. */
	int old = self->attr;
	old = old + 1;
	self->attr = old;

	/* Unlock the mutex. */
	rc = pthread_mutex_unlock(&self->mtx);
	if (rc != 0) {
		fprintf(stderr, "Failed to unlock mutex: %d\n", rc);
		exit(-1);
	}
}
