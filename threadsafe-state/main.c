#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>

#include "threadsafe_state.h"

/* State to be shared between the two threads. */
struct SharedState {
	struct ThreadsafeState *state;
	int loops;
};

/* Thread that increments the shared variable N times. */
static void *my_thread(void *arg)
{
	struct SharedState *my_shared_state = arg;

	int i;
	for (i=0; i<my_shared_state->loops; i++) {
		threadsafe_state_increment_attr(my_shared_state->state);
	}
}

int main(int argc, char *argv[])
{
	pthread_t t1, t2;
	int loops, rc;

	loops = 10000000;

	/* Create the threadsafe state. */
	struct ThreadsafeState my_state;
	threadsafe_state_new(&my_state, "Example threadsafe state");

	/* Create the shared state structure. */
	struct SharedState my_shared_state = {
		.state = &my_state,
		.loops = loops
	};

	/* Create thread 1. */
	rc = pthread_create(&t1, NULL, my_thread, &my_shared_state);
	if (rc != 0) {
		fprintf(stderr, "Failed to create t1: %d", rc);
		exit(-1);
	}

	/* Create thread 2. */
	pthread_create(&t2, NULL, my_thread, &my_shared_state);
	if (rc != 0) {
		fprintf(stderr, "Failed to create t2: %d", rc);
		exit(-1);
	}

	/* Join thread 1. */
	rc = pthread_join(t1, NULL);
	if (rc != 0) {
		fprintf(stderr, "Failed to join t1: %d", rc);
		exit(-1);
	}

	/* Join thread 2. */
	pthread_join(t2, NULL);
	if (rc != 0) {
		fprintf(stderr, "Failed to join t2: %d", rc);
		exit(-1);
	}

	/* Display the result of the increments to the user. */
	printf("attr = %d\n", my_state.attr);

	/* Clean up the state used in testing. */
	threadsafe_state_destroy(&my_state);
}
