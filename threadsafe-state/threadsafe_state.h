#ifndef _THREADSAFE_STATE
#define _THREADSAFE_STATE

#include <pthread.h>

struct ThreadsafeState {
	int id;
	char name[50];
	int attr;
	pthread_mutex_t mtx;
};

int threadsafe_state_new(struct ThreadsafeState *, char *);
int threadsafe_state_destroy(struct ThreadsafeState *);
int threadsafe_state_increment_attr(struct ThreadsafeState *);

#endif
