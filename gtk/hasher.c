#include <stdlib.h>
#include <string.h>

/* GTK */
#define OPENSSL_API_COMPAT 0x0908 /* Still use compatible symbol defs. */
#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <glib/gstring.h>
#include <gdk/gdkkeysyms.h>

#include <openssl/md5.h>

#define HEX_STR_SIZE 32

struct AppWidgets
{
	GtkWidget *window1;
	GtkWidget *entry1;
	GtkWidget *entry2;
	GtkWidget *label1;
};

gchar *make_hash(const gchar *input)
{
	int i;
	unsigned char bytes[16];
	gchar hexdigits[2];
	gchar *hexstring = g_malloc(HEX_STR_SIZE);
	memset(hexstring, 0, HEX_STR_SIZE);

	MD5((unsigned char *)input, g_utf8_strlen(input, 99), bytes);

	for (i=0; i<16; i++) {
		g_sprintf(hexdigits, "%02x", bytes[i]);
		g_strlcat(hexstring, hexdigits, 32);
	}

	return hexstring;
}

void check_hash(GtkWidget *widget, struct AppWidgets *app)
{
	gchar *hash = make_hash(gtk_entry_get_text(GTK_ENTRY(app->entry1)));

	if (!g_strcmp0(hash, gtk_entry_get_text(GTK_ENTRY(app->entry2)))) {
		gtk_label_set_text(GTK_LABEL(app->label1), "valid");
	} else {
		gtk_label_set_text(GTK_LABEL(app->label1), "fail");
	}

	g_free(hash);
}

void generate_hash(GtkWidget *widget, struct AppWidgets *app)
{
	gchar *hash = make_hash(gtk_entry_get_text(GTK_ENTRY(app->entry1)));
	gtk_entry_set_text(GTK_ENTRY(app->entry2), hash);
	g_free(hash);
}

int main(int argc, char **argv)
{
	GtkBuilder *builder;
	struct AppWidgets *app = g_slice_new(struct AppWidgets);
	GError *err = NULL;

	gtk_init(&argc, &argv);
	builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, "hasher.xml", &err);

	if (err) {
		g_error(err->message);
		g_error_free(err);
		g_slice_free(struct AppWidgets, app);
		return 1;
	}

	app->entry1 = GTK_WIDGET(gtk_builder_get_object(builder, "entry1"));
	app->entry2 = GTK_WIDGET(gtk_builder_get_object(builder, "entry2"));
	app->label1 = GTK_WIDGET(gtk_builder_get_object(builder, "label1"));

	gtk_builder_connect_signals(builder, app);
	g_object_unref(G_OBJECT(builder));

	gtk_main();

	g_slice_free(struct AppWidgets, app);
	return 0;
}
