#include <stdio.h>

#include <dog.h>
#include <animal.h>

char dog_talk()
{
	printf("Woof!\n");

	return 0;
}

char dog_create(struct Dog *self, char *name, char *species, char age)
{
	animal_create((struct Animal *)self, name, species, age);

	self->talk = dog_talk;

	return 0;
}

char dog_destroy(struct Dog *self)
{
	animal_destroy((struct Animal *)self);

	return 0;
}
