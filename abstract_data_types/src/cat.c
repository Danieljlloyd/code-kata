#include <stdio.h>

#include <animal.h>
#include <cat.h>

char cat_create(struct Cat *self, char *name, char *species, char age)
{
	animal_create((struct Animal *)self, name, species, age);
	self->talk = cat_talk;

	return 0;
}

char cat_destroy(struct Cat *self)
{
	animal_destroy((struct Animal *)self);

	return 0;
}

char cat_talk() {
	printf("Meowwwww!\n");

	return 0;
}
