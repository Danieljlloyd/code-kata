#include <animal.h>
#include <string.h>

char animal_create(struct Animal *self, char *name, char *species, char age)
{
	// Initialise the state of the new animal.
	strcpy(self->name, name);
	strcpy(self->species, species);
	self->age = age;

	return 0;
}

char animal_destroy(struct Animal *self)
{
	memset(self->name, '\0', STRING_SIZE);
	memset(self->species, '\0', STRING_SIZE);
	self->age = 0;	
	self->talk = NULL;

	return 0;	
}

char animal_grow(struct Animal *self)
{
	self->age++;

	return 0;
}
