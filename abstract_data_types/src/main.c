#include <stdio.h>

#include <animal.h>
#include <dog.h>
#include <cat.h>

int main(void)
{
	struct Dog my_dog;
	dog_create(&my_dog, "Spot", "Dalmatian", 12);

	struct Cat my_cat;
	cat_create(&my_cat, "Felix", "Black Cat", 6);

	// Inherited method.
	animal_grow((struct Animal *) &my_dog);
	printf("My dog is now %d years old.\n", my_dog.age);

	animal_grow((struct Animal *) &my_cat);
	printf("My cat is now %d years old.\n", my_cat.age);

	// Polymorphic method.
	struct Animal *my_animal_array[2];
	my_animal_array[0] = &my_dog;
	my_animal_array[1] = &my_cat;

	for (int i=0; i<2; i++)
		my_animal_array[i]->talk();
}
