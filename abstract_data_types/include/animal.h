#ifndef _ANIMAL_H
#define _ANIMAL_H

#include <general.h>

struct Animal {
	char name[STRING_SIZE];
	char species[STRING_SIZE];
	char age; 
	char (*talk) ();
};

char animal_create(struct Animal *self, char *name, char *species, char age);
char animal_destroy(struct Animal *self);
char animal_grow(struct Animal *self);

#endif
