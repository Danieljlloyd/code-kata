#ifndef _DOG_H
#define _DOG_H

#include <animal.h>

struct Dog {
	struct Animal;
};

char dog_create(struct Dog *self, char *name, char *species, char age);
char dog_destroy(struct Dog *self);
char dog_talk();

#endif
