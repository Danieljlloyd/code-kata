#ifndef _CAT_H
#define _CAT_H

#include <animal.h>

struct Cat {
	struct Animal;
};

char cat_create(struct Cat *self, char *name, char *species, char age);
char cat_destroy(struct Cat *self);
char cat_talk();

#endif
