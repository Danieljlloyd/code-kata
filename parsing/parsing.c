#include <stdio.h>
#include <string.h>

#define LINE_SIZE 100

int main(void) 
{
	FILE *fp = fopen("testfile.txt", "r");

	char line[LINE_SIZE];
	char *save, *tmp;
	char *words[10];

	int i, j;

	while ((fgets(line, LINE_SIZE, fp)) != NULL) {
		save = NULL;
		tmp = line;

		for(i=0; words[i-1] != NULL; i++) {
			words[i] = strtok_r(tmp, ",", &save);
			tmp = NULL;
		}

		for (j=0; j<i-1; j++) {
			printf("%s\n", words[j]);
		}
	}

	fclose(fp);

	return 0;
}
