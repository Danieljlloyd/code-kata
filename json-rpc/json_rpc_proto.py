SAMPLE_REQUEST = {
  "version": "1.0",
  "proc": "SetParameter",
  "id": "1",
  "format": "JSON",
  "passwd": "a289fa4252ed5af8e3e9f9bee545c172",
  "params":
  {
    "devices":
    [
      {
        "key": "WR21TL06:2000101000",
        "channels":
        [
          {
            "meta": "Mode",
            "value": "Mpp Operation"
          }
        ]
      },
      {
        "key": "WR21TL06:2000101001",
        "channels":
        [
          {
            "meta": "Mode",
            "value": "Stop"
          }
        ]
      }
    ]
  }
}

SAMPLE_RESPONSE = {
  "version": "1.0",
  "id": "1",
  "format": "JSON",
  "proc": "SetParameter",
  "result":
  {
    "devices":
    [
      {
        "key": "WR21TL06:2000101000",
        "channels":
        [
          {
            "min": "0",
            "max": "7",
            "meta": "Mode",
            "options":
            [
              "Stop",
              "Konstantspg.",
              "Mpp-Betrieb",
              "Res1",
              "Res2",
              "Res3",
              "Res4",
              "Res5"
            ],
            "value": "Mpp Operation",
            "name": "Mode",
            "unit": ""
          }
        ]
      },
      {
        "key": "WR21TL06:2000101001",
        "channels":
        [
          {
            "min": "0",
            "max": "7",
            "meta": "Mode",
            "options":
            [
              "Stop",
              "Konstantspg.",
              "Mpp-Betrieb",
              "Res1",
              "Res2",
              "Res3",
              "Res4",
              "Res5"
            ],
            "value": "Stop",
            "name": "Mode",
            "unit": ""
          }
        ]
      }
    ]
  }
}

def handle_request(request):
    """
    Pretend to be the SMA Webbox and generate the reply corresponding to
    the request.
    """

    if request == SAMPLE_REQUEST:
        return SAMPLE_RESPONSE

def make_request(actions, command_config):
    password = "a289fa4252ed5af8e3e9f9bee545c172"

    # Convert the commands to the RPC representation.
    rpc_commands = []
    for action in actions:
        for conf in command_config:
            if action['metric'] == conf['metric']:
                rpc_commands.append({
                    'key': conf['device'],
                    'channels': [
                        {
                            'meta': conf['meta'],
                            'value': action['value']
                        }
                    ]
                })

    # Construct the RPC query.
    request = {
        'version': '1.0',
        'proc': 'SetParameter',
        'id': '1',
        'format': 'JSON',
        'passwd': password,
        'params': {
            'devices': rpc_commands
        }
    }

    response = handle_request(request)

    return response

def main():

    # Say this is the result of actions created from the controller.
    actions = [
        {
            'command': 'SETSWOOP',
            'metric': 'SWDINPV.INV1.mode',
            'value': 'Mpp Operation'
        },
        {
            'command': 'SETSWOOP',
            'metric': 'SWDINPV.INV2.mode',
            'value': 'Stop'
        }
    ]

    # The command config that is loaded by the Webbox driver.
    command_config = [
        {
            'metric': 'SWDINPV.INV1.mode',
            'device': 'WR21TL06:2000101000',
            'meta': 'Mode'
        },
        {
            'metric': 'SWDINPV.INV2.mode',
            'device': 'WR21TL06:2000101001',
            'meta': 'Mode'
        }
    ]

    response = make_request(actions, command_config)

    print response

if __name__ == '__main__':
    main()
