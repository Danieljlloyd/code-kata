#ifndef H_CUSTOMER
#define H_CUSTOMER

class Customer
{
public:
	Customer(char *name, char *email);
	~Customer();
	char *getName();
	char *getEmail();

private:
	struct Impl;
	struct Impl *p_impl;
};

#endif
