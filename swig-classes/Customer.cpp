#include <stdlib.h>
#include <string.h>
#include "Customer.h"

#define STRING_SIZE 50

struct Customer::Impl
{
	char name[STRING_SIZE];
	char email[STRING_SIZE];
};

Customer::Customer(char *name, char *email)
{
	p_impl = (struct Customer::Impl *) malloc(sizeof(struct Customer::Impl));
	strncpy(p_impl->name, name, STRING_SIZE);
	strncpy(p_impl->email, email, STRING_SIZE);
}

Customer::~Customer()
{
	free(p_impl);
}

char *Customer::getName()
{
	return p_impl->name;
}

char *Customer::getEmail()
{
	return p_impl->email;
}
