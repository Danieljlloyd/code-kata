import swig_example as se

def main():
    daniel = se.Customer('Daniel Lloyd', 'DanielLloyd7@gmail.com')
    print('My name is {0} and my email is {1}'.format(
        daniel.getName(), daniel.getEmail()))

if __name__ == '__main__':
    main()
