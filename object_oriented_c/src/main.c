#include <stdio.h>
#include "base_class.h"
#include "derived_class.h"

int main(void)
{
	struct base_class base_object;		
	base_class_new(&base_object, 3, 2.8, "Hello world");

	struct derived_class derived_object;
	derived_class_new(&derived_object, 8, 1.7, "To infinity and beyond", 1992010112, 3.22);

	printf("Inheritance\n");
	printf("-----------\n\n");

	double res;
	res = base_object.base_ops->add(&base_object);
	printf("Base class result: %lf\n", res);

	res = derived_object.base_ops->add((struct base_class *) &derived_object);
	printf("Derived class result %lf\n", res);


	printf("\n\nPolymorphism\n");
	printf(    "------------\n\n");

	struct base_class *base_class_list[2] = { &base_object, (struct base_class *) &derived_object };

	int i;
	for(i=0; i<2; i++) {
		base_class_list[i]->base_ops->print(base_class_list[i]);
	}
}
