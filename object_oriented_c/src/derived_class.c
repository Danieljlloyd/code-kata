#include <stdio.h>
#include "base_class.h"
#include "derived_class.h"

struct base_class_ops derived_base_ops = {
	.print = derived_class_print,
	.add = base_class_add
};

void derived_class_new(struct derived_class *self, int a, double b, 
		const char *str, long c, float k)
{
	base_class_new((struct base_class *) self, a, b, str);
	self->derived_data.c = c;
	self->derived_data.k = k;
	self->base_ops = &derived_base_ops;
}

void derived_class_print(struct base_class *self_p)
{
	struct derived_class *self = (struct derived_class *) self_p;

	printf("Derived class:\n"
	       "\t%d\n"
	       "\t%lf\n"
	       "\t%s\n" 
	       "\t%ld\n"
	       "\t%f\n\n",
	       self->base_data.a, self->base_data.b, self->base_data.str,
	       self->derived_data.c, self->derived_data.k);
}

