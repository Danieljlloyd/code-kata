#include <stdio.h>
#include <string.h>
#include "base_class.h"

struct base_class_ops base_ops = {
	.print = base_class_print,
	.add = base_class_add
};

void base_class_new(struct base_class *self, int a, double b, const char *str)
{
	self->base_ops = &base_ops;
	self->base_data.a = a;
	self->base_data.b = b;
	strcpy(self->base_data.str, str);
}

double base_class_add(struct base_class *self)
{	
	return self->base_data.a + self->base_data.b;
}

void base_class_print(struct base_class *self)
{
	printf("Base class:\n"
	       "\t%i\n"
	       "\t%lf\n"
	       "\t%s\n\n", 
	       self->base_data.a, self->base_data.b, self->base_data.str);
}
