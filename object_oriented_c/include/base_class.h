#ifndef BASE_CLASS_H
#define BASE_CLASS_H

struct base_class;

struct base_class_data {
	int a;
	double b;
	char str[100];
};

struct base_class_ops {
	void (*print) (struct base_class *);
	double (*add) (struct base_class *); 
};

struct base_class {
	struct base_class_ops *base_ops;
	struct base_class_data base_data;
};

void base_class_new(struct base_class *, int, double, const char *);
double base_class_add(struct base_class *);
void base_class_print(struct base_class *);

extern struct base_class_ops base_ops;

#endif
