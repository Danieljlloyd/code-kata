#ifndef DERIVED_CLASS_H
#define DERIVED_CLASS_H

#include "base_class.h"

struct derived_class_data {
	long c;	
	float k;
};

struct derived_class {
	struct base_class_ops *base_ops;
	struct base_class_data base_data;
	struct derived_class_data derived_data;
};

void derived_class_new(struct derived_class *, int, double, const char *, long, float);
void derived_class_print(struct base_class *);

extern struct base_class_ops derived_base_ops;

#endif
