#ifndef _EMPLOYEE_H
#define _EMPLOYEE_H

struct Employee {
	int id;
	char name[50];
	int age;
	int salary;
};

struct Employee employee_new(int id, char *name, int age, int salary);
int employee_save(struct Employee *self);
struct Employee find_employee_by_id(int id);
struct Employee prompt_for_employee();
int create_employees_table();
int create_database();

#endif
