from datetime import datetime, timedelta

class DeviceModes(object):
    START = 0
    STOP = 1

class State(object):
    def __init__(self, energy_model):
        self.energy_model = energy_model

    def enter_state(self):
        pass

    def exit_state(self):
        pass

    def processing(self):
        pass

class StopState(State):
    def __init__(self):
        super(StopState, self).__init__(energy_model)

    def processing(self):
        print('In stop state!')

class StartingState(State):
    def __init__(self):
        super(StartingState, self).__init__(energy_model)

    def processing(self):
        print('In starting state!')

class RunningState(State):
    def __init__(self):
        super(RunningState, self).__init__(energy_model)

    def processing(self):
        print('In running state!')

class StoppingState(State):
    def __init__(self):
        super(StoppingState, self).__init__(energy_model)

    def processing(self):
        print('In stopping state!')

class SimpleInputDrivenEnergyModel(self):
    def __init__(self, max_output, loss_percentage):
        self.max_output = max_output
        self.loss_percentage = loss_percentage

    def calculate(self, proposed_output_power):
        input_power = 0.0
        output_power = 0.0

        if proposed_output_power > self.max_output:
            proposed_output_power = self.max_output

        output_power = proposed_output_power
        input_power = (1 - self.loss_percentage) * proposed_output_power

class SimpleOutputDrivenEnergyModel(self):
    def __init__(self, max_input, loss_percentage):
        self.max_input = max_input
        self.loss_percentage = loss_percentage

    def calculate(self, proposed_input_power):
        output_power = 0.0
        input_power = 0.0

        if proposed_input_power > self.max_input:
            proposed_input_power = self.max_input

        input_power = proposed_input_power
        output_power = (1 - self.loss_percentage) * proposed_input_power

class SimulatedDevice(object):
    def __init__(self, server):
        self.server = server

        self.energy_model = SimpleInputDrivenEnergyModel(250.0, 0.05)

        self.s_stop = StopState(self.energy_model)
        self.s_starting = StartingState(self.energy_model)
        self.s_running = RunningState(self.energy_model)
        self.s_stopping = StoppingState(self.energy_model)

        self.current_state = self.s_stop

        self.time_state_entered = datetime.utcnow()

        self.start_sequence_duration = 5
        self.stop_sequence_duration = 10

    def update(self, output_power):
        mode = 0

        if self.current_state == self.s_stop:
            if mode == DeviceModes.START:
                next_state == self.s_starting
                self.time_state_entered = datetime.utcnow()

        elif self.current_state == self.s_starting:
            if datetime.utcnow() > self.time_state_entered + \
                timedelta(seconds=self.start_sequence_duration):

                next_state = self.s_running

        elif self.current_state == self.s_running:
            if mode == DeviceModes.STOP:
                next_state = self.s_stopping
                self.time_state_entered = datetime.utcnow()

        elif self.current_state == self.s_stopping:
            if datetime.utcnow() > self.time_state_entered + \
                timedelta(seconds=self.stop_sequence_duration):

                next_state = self.s_stop

        if next_state != self.current_state:
            self.current_state.exit_state()
            next_state.enter_state()
            self.current_state = next_state

        self.current_state.periodic_processing()
