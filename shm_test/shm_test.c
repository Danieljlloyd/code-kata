#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include "semun.h"


#define SHM_KEY 0x1424
#define SEM_KEY 0x8276
#define OBJ_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define BUF_SIZE 1024

#define WRITE_SEM 0
#define READ_SEM 1

char use_sem_undo = 0;
char retry_on_eintr = 1;

struct shmseg {
	int number;
};

int reserve_sem(int, int);
int release_sem(int, int);
int init_sem_available(int, int);
int init_sem_in_use(int, int);
void err_exit(const char *);

int main(void) 
{
	int ret = 0;

	int shmid = shmget(SHM_KEY, sizeof(struct shmseg), IPC_CREAT | OBJ_PERMS);
	if (shmid == -1)
		err_exit("Couldn't obtain shared memory: shmget\n");

	struct shmseg *shmp = shmat(shmid, NULL, 0);
	if (shmp == (void *) -1) 
		err_exit("Couldn't obtain shared memory: shmat\n");

	int semid = semget(SEM_KEY, 1, IPC_CREAT | OBJ_PERMS);

	ret = init_sem_available(semid, WRITE_SEM);
	if (ret == -1)
		err_exit("Couldn't initialise semaphore\n.");

	int i;
	clock_t start, end;
	start = clock();
	for (i=0; i<1e7; i++) {
		ret = reserve_sem(semid, WRITE_SEM);
		if (ret == -1)
			err_exit("Couldn't reserve semaphore\n");

		shmp->number = 1;

		ret = release_sem(semid, WRITE_SEM);
		if (ret == -1) 
			err_exit("Couldn't release semaphore\n");		
	}
	end = clock();

	shmdt(shmp);

	double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
	printf(
		"Total time: %lf\n"
		"Average time: %lf\n",
		time_taken, time_taken / (1e7 * 1.0)
		);

	return 0;
}

int reserve_sem(int semid, int semnum)
{
	struct sembuf sops;

	sops.sem_num = semnum;
	sops.sem_op = -1;
	sops.sem_flg = use_sem_undo ? SEM_UNDO : 0;

	while (semop(semid, &sops, 1) == -1)
		if(errno != EINTR || !retry_on_eintr)
			return -1;

	return 0;
}

release_sem(int semid, int semnum)
{
	struct sembuf sops;

	sops.sem_num = semnum;
	sops.sem_op = 1;
	sops.sem_flg = use_sem_undo ? SEM_UNDO : 0;

	return semop(semid, &sops, 1);
}

int init_sem_available(int semid, int semnum)
{
	union semun arg;

	arg.val = 1;
	return semctl(semid, semnum, SETVAL, arg);
}

int init_sem_in_use(int semid, int semnum)
{
	union semun arg;

	arg.val = 0;
	return semctl(semid, semnum, SETVAL, arg);
}

void err_exit(const char *error)
{
	fprintf(stderr, error);
	exit(-1);
}
