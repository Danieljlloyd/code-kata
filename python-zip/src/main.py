from flask import Flask
from waitress import serve
import other
import argparse

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World!'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', help='port', default=41625)
    args = parser.parse_args()

    serve(app, host='0.0.0.0', port=args.p)
