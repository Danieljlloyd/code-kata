#!/bin/bash

#
# This script is a utility that allows you to build a runnable python directory
# into a zip file that can be executed.
#

build_dir="${PWD}/dist"
src_dir="$1"
prog="$2"

clean () {
	rm -rf "${build_dir}"
	rm -rf "${prog}"
}

fail () {
	clean
	exit -1
}

make_dirs () {
	mkdir "${build_dir}"
}

venv_setup () {
	virtualenv -v venv
	source venv/bin/activate
	pip -v install -r requirements.txt
}

venv_cleanup () {
	deactivate
	rm -rf venv
}

install_python_deps () {
	venv_setup
	pip -v install -r requirements.txt -t "${build_dir}"
	venv_cleanup
}

prepare_build_dir () {
	cp "${src_dir}/"* "${build_dir}/"
	install_python_deps
}

make_executable () {
	sed -i '1s;^;#!/usr/bin/env python\n;' $1 || return -1
	chmod 755 $1 || return -1
}

build () {
	pushd ${build_dir} || return -1
	zip -vr /tmp/work.zip * || return -1
	popd
	mv /tmp/work.zip "${prog}" || return -1
	make_executable "${prog}" || return -1
}

echo "Building python executable..."
clean
make_dirs || fail
prepare_build_dir || fail
build || fail
echo "Successfully built python executable."
