import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name='example_project',
    version='0.0.3',
    author='Daniel Lloyd',
    author_email='DanielLloyd7@gmail.com',
    description='A test demonstration of setuptools',
    license='GPL',
    keywords='demo test setuptools',
    url='',
    packages=['example_project'],
    scripts=['scripts/test_script'],
    long_description=read('README'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Utilities',
        'License :: OSI Approved :: GPL License'
    ]
)
