#ifndef MY_CUSTOMERS_H
#define MY_CUSTOMERS_H

/*
 * A simple customer class.
 */

#define CUSTOMERS_MAX_SIZE 100
#define CUSTOMER_NAME_SIZE 50
#define CUSTOMER_EMAIL_SIZE 50

/* Customer data structure. */
typedef struct customer_s {
	char name[CUSTOMER_NAME_SIZE];
	char email[CUSTOMER_EMAIL_SIZE];
	int age;
	int sex;
} customer_t;

/* Collection of customers. */
typedef struct customers_s {
	customer_t data[CUSTOMERS_MAX_SIZE];
	int n;
} customers_t;

/* Create a customer with the given attributes. */
customer_t customer_create(char name[CUSTOMER_NAME_SIZE],
		char email[CUSTOMER_EMAIL_SIZE], int age, int sex);

/* Create a customers collection. */
customers_t customers_create(void);

/* Get a customer by name from the customers collection. */
customer_t customers_get(customers_t *self, char name[CUSTOMER_NAME_SIZE]);

/* Update a customer in the customer collection referred to by name. */
int customers_update(customers_t *self, customer_t customer);

#endif
