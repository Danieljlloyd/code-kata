/*
 * This is an example of how typedef can be used to make sure that all data
 * is passed by value. This is a good way to do things generally as it generates
 * more simple and readable code.
 */

#include <customer.h>
#include <stdio.h>

int print_customers(customers_t customers)
{
	/* Find Daniel and print him. */
	customer_t my_customer = customers_get(&customers, "Daniel Lloyd");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", my_customer.name,
			my_customer.email, my_customer.age, my_customer.sex);

	/* Find Shekar and print her. */
	my_customer = customers_get(&customers, "Shekar Seldon");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", my_customer.name,
			my_customer.email, my_customer.age, my_customer.sex);

	/* Find Dad and print him. */
	my_customer = customers_get(&customers, "Steven Lloyd");
	printf("name: %s\nemail: %s\nage: %d\nsex: %c\n\n", my_customer.name,
			my_customer.email, my_customer.age, my_customer.sex);

	return 0;
}

customers_t create_all_customers(void)
{
	customers_t my_customers = customers_create();

	/* Create a customer. */
	customer_t my_customer;
	my_customer = customer_create("Daniel Lloyd", "DanielLloyd7@gmail.com",
			24, 'm');
	customers_update(&my_customers, my_customer);

	/* Create another customer. */
	my_customer = customer_create("Shekar Seldon", "ss.dorji@gmail.com",
			24, 'f');
	customers_update(&my_customers, my_customer);

	/* Create another customer. */
	my_customer = customer_create("Steven Lloyd", "stevenjlloyd@gmail.com",
			57, 'm');
	customers_update(&my_customers, my_customer);

	return my_customers;
}

int main(int argc, char *argv[])
{
	customers_t my_customers = create_all_customers();
	print_customers(my_customers);

	return 0;
}
