#include <customer.h>
#include <string.h>

customer_t customer_create(char name[CUSTOMER_NAME_SIZE],
		char email[CUSTOMER_EMAIL_SIZE], int age, int sex)
{
	customer_t customer;

	strcpy(customer.name, name);
	strcpy(customer.email, email);
	customer.age = age;
	customer.sex = sex;

	return customer;
}

customers_t customers_create(void)
{
	customers_t customers;

	customers.n = 0;
	memset(customers.data, 0, sizeof(customer_t) * CUSTOMERS_MAX_SIZE);
}

customer_t customers_get(customers_t *self, char name[CUSTOMER_NAME_SIZE])
{
	customer_t customer = customer_create("", "", 0, 0);

	/* Try and find the customer and return it. */
	for (int i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].name, name)) {
			customer = self->data[i];
		}
	}

	return customer;
}

int customers_update(customers_t *self, customer_t customer)
{
	/* Try and find the customer in the collection already and update it. */
	for (int i=0; i<self->n; i++) {
		if (!strcmp(self->data[i].name, customer.name)) {
			self->data[i] = customer;
			return 0;
		}
	}

	/* If no customer found, create a new one. */
	self->data[self->n] = customer;
	self->n++;
	return 0;
}
