/Goodbye/{
	count += 1;
	printf("%s\n", $0);
	printf("Count is: %d\n", count);
	fflush(stdout);
}

END {
	printf("Finished processing.\n");
	printf("Counted %d messages.\n", count);
	fflush(stdout);
}
