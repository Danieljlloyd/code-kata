/* Creates a source of information. */
#include <stdio.h>
#include <unistd.h>

int main(void)
{
	int i = 0;
	for (i = 0; i < 20; i++) {
		printf("Hello world!\n");
		fflush(stdout);
		sleep(1);
	}
}
