! Function that mimicks the behaviour of MATLAB's linspace.
subroutine linspace (x, x0, xn, n)
        implicit none

        double precision, dimension (n), intent(out) :: x
        double precision, intent(in) :: x0, xn 
        integer, intent(in) :: n
        integer :: i
        double precision :: dx

        dx = (xn - x0) / n

        x(1) = x0

        do i=2, n
                x(i) = x(i-1) + dx
        end do
end subroutine linspace

! Function that takes in an x vector and computes the normal distribution with
! a specified mean and variance.
!
! Inputs:
! * x - The input vector to compute y in reference to.
! * mean - The mean of the output normal distribution.
! * var - The variance of the output normal distribution.
! * n - The size of the input vector.
!
! Outputs:
! * y - The gaussian distribution specified by the inputs.
subroutine gaussian (y, x, mean, var, n)
        double precision, parameter :: pi=3.141592
        double precision, intent(in) :: mean, var
        double precision, dimension (n), intent(in) :: x
        double precision, dimension (n), intent(out) :: y

        y = exp(-( ((x-mean)**2) / (2*var) ))
        y = y * sqrt(2*pi*var)
end subroutine gaussian

! Program that demonstrates the ability of the linspace and gaussian functions.
program test_gaussian
        implicit none

        double precision, dimension (1000) :: x, y
        double precision, dimension (1000) :: gaussian_distribution
        double precision :: mean, var, x0, xn
        integer n, i
        character(LEN=20) :: outfmt

        ! Define the characteristics of the Gaussian (models something like an
        ! average solar day.
        x0 = 0.0
        xn = 24.0
        mean = 12
        var = 2.5
        n = 1000

        ! Create the input vector.
        call linspace(x, x0, xn, n)

        ! Generate the Gaussian distribution.
        call gaussian(y, x, mean, var, n)

        ! Write the resultant vectors to stdout.
        do i = 1, n
                write (*,"(F5.2,F5.2)") x(i), y(i)
        end do
end program test_gaussian
