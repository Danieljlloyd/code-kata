set terminal pngcairo
set output "plot.png"

set title "Normal distribution"
set xlabel "time (hours)"
set ylabel "magnitude"

plot "data.out" using 1:2 title "Normal Distribution" with lines
