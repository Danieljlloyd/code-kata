/*
Author: Daniel Lloyd
Date: 2017-08-28

This is an example of how to use fgets and strtok to parse input. The chosen
example is to parse the ARP table in the /proc filesystem. This is a useful
application which can be used to bridge the data-link and network layers of a
Linux system. Most useful if there is some other infrastructure available to
ping for devices in order to fill the ARP cache.
*/

#include <string.h>
#include <stdio.h>

#define MAX_ADDR 250	// Maximum number of addresses to be associated.
#define LINE_SIZE 200	// Maximum size of a line.

/*
Structure to associate MAC and IP addresses.
*/
struct MacIpPair {
	char ip[16];
	char mac[18];
};

/*
Array data structure for holding MAC/IP pairs.
*/
struct MacIpArray {
	struct MacIpPair data[MAX_ADDR];
	char count;
};

/*
Routine to initialise the MAC/IP array. Zeroes the data portion and sets the
count to zero.
*/
int mac_ip_array_init(struct MacIpArray *self)
{
	memset(self->data, '\0', sizeof(struct MacIpPair) * MAX_ADDR);
	self->count = 0;
}

/*
Routine to append an IP/MAC pair to the MAC/IP array.
*/
int mac_ip_array_append(struct MacIpArray *self, char *ip, char *mac)
{
	strcpy(self->data[self->count].ip, ip);
	strcpy(self->data[self->count].mac, mac);

	self->count++;
}

/*
Routine demonstrating the use of strtok to parse information from the /proc
filesystem.
*/
int main(void)
{
	int rc;

	// Create an array of MAC and IP pairs.
	struct MacIpArray my_array;
	mac_ip_array_init(&my_array);

	// Open ARP table file
	FILE *fp = fopen("/proc/net/arp", "r");
	if (fp == NULL)
		perror("Unable to open ARP table file.");

	// For each line in the file that is not the first line.
	char nr = 0;
	while (1) {
		nr++;
		if (nr == 1)
			continue;

		// Read a line from the file,
		char line[LINE_SIZE];
		if (fgets(line, 150, fp) == NULL) break;

		// split the line into tokens and find the MAC and IP addresses.
		char nf = 0;
		char *token;
		char ip[16];
		char mac[18];
		for (token = strtok(line, " "); token;
			token = strtok(NULL, " ")) {

			if (token != "")
				nf++;

			if (nf == 1)
				strcpy(ip, token);

			if (nf == 4)
				strcpy(mac, token);
		}

		// store MAC and IP address into the array.
		mac_ip_array_append(&my_array, ip, mac);
	}

	// Close the ARP table file.
	rc = fclose(fp);
	if (rc == EOF)
		perror("Unable to close ARP table file.");

	// Print out the MAC and IP associations to the user.
	char i;
	for (i=0; i<my_array.count; i++) {
		printf("%s => %s\n", my_array.data[i].mac, my_array.data[i].ip);
	}
}
