#include <stdio.h>
#include <stdint.h>
#include <pid_controller.h>

struct ThermalModel
{
	float temp;
	float heat_capacity;
};

int thermal_model_init(struct ThermalModel *self, float initial_temp,
		float heat_capacity)
{
	self->temp = initial_temp;
	self->heat_capacity = heat_capacity;

	return 0;
}

int thermal_model_update(struct ThermalModel *self, int voltage)
{
	int rounded;

	self->temp = self->temp + voltage * self->heat_capacity;
	rounded = self->temp;

	return rounded;
}

int main(void)
{
	struct PIDController temp_controller;
	struct ThermalModel thermal_model;
	uint32_t now;
	uint32_t timestep;
	int32_t setpoint;
	int32_t measured;
	int32_t voltage;
	int32_t temp_setpoint;
	int32_t measured_temp;

	now = 0;
	timestep = 5;

	pid_controller_init(&temp_controller, 150, 15, 0, 20, 6);
	thermal_model_init(&thermal_model, 0, 0.01);

	temp_setpoint = 10;
	voltage = 0;
	for (int i=0; i<1000; i++) {
		measured_temp = thermal_model_update(&thermal_model, voltage);
		voltage = pid_controller_calculate(&temp_controller,
				now, temp_setpoint, measured_temp);
		printf("%d, %d, %f\n", now, voltage, thermal_model.temp);

		now += timestep;
	}
}
