set terminal png
set datafile separator ","
set output "plot.png"

set title "Temperature PID Control"
set xlabel "time (s)"
set ylabel "Temperature"

plot 'data.csv' using 1:3 with lines
