#include <stdint.h>
#include <stdio.h>

struct ThermalModel
{
	float temp;
	float heat_capacity;
};

int thermal_model_init(struct ThermalModel *self, float initial_temp,
		float heat_capacity)
{
	self->temp = initial_temp;
	self->heat_capacity = heat_capacity;

	return 0;
}

int thermal_model_update(struct ThermalModel *self, int voltage)
{
	int rounded;

	self->temp = self->temp + voltage * self->heat_capacity;
	rounded = self->temp;

	return rounded;
}

int32_t pid(uint32_t setpoint, uint32_t measured, uint32_t kp, uint32_t ki,
	uint32_t kd, int32_t ulimit, int32_t llimit)
{
	static int32_t err = 0;
	static int32_t i_err = 0;

	int32_t err_old;
	int32_t p_err;
	int32_t d_err;
	int32_t output;

	err_old = err;
	err = setpoint - measured;

	p_err = err;
	d_err = err - err_old;
	i_err += err_old;

	output = kp*p_err + ki*i_err + kd*d_err;

	/* Anti-windup processing. */
	if (output > ulimit) {
		output = ulimit;
		i_err -= err_old;
	}

	if (output < llimit) {
		output = llimit;
		i_err -= err_old;
	}

	return output;
}

int main(void)
{
	struct ThermalModel thermal_model;
	uint32_t now;
	uint32_t timestep;
	int32_t setpoint;
	int32_t measured;
	int32_t voltage;
	int32_t temp_setpoint;
	uint32_t measured_temp;

	now = 0;
	timestep = 5;

	thermal_model_init(&thermal_model, 0, 0.01);

	temp_setpoint = 10;
	voltage = 0;
	for (int i=0; i<1000; i++) {
		measured_temp = thermal_model_update(&thermal_model, voltage);

		if (i % 20 == 0)
			voltage = pid(temp_setpoint, measured_temp, 100,
				20, 0, 20 << 6, -3 << 6) >> 6;

		printf("%d, %d, %f\n", now, voltage, thermal_model.temp);

		now += timestep;
	}
}
