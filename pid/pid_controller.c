#include <stdint.h>
#include <pid_controller.h>

int pid_controller_update(struct PIDController *self, int32_t setpoint,
		int32_t measured);

int pid_controller_init(struct PIDController *self, uint32_t kp,
		uint32_t ki, uint32_t kd, uint32_t sampling_period,
		int8_t shift)
{
	self->kp = kp;
	self->ki = ki;
	self->kd = kd;

	self->sampling_period = sampling_period;

	self->err = 0;
	self->int_err = 0;
	self->last_updated = 0;
	self->output = 0;
	self->shift = shift;
}

int pid_controller_calculate(struct PIDController *self,
		uint32_t now, int32_t setpoint, int32_t measured)
{
	if (now >= self->last_updated &&
		now > self->last_updated + self->sampling_period) {

		pid_controller_update(self, setpoint, measured);
		self->last_updated = now;
	}

	return self->output;
}

int pid_controller_update(struct PIDController *self,
		int32_t setpoint, int32_t measured)
{
	int32_t err_old;
	int32_t prop_err;
	int32_t diff_err;

	err_old = self->err;
	self->err = setpoint - measured;

	prop_err = self->err;
	self->int_err += err_old;
	diff_err = self->err - err_old;

	self->output = self->kp * prop_err + self->ki * self->int_err + \
		       self->kd * diff_err;
	self->output = self->output >> self->shift;
}
