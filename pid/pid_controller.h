#include <stdint.h>

struct PIDController
{
	int32_t err;
	int32_t int_err;
	int32_t output;
	uint32_t kp;
	uint32_t ki;
	uint32_t kd;
	uint32_t sampling_period;
	uint32_t last_updated;
	int8_t shift;
};

int pid_controller_calculate(struct PIDController *self,
		uint32_t now, int32_t setpoint, int32_t measured);


int pid_controller_init(struct PIDController *self, uint32_t kp,
		uint32_t ki, uint32_t kd, uint32_t sampling_period,
		int8_t shift);
