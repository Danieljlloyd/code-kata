"""
An example of a MVC application using Tkinter. Just counts the number of clicks
on a button.
"""

import Tkinter

class ClicksModel(object):
    def __init__(self):
        self._click_count = 0

    def increase_count(self):
        self._click_count += 1

    def get_count(self):
        return self._click_count

class ClicksView(object):
    def __init__(self, root, model, controller, on_click):
        self._root = root
        self._model = model

        self._label = Tkinter.Label(self._root, text='Number of clicks: 0')
        self._label.pack()

        self._button = Tkinter.Button(self._root, text='Click me',
                command=lambda x=controller: on_click(controller))
        self._button.pack()

    def update_click_count(self):
        num_clicks = self._model.get_count()
        self._label['text'] = 'Number of clicks: '+str(num_clicks)
        self._label.update()

class ClicksController(object):
    def __init__(self, root):
        self._root = root
        self._model = ClicksModel()
        self._view = ClicksView(self._root, self._model, self,
                ClicksController.on_click)

    @staticmethod
    def on_click(self):
        self._model.increase_count()
        self._view.update_click_count()

class Master(object):
    def __init__(self):
        self._root = Tkinter.Tk()
        self._clicks_controller = ClicksController(self._root)

    def run(self):
        self._root.mainloop()

def main():
    master = Master()
    master.run()

if __name__ == '__main__':
    main()
