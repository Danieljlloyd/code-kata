////////////////////////////////////////////////////////////////////////////////
// AUTHOR: Daniel Lloyd
// DATE: Tue Mar 14 22:36:37 AEDT 2017
// 
// DESCRIPTION
// -----------
// This is an example of a sorted array with a binary search function. In this
// way, large arrays of data may be searched quickly and only take up a small
// amount of memory. It should be noted that while this is a good approach
// for large arrays, many times in a real world application the array size
// is very small. In this case you would be better served to just use an
// unsorted array and a linear search, if time permits. Simplicity is the most
// important design parameter.
//////////////////////////////////////////////////////////////////////////////// 

#include <stdio.h>
#include <string.h>

struct SortedArray {
	int data[1024];
	int len;
};

int sorted_array_init(struct SortedArray *self)
{
	memset(self->data, 0, 1024);
	self->len = 0;
}

int sorted_array_insert(struct SortedArray *self, int data)
{
	// Find where to insert.
	int *ptr = self->data + self->len;
	while (ptr > self->data) {
		if (*(ptr - 1) > data) {
			*ptr = *(ptr - 1);
			ptr--;
		} else {
			break;
		}
	}

	// Insert the data
	*ptr = data;
	self->len++;

	return 0;
}

/*
* Array search using the binary search method.
*/
int sorted_array_find(struct SortedArray *self, int search)
{
	int first = 0;
	int last = self->len - 1;

	int middle = (first + last) / 2;

	while (first <= last) {
		if (self->data[middle] < search)
			first = middle + 1;
		else if (self->data[middle] > search)
			last = middle - 1;
		else
			return middle;

		middle = (first + last) / 2;
	}

	return -1;
}

int main(void) {
	struct SortedArray my_array;

	sorted_array_init(&my_array);

	sorted_array_insert(&my_array, 20);
	sorted_array_insert(&my_array, 1);
	sorted_array_insert(&my_array, 100);
	sorted_array_insert(&my_array, 33);
	sorted_array_insert(&my_array, 21);
	sorted_array_insert(&my_array, 19);
	sorted_array_insert(&my_array, 20);

	int i = 0;
	for (i = 0; i<my_array.len; i++) {
		printf ("%d: %d\n", i, my_array.data[i]);
	}

	int search = 0;

	search = sorted_array_find(&my_array, 33);
	printf ("33 found at index %d\n", search);	

	search = sorted_array_find(&my_array, 1);
	printf ("1 found at index %d\n", search);	

	search = sorted_array_find(&my_array, 20);
	printf ("20 found at index %d\n", search);	

	search = sorted_array_find(&my_array, 100);
	printf ("100 found at index %d\n", search);	

	return 0;
}
