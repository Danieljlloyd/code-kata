import sympy as sp

print sp.sqrt(8)

x, y = sp.symbols('x y')
expr = x + 2*y

print expr

expanded_expr = sp.expand(x*expr)
print expanded_expr

print sp.factor(expanded_expr)

print '''
\documentclass{article}

\\title{SymPy LaTeX Example}
\date{\\today}
\\author{Daniel Lloyd}

\\begin{document}

\maketitle

\section{LaTeX example}
SymPy also allows you to write your proofs entirely in Python and then
output the results as latex. For example the formula

\[%s\]

is generated directly from the symbol objects in SymPy using the
sp.latex method.

\end{document}
''' % sp.latex(expanded_expr)
