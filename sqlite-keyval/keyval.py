"""
A key-value store implemented in an sqlite table.
"""

import sqlite3

class KeyVal(object):
    def __init__(self, db_name, table_name):
        self.conn = sqlite3.connect(db_name)
        self.c = self.conn.cursor()
        self.table_name = table_name

        self._table_setup()

    def __del__(self):
        self.conn.close()

    def _table_setup(self):
        """
        Create the key/value store table if it does not already exist.
        """

        self.c.execute("""
            CREATE TABLE IF NOT EXISTS {0} (
                key BLOB PRIMARY KEY,
                value BLOB
            );
        """.format(self.table_name))

    def set(self, key, value):
        """
        Set a key in the store to equal "value".
        """

        self.c.execute("""
            INSERT OR REPLACE INTO {0} VALUES(?, ?);
        """.format(self.table_name), (key, value))

        self.conn.commit()

        return True

    def get(self, key):
        """
        Get a key from the store.
        """

        self.c.execute("""
            SELECT (value) FROM {0} WHERE key=?;
        """.format(self.table_name), (key,))

        return self.c.fetchone()[0]

    def get_all(self, limit=1000):
        self.c.execute("""
            SELECT * FROM {0} LIMIT {1};
        """.format(self.table_name, limit))

        return { x: y for x, y in self.c.fetchall() }

def main():
    import random
    import time
    from pprint import PrettyPrinter

    keyval = KeyVal('keyval.sqlite', 'keyval')
    pp = PrettyPrinter()

    while True:
        voltage = random.randint(0, 1000)
        current = random.randint(0, 1000)

        keyval.set('voltage', str(voltage))
        keyval.set('current', str(current))

        print 'Voltage: {0}'.format(keyval.get('voltage'))
        print 'Current: {0}'.format(keyval.get('current'))

        pp.pprint(keyval.get_all())

        time.sleep(1)

if __name__ == '__main__':
    main()
