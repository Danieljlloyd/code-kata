package com.HelloWorldServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HelloWorld")
class HelloWorldServlet extends HttpServlet
{
	public HelloWorldServlet() {
		super();
	}

	public void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			IOException {

		PrintWriter out = response.getWriter();
		out.println("<html><body>Hello world!</body></html>");
	}
}
