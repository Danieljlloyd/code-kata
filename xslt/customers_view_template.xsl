<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="base.xsl"/>
<xsl:include href="customers_template.xsl"/>

<xsl:template match="content">
  <xsl:apply-templates select="customers"/>
  <button type="button" onclick="updateCustomers();">Update</button>
</xsl:template>

</xsl:stylesheet>
