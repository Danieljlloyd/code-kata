function loadXMLDoc(filename)
{
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", filename, false);
	xhttp.send("");
	return xhttp.responseXML;
}

function renderXSL(xmlData, xslFile)
{
	hiddenCustomers = document.getElementById("hidden-customers");
	xsl = loadXMLDoc(xslFile);
	xsltProcessor = new XSLTProcessor();
	xsltProcessor.importStylesheet(xsl);
	resultDocument = xsltProcessor.transformToFragment(xmlData, document);
	return resultDocument;
}

function updateCustomers()
{
	/* This would normally be a REST API request. */
	xml = loadXMLDoc("new_customers.xml");

	newCustomers = renderXSL(xml, "customers_template.xsl");
	document.getElementById("customer-list").replaceWith(newCustomers);
}
