<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="customers">
<ul id="customer-list">
  <xsl:apply-templates select="customer"/>
</ul>
</xsl:template>

<xsl:template match="customer">
<li>
  <xsl:value-of select="@first_name"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="@last_name"/>
  <xsl:text> is </xsl:text>
  <xsl:value-of select="@age"/>
  <xsl:text> years of age.</xsl:text>
</li>
</xsl:template>

</xsl:stylesheet>
