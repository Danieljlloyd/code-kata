<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="view">
  <html>
    <head>
      <script src="app.js"></script>
      <page-title><xsl:value-of select="page-title"/></page-title>
    </head>
    <body>
      <h1><xsl:value-of select="page-title"/></h1>
      <xsl:apply-templates select="content"/>
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
