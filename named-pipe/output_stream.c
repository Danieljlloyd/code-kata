#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <stdio.h>

char pipe_close_flag;
void pipe_close_handler(int sigint);

#define FIFO_PERMS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH

struct OutputStream {
	char fifo_filename[50];
	int fifo_fd;
};

int output_stream_init(struct OutputStream *self, const char *fifo_file);
int output_stream_print(struct OutputStream *self, char *msg);

#define Print(x, y) output_stream_print(x, y)

int main(void)
{
	struct OutputStream out;

	pipe_close_flag = 0;
	signal(SIGPIPE, pipe_close_handler);

	output_stream_init(&out, "/tmp/my_fifo");

	for (;;) {
		Print(&out, "Hello FIFO!\n");
		sleep(1);
	}

	return 0;
}

int output_stream_init(struct OutputStream *self, const char *fifo_file)
{
	strcpy(self->fifo_filename, fifo_file);
	self->fifo_fd = -1;

	if (strcmp(fifo_file, "")) {
		unlink(fifo_file);
		mkfifo(fifo_file, FIFO_PERMS);
	}

	return 0;
}

int output_stream_print(struct OutputStream *self, char *msg)
{
	int fifo_fd;
	int rc;

	printf("%d\n", self->fifo_fd);
	puts(msg);

	if (!strcmp(self->fifo_filename, "")) {
		return 1;
	}

	if (pipe_close_flag == 1) {
		puts("Bad file descriptor");
		close(self->fifo_fd);
		self->fifo_fd = -1;
		pipe_close_flag = 0;
	}

	if (self->fifo_fd < 0)
		self->fifo_fd = open(self->fifo_filename,
			O_WRONLY | O_NONBLOCK);
	else
		rc = write(self->fifo_fd, msg, strlen(msg));
}

void pipe_close_handler(int sigint)
{
	pipe_close_flag = 1;
}
