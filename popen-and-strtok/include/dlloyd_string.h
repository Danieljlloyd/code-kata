#ifndef DLLOYD_STRING_H
#define DLLOYD_STRING_H

enum dlloyd_string_error {
    NO_ERROR,
    SPLIT_NULL_STRING
};

enum dlloyd_string_error dlloyd_string_errno;
extern enum dlloyd_string_error dlloyd_string_errno;

int split(char *, const char *, char **);
int trim(char *);
const char * dlloyd_string_error(void);

#endif
