#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dlloyd_string.h>

int split(char *str, const char *delim, char **fields) {
    if(str == NULL) {
        dlloyd_string_errno = SPLIT_NULL_STRING;
        return -1;
    }

    char **field = fields;
    char *saveptr = "";
    char *tok = "";
    int nf = 0;

    for(nf=0; tok != NULL; nf++) {
        tok = strtok_r(str, delim, &saveptr);

        if(tok != NULL) {
            *field = tok;
            field++;
        }

        while(isspace(*saveptr)){
            *saveptr = '\0';
            saveptr++;
        }

        str=NULL;
    }

    return nf;
}
