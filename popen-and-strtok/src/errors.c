#include <dlloyd_string.h>

const char * dlloyd_string_error(void)
{ 
    switch(dlloyd_string_errno) {
        case NO_ERROR:
            return "No error for D-Lloyd's string functions.\n";
            break;
        case SPLIT_NULL_STRING:
            return "Function \"split\" cannot accept NULL character for str argument.\n";
            break;
    }

    return "This should never happen.";
}
