#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <dlloyd_string.h>

#define SZ_LINE 1000
#define MAX_FIELDS 100

int main(void)
{
    /* Error catching variables */
    char command[100];
    char err[100];


    /* Get the output of todo.sh */
    strcpy(command, "t ls");
    FILE *fp = popen(command, "r");
    if(fp == NULL) goto FAIL_PIPE;

    /* Parse the output into words */
    /* AWK-like functionality */
    char line[SZ_LINE] = {'\0'};
    char *fields[MAX_FIELDS] = {"\0"};

    for(int nr = 1; fgets(line, SZ_LINE, fp); nr++) {
        trim(line);
        int nf = split(line, " ", fields);
        if(nf == -1) goto FAIL_PARSE;

        /* Do processing. In this case: print out fields for task 3 */
        if(!strcmp(fields[0], "03")) {
            for(int i=1; i<nf; i++)
                printf("%d,%d --> %s\n", nr, i, fields[i-1]);
        }
    }

    pclose(fp);

    /* Grep-like functionality */
    strcpy(command, "ls");
    fp = popen(command, "r");
    if(fp == NULL) goto FAIL_PIPE;

    for(int nr = 1; fgets(line, SZ_LINE, fp); nr++) {
        trim(line);
        if(strstr(line, "Make"))
            printf("--> %s\n", line);
    }

    int ret = split(NULL, " ", fields);
    if(ret == -1) goto FAIL_PARSE;

    pclose(fp);

    return 0;

FAIL_PARSE:
    printf("Failed parsing output for command \"%s\" -- %s", command, dlloyd_string_error());
    pclose(fp);
    exit(1);

FAIL_PIPE:
    sprintf(err, "Failed retrieving output of command \"%s\"", command);
    perror(err);
    exit(1);
}
