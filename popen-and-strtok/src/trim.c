#define _BSD_SOURCE 1

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int trim(char *str) {
    while(isspace(*str)) str++;

    char *i = str + strlen(str) - 1;
    while(isspace(*i)) i--;
    *(i + 1) = '\0';

    return 0;
}
