/*
An example of non-blocking I/O in C.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define BUFLEN 1024

#define LOGGER_LVL_CRITICAL 50
#define LOGGER_LVL_ERROR 40
#define LOGGER_LVL_WARNING 30
#define LOGGER_LVL_INFO 20
#define LOGGER_LVL_DEBUG 10

////////////////////////////////////////////////////////////////////////////////
// Logger Class: Put into a separate file.
////////////////////////////////////////////////////////////////////////////////
struct Logger {
	char name[50];
	int level;
};

int logger_init(struct Logger *self, const char *name, int level);
void logger_fatal_error(struct Logger *self, const char *msg);
int logger_print(struct Logger *self, const char *msg, int level);

int logger_init(struct Logger *self, const char *name, int level)
{
	strcpy(self->name, name);
	self->level = level;

	return 1;
}

void logger_fatal_error(struct Logger *self, const char *msg)
{
	time_t now;
	char nowstr[50];

	now = time(NULL); 
	strcpy(nowstr, ctime(&now));
	nowstr[strlen(nowstr)-1] = '\0';

	fprintf(stderr, "%s: %s: FATAL: %s\n", nowstr, self->name, msg);
	exit(-1);
}

int logger_print(struct Logger *self, const char *msg, int level)
{
	char level_desc[50];
	time_t now;
	char nowstr[50];

	now = time(NULL); 
	strcpy(nowstr, ctime(&now));
	nowstr[strlen(nowstr)-1] = '\0';
	 
	if (level >= self->level) {
		switch(level) {
			case LOGGER_LVL_CRITICAL:
				strcpy(level_desc, "CRITICAL");
				break;
			case LOGGER_LVL_ERROR:
				strcpy(level_desc, "ERROR");
				break;
			case LOGGER_LVL_WARNING:
				strcpy(level_desc, "WARNING");
				break;
			case LOGGER_LVL_INFO:
				strcpy(level_desc, "INFO");
				break;
			case LOGGER_LVL_DEBUG:
				strcpy(level_desc, "DEBUG");
				break;
			default:
				logger_fatal_error(self,
					"Log level not defined");
		}

		fprintf(stderr, "%s: %s: %s: %s\n", nowstr, self->name,
			level_desc, msg);

		return 0;
	}

	return 1;
}
////////////////////////////////////////////////////////////////////////////////
// End logger class
////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	int rc;
	int fd;
	char buf[BUFLEN];
	int bufind;
	struct Logger logger;

	bzero(buf, BUFLEN);
	bufind = 0;

	logger_init(&logger, "main", LOGGER_LVL_WARNING);

	/* Open the file to do non-blocking I/O for. */
	fd = open("/dev/ttyS0", O_RDWR);
	if (fd < 0)
		logger_fatal_error(&logger, "Failed to open file");

	fcntl(fd, F_SETFL, O_NONBLOCK);

	while (1) {
		int i;
		int bufind_old = bufind;

		/* Read as much as you can from the buffer. */
		rc = 0;
		while (rc != -1) {
			rc = read(fd, buf+bufind, 1);

			if (rc != -1)
				bufind = (bufind + rc) % BUFLEN;
		}

		/* Echo what you have read. */
		for (i=bufind_old; i<=bufind; i++) {
			write(1, buf+i, 1);
		}

		/* Sleep for this cycle. */
		puts("Sleeping 1s...");
		sleep(1);
	}
}
