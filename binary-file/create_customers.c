#include <stdio.h>
#include <stdlib.h>
#include <customers.h>

int main(void)
{
	struct Customers my_customers;
	struct Customer my_customer;

	customers_init(&my_customers);

	puts("Creating customers...");
	customer_init(&my_customer, "Daniel", "DanielLloyd7@gmail.com",
			24, 'm');
	customers_push(&my_customers, my_customer);

	customer_init(&my_customer, "Mathew", "Mathew.Lloyd@gmail.com",
			18, 'm');
	customers_push(&my_customers, my_customer);

	customer_init(&my_customer, "Steven", "stevenjohnlloyd@gmail.com",
			57, 'm');
	customers_push(&my_customers, my_customer);

	customer_init(&my_customer, "Lorraine Lloyd", "l.j.lloyd64@gmail.com",
			53, 'f');
	customers_push(&my_customers, my_customer);

	puts("Writing customers to file...");
	customers_dump(&my_customers);

	customers_print(&my_customers);

	return 0;
}
