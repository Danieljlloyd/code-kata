#include <stdio.h>
#include <customers.h>

int main(void)
{
	struct Customers my_customers;

	puts("Reading customers from file...");
	customers_load(&my_customers);

	customers_print(&my_customers);
}
