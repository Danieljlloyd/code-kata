struct Customer {
	char name[50];
	char email[50];
	char age;
	char sex;
};

struct Customers {
	struct Customer data[100];
	char n;
};

int customer_init(struct Customer *, char *, char *, char, char);
int customers_init(struct Customers *);
int customers_push(struct Customers *, struct Customer);
int customers_dump(struct Customers *);
int customers_print(struct Customers *);
int customers_load(struct Customers *);
