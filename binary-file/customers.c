#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <customers.h>

#define CUSTOMERS_FILE "/tmp/customers.bin"

int customer_init(struct Customer *self, char *name, char *email,char age,
		char sex)
{
	strcpy(self->name, name);
	strcpy(self->email, email);
	self->age = age;
	self->sex = sex;
}

int customers_init(struct Customers *self)
{
	memset(&self->data, 0, sizeof(struct Customer) * 100);
}

int customers_push(struct Customers *self, struct Customer customer)
{
	self->data[self->n] = customer;
	self->n++;
}

int customers_dump(struct Customers *self)
{
	int fd = open(CUSTOMERS_FILE, O_WRONLY | O_CREAT);
	if (fd == -1) {
		perror("Persisting customers");
		exit(-1);
	}

	write(fd, self, sizeof(struct Customers));
}

int customers_print(struct Customers *self)
{
	int i;
	char buf[100];
	int count;
	struct Customer customer;

	for (i=0; i<self->n; i++)
	{
		customer = self->data[i];
		count = sprintf(buf, "%s, %s, %d, %c\n", customer.name,
				customer.email, customer.age, customer.sex);
		write(1, buf, count);
	}
}

int customers_load(struct Customers *self)
{
	int fd = open(CUSTOMERS_FILE, O_RDONLY);
	if (fd == -1) {
		perror("Loading customers");
		exit(-1);
	}

	read(fd, self, sizeof(struct Customers));
}
