#include <string.h>
#include <stdio.h>

struct cashier {
	int items_sold;
};

struct manager {
	int projects_completed;
};

struct person {
	char name[30];
	int salary;

	union {
		struct cashier cashier_i;
		struct manager manager_i;
	} u;
};

int main(void)
{
	struct person cashier;
	struct person manager;

	cashier.u.cashier_i.items_sold = 30;
	strcpy(cashier.name, "Fred");
	cashier.salary = 30000;

	strcpy(manager.name, "Gustavo");
	manager.salary = 80000;
	manager.u.manager_i.projects_completed = 12;

	printf("My name is %s, I am a cashier and I have sold %d "
		"items today. My salary is %d\n", cashier.name,
		cashier.u.cashier_i.items_sold, cashier.salary);

	printf("My name is %s, I am a manager and I have executed %d "
		"projects this year. My salary is %d\n", manager.name,
		manager.u.manager_i.projects_completed, manager.salary);


	return 0;
}
