#include <stdio.h>

struct modbus_device {
	int slave_id;
};

struct canbus_device {
	int msg_ids[100];
	int write_msg_id;
};

struct device {
	int (*read) (struct device *);
	int (*write) (struct device *);

	union {
		struct modbus_device modbus_i;
		struct canbus_device canbus_i;
	} u;
};

////////////////////////////////////////////////////////////////////////////////

int modbus_device_read(struct device *dev)
{
	printf("Reading from modbus device with slave id %d.\n",
		dev->u.modbus_i.slave_id);
}

int modbus_device_write(struct device *dev)
{
	printf("Writing to modbus device with slave id %d.\n",
		dev->u.modbus_i.slave_id);
}

struct device create_modbus_device(void)
{
	struct device dev;

	dev.read = modbus_device_read;
	dev.write = modbus_device_write;
	dev.u.modbus_i.slave_id = 24;

	return dev;
}

////////////////////////////////////////////////////////////////////////////////

int canbus_device_read(struct device *dev)
{
	printf("Reading packets canbus device with message ids %d and %d.\n",
		dev->u.canbus_i.msg_ids[0], dev->u.canbus_i.msg_ids[1]);
}

int canbus_device_write(struct device *dev)
{
	printf("Writing packet to canbus device with message id %d.\n",
		dev->u.canbus_i.write_msg_id);
}

struct device create_canbus_device(void)
{
	struct device dev;

	dev.read = canbus_device_read;
	dev.write = canbus_device_write;
	dev.u.canbus_i.msg_ids[0] = 24;
	dev.u.canbus_i.msg_ids[1] = 86;
	dev.u.canbus_i.write_msg_id = 104;

	return dev;
}

////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	struct device devices[2] = {
		create_canbus_device(),
		create_modbus_device()
	};

	for (int i=0; i<2; i++) {
		struct device dev = devices[i];

		dev.read(&dev);
		dev.write(&dev);
	}
}
