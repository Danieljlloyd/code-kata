/*
Embedded function pointers are used where objects of the same type are
not populous.
*/

#include <stdio.h>
#include <string.h>

struct spi_driver {
	char buf[BUFSIZ];
	int (*read) (struct spi_driver *, char *in);
	int (*write) (struct spi_driver *, char *out);
};

int spi_driver_read(struct spi_driver *, char *in);
int spi_driver_write(struct spi_driver *, char *out);

void spi_driver_init(struct spi_driver *driver)
{
	bzero(driver->buf, BUFSIZ);
	driver->read = spi_driver_read;
	driver->write = spi_driver_write;
}

int spi_driver_read(struct spi_driver *driver, char *in)
{
	strcpy(in, "Buffer read from device.");
}

int spi_driver_write(struct spi_driver *driver, char *out)
{
	printf("Writing %s to SPI interface.\n", out);
}

int main(void)
{
	struct spi_driver my_driver;
	char in[BUFSIZ];
	char out[BUFSIZ];

	bzero(in, BUFSIZ);
	bzero(out, BUFSIZ);

	spi_driver_init(&my_driver);
	spi_driver_read(&my_driver, in);

	puts(in);

	spi_driver_write(&my_driver, "Hello world!");

	return 0;
}
