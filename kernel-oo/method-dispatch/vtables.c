#include <stdio.h>
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
// Generic Animal Class
////////////////////////////////////////////////////////////////////////////////

struct animal_ops;
struct animal;

struct animal {
	const struct animal_ops *ops;
	char name[30];
};

struct animal_ops {
	void (*say) (struct animal *);
	void (*eat) (struct animal *);
};

void generic_animal_say(struct animal *animal)
{
	puts("Says nothing.");
}

void generic_animal_eat(struct animal *animal) {
	puts("Eating grass.");
}

#define ANIMAL_DEFAULTS \
	.say = generic_animal_say, \
	.eat = generic_animal_eat

////////////////////////////////////////////////////////////////////////////////
// Cow type animal
////////////////////////////////////////////////////////////////////////////////

void cow_init(struct animal *);
void cow_say(struct animal *);

static const struct animal_ops COW_OPS = {
	ANIMAL_DEFAULTS,
	.say = cow_say
};

void cow_init(struct animal *cow)
{
	cow->ops = &COW_OPS;
	strcpy(cow->name, "Cassy");
}

void cow_say(struct animal *cow)
{
	printf("Moooooo! I'm %s the cow.\n", cow->name);
}

////////////////////////////////////////////////////////////////////////////////
// Lion type animal
////////////////////////////////////////////////////////////////////////////////

void lion_init(struct animal *);
void lion_say(struct animal *);
void lion_eat(struct animal *);

static const struct animal_ops LION_OPS = {
	ANIMAL_DEFAULTS,
	.say = lion_say,
	.eat = lion_eat
};

void lion_init(struct animal* lion)
{
	lion->ops = &LION_OPS;
	strcpy(lion->name, "Larry");
}

void lion_say(struct animal *lion)
{
	printf("Roooaaaarrrrrrrr! I'm %s the lion.\n", lion->name);
}

void lion_eat(struct animal *lion)
{
	puts("Eating YOU!");
}

////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Create a cow called Cassy
	struct animal cassy = {0};
	cow_init(&cassy);

	// Create a lion called Larry
	struct animal larry = {0};
	lion_init(&larry);

	cassy.ops->say(&cassy);
	cassy.ops->eat(&cassy);

	larry.ops->say(&larry);
	larry.ops->eat(&larry);

	return 0;
}
