set terminal eps enhanced font 'Arial,10'

set output 'plot.eps'

set title 'Test plot'
set xlabel 'Time (s)'
set ylabel 'Voltage (V)'
set zlabel 'Magnitude'

splot sin(x) + cos(y) * exp(x / 5) + exp(-x/20) + x / 3
