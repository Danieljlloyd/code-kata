#include <stdio.h>
#include <stdlib.h>
#include "driver.h"

int main(void)
{
	driver_factory_t *factory = make_driver_factory();
	char buf[100];
	driver_t *driver;

	driver = driver_factory_make_driver(factory, "modbus");
	driver->operations->read_fn(driver, buf, 100);
	puts(buf);
	free(driver);

	driver = driver_factory_make_driver(factory, "canbus");
	driver->operations->read_fn(driver, buf, 100);	
	puts(buf);
	free(driver);

	free(factory);

	return 0;
}
