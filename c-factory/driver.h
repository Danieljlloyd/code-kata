#ifndef DRIVER_H
#define DRIVER_H

typedef struct driver_operations_s {
	void (*read_fn) (void *driver, char *buf, int n);
} driver_operations_t;

typedef struct driver_s {
	driver_operations_t *operations;
} driver_t;

typedef struct driver_factory_s driver_factory_t;
driver_factory_t *make_driver_factory(void);
driver_t *driver_factory_make_driver(driver_factory_t *factory,
	char *identifier);

#endif
