#include <stdlib.h>
#include <string.h>
#include "driver.h"

typedef struct modbus_driver_s {
	driver_operations_t *operations;
} modbus_driver_t;

typedef struct canbus_driver_s {
	driver_operations_t *operations;
} canbus_driver_t;

typedef struct driver_factory_s {
} driver_factory_t;

///////////////////////
//// MODBUS_DRIVER ////
///////////////////////
void modbus_driver_read(void *driver, char *buf, int n)
{
	strncpy(buf, "Reading from modbus driver.", n);
}

driver_operations_t MODBUS_DRIVER_OPERATIONS[] = {{
	modbus_driver_read
}};

driver_t *make_modbus_driver()
{
	modbus_driver_t *modbus_driver = malloc(sizeof(modbus_driver_t));
	modbus_driver->operations = MODBUS_DRIVER_OPERATIONS;
	return (driver_t *) modbus_driver;
}

///////////////////////
//// CANBUS DRIVER ////
///////////////////////
void canbus_driver_read(void *driver, char *buf, int n)
{
	strncpy(buf, "Reading from canbus driver.", n);
}

driver_operations_t CANBUS_DRIVER_OPERATIONS[] = {{
	canbus_driver_read
}};

driver_t *make_canbus_driver()
{
	canbus_driver_t *canbus_driver = malloc(sizeof(canbus_driver_t));
	canbus_driver->operations = CANBUS_DRIVER_OPERATIONS;
	return (driver_t *) canbus_driver;
}

////////////////////////
//// DRIVER FACTORY ////
////////////////////////
driver_factory_t *make_driver_factory(void)
{
	driver_factory_t *new_driver_factory = malloc(sizeof(driver_factory_t));
	return new_driver_factory;
}

driver_t *driver_factory_make_driver(driver_factory_t *factory,
	char *identifier)
{
	driver_t *new_driver = NULL;

	if (!strcmp(identifier, "modbus")) {
		new_driver = make_modbus_driver();
	}

	else if (!strcmp(identifier, "canbus")) {
		new_driver = make_canbus_driver();
	}

	return new_driver;
}
