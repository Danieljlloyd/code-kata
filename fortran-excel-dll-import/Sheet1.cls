VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Private Declare Function fortran_dll_add Lib "fortran_example.dll" Alias "fortran_dll_add_@8" (ByRef a As Long, ByRef b As Long) As Long

Sub CallFortranDLL()
    MsgBox "10 + 3 = " & fortran_dll_add(10, 3)
End Sub
