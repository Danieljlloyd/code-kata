"""
Script to demonstrate a UNIX domain socket publisher interface.
"""
import socket
import time

MCAST_GRP='224.0.2.50' 
MCAST_PORT=55542

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

while True:
    sock.sendto('Hello world!', (MCAST_GRP, MCAST_PORT))
    time.sleep(1)
