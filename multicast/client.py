import socket
import time
import struct

MCAST_GRP='224.0.2.50' 
MCAST_PORT=55542

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', MCAST_PORT))
mreq = struct.pack('4sl', socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    print sock.recv(1024)
