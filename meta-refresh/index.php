<?php
  /*
  This is a script to demonstrate the ability of PHP to stream live data through
  the simple use of a meta refresh. Old-school but still effective.
  */

  /* Generate a plot "my_plot.png" based on some data set. */  
  exec("gnuplot my_plot.gnuplot");
?>

<html>
  <head>
    <!-- Refresh this page every 10 seconds. -->
    <meta http-equiv="refresh" content="10">
  </head>

  <body>
    <!-- Display a plot of a sinusoid. -->
    <img src="my_plot.png" alt="my plot" style="width:900px;height:512px">
  </body>
</html>
