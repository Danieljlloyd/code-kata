set terminal png
set output "my_plot.png"

set datafile separator ","

set title "Voltage Expressed In Time"
set xlabel "time (s)"
set ylabel "voltage (V)"

plot 'data.csv' using 1:2 lt -1 title 'Test function'
