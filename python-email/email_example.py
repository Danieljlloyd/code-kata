import smtplib
from email.mime.text import MIMEText
import sys

def error_exit(msg):
    sys.stderr.write(msg + '\n')
    sys.exit(-1)

def main():
    sender = 'DanielLloyd7@gmail.com'
    receiver = 'DanielLloyd7@gmail.com'
    password = 'not my real password'

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(sender, password)

    msg = None
    with open('hello.txt') as fp:
        msg = MIMEText(fp.read())

    if msg is None:
        error_exit('Could not open file')


    msg['Subject'] = 'Hello'
    msg['From'] = sender
    msg['To'] = receiver

    server.sendmail(sender, [receiver], msg.as_string())
    server.quit()

    print('Successfully sent email to {0}'.format(receiver))

if __name__ == '__main__':
    main()
