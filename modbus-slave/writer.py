import modbus_tk
import modbus_tk.defines as cst
import modbus_tk.modbus_tcp as modbus_tcp
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('slave', help='Slave ID', type=int)
parser.add_argument('port', help='Modbus port', type=int)
parser.add_argument('register', help='Register to write', type=int)
parser.add_argument('value', help='Value to write', type=int)
args = parser.parse_args()

logger = modbus_tk.utils.create_logger('console')

master = modbus_tcp.TcpMaster(port=args.port)
master.set_timeout(5.0)

master.execute(args.slave, cst.WRITE_MULTIPLE_REGISTERS, args.register, 1,
    output_value=[args.value])
