import sys
import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp
import time
import socket

logger = modbus_tk.utils.create_logger('console')

server = modbus_tcp.TcpServer(port=51101)
server.start()

slave = server.add_slave(1)
slave.add_block('0', cst.HOLDING_REGISTERS, 0, 100)

while True:
    start_time = time.time()

    offset = 0
    length = 10
    values = slave.get_values('0', offset, length)

    print '-'*80
    for i in range(length):
        print('{0}: {1}'.format(offset+i, values[i]))

    while time.time() < start_time + 1:
        time.sleep(0.1)
