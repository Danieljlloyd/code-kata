#ifndef _MY_ERROR_CODES
#define _MY_ERROR_CODES

/*
 * A pure C library for calculating the error codes using the parity, checksum
 * and CRC methods.
 */

/* Calculate the checksum of a packet.
 *
 * Inputs
 * ------
 *  * packet: Pointer to the packet buffer consisting of 16-bit words.
 *  * count: Amount of bytes in the packet.
 *
 * Return Value
 * ------------
 *  Returns the 16-bit integer checksum value.
 */
unsigned short cksum16(unsigned short *packet, int count);

#endif
