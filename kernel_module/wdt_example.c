#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#define DEVICE_NAME "mywdt"
#define CLASS_NAME "mywdt_class"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Daniel Lloyd");
MODULE_DESCRIPTION("A simple watchdog timer simulator");
MODULE_VERSION("0.1");
MODULE_SUPPORTED_DEVICE(DEVICE_NAME);

static int major_number;
static struct class *wdt_class;
static int wdt_device;

static int wdt_open(struct inode *inodep, struct file *filep);
static ssize_t wdt_read(struct file *filep, char *buffer, size_t len,
	loff_t *offset);
static  ssize_t wdt_write(struct file *filep, const char *buffer, size_t len,
	loff_t *offset);
static int wdt_release(struct inode *inodep, struct file *filep);

static struct file_operations fops = {
	.open = wdt_open,
	.read = wdt_read,
	.write = wdt_write,
	.release = wdt_release
};

static int __init wdt_init(void)
{
	printk(KERN_INFO "mywdt: Attempting to load driver.\n");

	/* Try to dynamically allocate a major number for the device. */
	printk(KERN_INFO "mywdt: Attempting to get major number.\n");
	major_number = register_chrdev(0, DEVICE_NAME, &fops);
	if (major_number < 0) {
		printk(KERN_ALERT "mywdt: Failed to register a major number.\n");
	}

	/* Register the device class. */
	printk(KERN_INFO "mywdt: Attempting to register device class.\n");
	wdt_class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(wdt_class)) {
		unregister_chrdev(major_number, DEVICE_NAME);
		printk(KERN_ALERT "mywdt: Failed to register device class.\n");
		return PTR_ERR(wdt_class);
	}

	/* Create the device file. */
	printk(KERN_INFO "mywdt: Attempting to create device file.\n");
	wdt_device = device_create(wdt_class, NULL, MKDEV(major_number, 0),
		NULL, DEVICE_NAME);
	if (IS_ERR(wdt_device)) {
		class_destroy(wdt_class);
		unregister_chrdev(major_number, DEVICE_NAME);
		printk(KERN_ALERT "mywdt: Failed to create the device.\n");
		return PTR_ERR(wdt_device);
	}

	printk(KERN_INFO "mywdt: device class registered correctly.\n");
}

static void __exit wdt_exit(void)
{
	device_destroy(wdt_class, MKDEV(major_number, 0));
	class_unregister(wdt_class);
	class_destroy(wdt_class);
	unregister_chrdev(major_number, DEVICE_NAME);
}

static int wdt_open(struct inode *inodep, struct file *filep)
{
	return 0;
}

static ssize_t wdt_read(struct file *filep, char *buffer, size_t len,
	loff_t *offset)
{
	return 0;
}

static  ssize_t wdt_write(struct file *filep, const char *buffer, size_t len,
	loff_t *offset)
{
	printk(KERN_INFO "mywdt: Kicking the watchdog.");
	return 1;
}

static int wdt_release(struct inode *inodep, struct file *filep)
{
	return 0;
}

module_init(wdt_init);
module_exit(wdt_exit);
